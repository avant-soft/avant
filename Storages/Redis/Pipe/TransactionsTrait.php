<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis\Pipe;


/**
 * Trait TransactionsTrait
 * @subpackage Avant\Storages\Redis\Pipe
 */
trait TransactionsTrait
{
    use PipeTrait;

    /**
     * DISCARD
     * @link http://redis.io/commands/discard
     *
     * @return bool Always True
     */
    public function discard()
    {
        return $this->returnCommand( ['DISCARD'] );
    }

    /**
     * EXEC
     * @link http://redis.io/commands/exec
     *
     * @return mixed
     */
    public function exec()
    {
        return $this->returnCommand( ['EXEC'] );
    }

    /**
     * MULTI
     * @link http://redis.io/commands/multi
     *
     * @return bool Always True
     */
    public function multi()
    {
        return $this->returnCommand( ['MULTI'] );
    }

    /**
     * UNWATCH
     * Time complexity: O(1)
     * @link http://redis.io/commands/unwatch
     *
     * @return bool Always True
     */
    public function unwatch()
    {
        return $this->returnCommand( ['UNWATCH'] );
    }

    /**
     * WATCH key [key ...]
     * Time complexity: O(1) for every key.
     * @link http://redis.io/commands/watch
     *
     * @param string|string[] $keys
     * @return bool Always True
     */
    public function watch($keys)
    {
        return $this->returnCommand( ['WATCH'], (array)$keys );
    }
}