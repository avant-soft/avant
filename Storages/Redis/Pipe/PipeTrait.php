<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis\Pipe;


/**
 * Trait PipeTrait
 * @subpackage Avant\Storages\Redis\Pipe
 *
 * @method returnCommand(array $command, array $params = null, $parserId = null)
 * @method subscribeCommand(array $subCommand, array $unsubCommand, array $params = null, $callback)
 */
trait PipeTrait
{

}