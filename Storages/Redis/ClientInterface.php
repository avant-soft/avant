<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis;


/**
 * Interface ClientInterface
 * @subpackage Avant\Storages\Redis
 */
interface ClientInterface
{
    public function returnCommand(array $command, array $params = null, $parserId = null);

    public function subscribeCommand(array $subCommand, array $unsubCommand, array $params = null, $callback);
}