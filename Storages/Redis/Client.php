<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis;

use Avant\Storages\Redis\Pipe\ChannelTrait;
use Avant\Storages\Redis\Pipe\ClusterTrait;
use Avant\Storages\Redis\Pipe\ConnectionTrait;
use Avant\Storages\Redis\Pipe\HashTrait;
use Avant\Storages\Redis\Pipe\HyperLogTrait;
use Avant\Storages\Redis\Pipe\KeysTrait;
use Avant\Storages\Redis\Pipe\LatencyTrait;
use Avant\Storages\Redis\Pipe\ListTrait;
use Avant\Storages\Redis\Pipe\ScriptTrait;
use Avant\Storages\Redis\Pipe\ServerTrait;
use Avant\Storages\Redis\Pipe\SetsTrait;
use Avant\Storages\Redis\Pipe\SortedTrait;
use Avant\Storages\Redis\Pipe\StringsTrait;
use Avant\Storages\Redis\Pipe\TransactionsTrait;


/**
 * Class Client
 * @subpackage Avant\Storages\Redis
 */
class Client extends ClientAbstract
{
    use ChannelTrait;
    use ClusterTrait;
    use ConnectionTrait;
    use HashTrait;
    use HyperLogTrait;
    use KeysTrait;
    use LatencyTrait;
    use ListTrait;
    use ScriptTrait;
    use ServerTrait;
    use SetsTrait;
    use SortedTrait;
    use StringsTrait;
    use TransactionsTrait;

    /**
     * @var int
     */
    protected $db = 0;

    /**
     * @return int
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param int $db
     */
    public function setDb($db)
    {
        $this->select( $this->db = $db );
    }

}

/* End of file Client.php */
