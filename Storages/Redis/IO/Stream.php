<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis\IO;

use Avant\Storages\Redis\IODriverInterface;


/**
 * Class Stream
 * @subpackage Avant\Storages\Redis\IO
 */
class Stream implements IODriverInterface
{
    const EOL                 = "\r\n";
    const TYPE_SIMPLE_STRINGS = '+';
    const TYPE_ERRORS         = '-';
    const TYPE_INTEGERS       = ':';
    const TYPE_BULK_STRINGS   = '$';
    const TYPE_ARRAYS         = '*';

    /**
     * @var resource
     */
    protected $resource;

    /**
     * @var string
     */
    protected $server;

    /**
     * @var int|null
     */
    protected $ttl;

    /**
     * @param string         $server
     * @param int|float|null $ttl
     */
    public function __construct($server, $ttl = null)
    {
        $this->server = $server;
        if (is_numeric( $ttl )) {
            $this->ttl = ceil( $ttl * 1000000 );
        }
    }

    /**
     *
     */
    public function __destruct()
    {
        if ($this->resource) {
            fclose( $this->resource );
        }
    }

    /**
     * @inheritdoc
     */
    public function send(array $structures)
    {
        $this->write( $this->packArray( $structures ) );

        return $response = $this->read();
    }

    /**
     * @inheritdoc
     */
    public function sendMulti(array $structures)
    {
        $raw = '';
        foreach ($structures as $structure) {
            $raw .= $this->pack( $structure );
        }
        $this->write( $raw );
        $response = [];
        for ($i = count( $structures ); $i > 0; --$i) {
            $response[] = $this->read();
        }

        return $response;
    }

    /**
     * @inheritdoc
     */
    public function subscribe(array $structures, $callback)
    {
        $this->write( $this->packArray( $structures ) );
        do {
            try {
                $response = (array)$this->read();
                for ($i = count( $response ); $i < 4; ++$i) {
                    $response[] = null;
                }
            } catch (\ErrorException $Ex) {
                $response = [null, null, null, null];
            }
            $continue = call_user_func_array( $callback, $response );
        } while ($continue);

    }

    /**
     * @return resource
     * @throws \ErrorException
     */
    protected function getResource()
    {
        if (!$this->resource) {
            if (!$this->resource = stream_socket_client( $this->server )) {
                throw new \ErrorException( sprintf(
                  'Unable to connect to %s', $this->server
                ) );
            }
            if (isset( $this->ttl )) {
                stream_set_timeout( $this->resource, 0, $this->ttl );
            }
        }

        return $this->resource;
    }

    /**
     * @param $data
     * @return string
     * @throws \ErrorException
     */
    protected function pack($data)
    {
        if (is_string( $data ) || is_int( $data ) || is_bool( $data ) || is_float( $data ) || is_null( $data )) {
            return $this->packString( (string)$data );
        }
        if (is_array( $data )) {
            return $this->packArray( $data );
        }
        throw new \ErrorException( gettype( $data ) );
    }

    /**
     * @param array $array
     * @return string
     */
    protected function packArray($array)
    {
        $pack = self::TYPE_ARRAYS.count( $array ).self::EOL;
        foreach ($array as $a) {
            $pack .= $this->pack( $a );
        }

        return $pack;
    }

    /**
     * @return string
     */
    protected function packNull()
    {
        return self::TYPE_BULK_STRINGS.'-1'.self::EOL;
    }

    /**
     * @param string $string
     * @return string
     */
    protected function packString($string)
    {
        return self::TYPE_BULK_STRINGS.mb_strlen( $string ).self::EOL.$string.self::EOL;
    }

    /**
     * @return array|\ErrorException|int|null|string
     * @throws \ErrorException
     */
    protected function read()
    {
        if (!$line = $this->readRawLine()) {
            throw new \ErrorException( 'Empty response. Please, check connection timeout.' );
        }

        $type = $line[0];
        $data = mb_substr( $line, 1, -2 );

        if ($type === self::TYPE_BULK_STRINGS) {
            $length = intval( $data );
            if ($length === -1) {
                return null;
            }

            return mb_substr( $this->readRaw( $length + 2 ), 0, -2 );
        }

        if ($type === self::TYPE_SIMPLE_STRINGS) {
            if ($data === 'OK') {
                return true;
            }

            return $data;
        }

        if ($type === self::TYPE_INTEGERS) {
            return (int)$data;
        }

        if ($type === self::TYPE_ARRAYS) {
            $count = (int)$data;
            if ($count === -1) {
                return null;
            }
            $array = [];
            for ($i = 0; $i < $count; ++$i) {
                $array[] = $this->read();
            }

            return $array;
        }

        if ($type === self::TYPE_ERRORS) {
            return new \ErrorException( $data );
        }

        throw new \ErrorException( 'Unknown protocol type '.$type );
    }

    /**
     * @param $length
     * @return string
     * @throws \ErrorException
     * @throws \Exception
     */
    protected function readRaw($length)
    {
        $buffer = '';
        $read   = 0;
        if ($length > 0) {
            do {
                $block_size = ($length - $read) > 1024 ? 1024 : ($length - $read);
                $block      = fread( $this->getResource(), $block_size );
                if ($block === false) {
                    throw new \Exception( 'Failed to read response from stream' );
                } else {
                    $read   += mb_strlen( $block );
                    $buffer .= $block;
                }
            } while ($read < $length);
        }

        return $buffer;
    }

    /**
     * @return string
     */
    protected function readRawLine()
    {
        return fgets( $this->getResource() );
    }

    /**
     * @param string $string
     * @return int
     */
    protected function write($string)
    {
        return fwrite( $this->getResource(), $string, mb_strlen( $string ) );
    }
}

/* End of file Stream.php */
