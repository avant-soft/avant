<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis;

use Avant\Base\Configurable;
use Avant\Stdlib\InitTrait;
use Avant\Storages\Redis\IO\Stream;


/**
 * Class ClientAbstract
 * @subpackage Avant\Storages\Redis
 */
abstract class ClientAbstract extends Configurable implements ClientInterface
{
    use InitTrait;

    /**
     * @var IODriverInterface
     */
    protected $driver;
    /**
     * @var string
     */
    protected $host = '127.0.0.1';
    /**
     * @var int
     */
    protected $lifetime = 60;
    /**
     * @var int
     */
    protected $port = 6379;


    public function executeRaw($structure)
    {
        $response = $this->getDriver()->send( $structure );
        if ($response instanceof \ErrorException) {
            throw $response;
        }

        return $response;
    }

    /**
     * @param string $command
     * @return mixed
     */
    public function executeRawString($command)
    {
        return $this->executeRaw( $this->parseRaw( $command ) );
    }

    /**
     * @param string $command
     * @return string[]
     */
    public function parseRaw($command)
    {
        $structure = [];
        $line      = '';
        $quotes    = false;
        for ($i = 0, $length = strlen( $command ); $i <= $length; ++$i) {
            if ($i === $length) {
                if (isset( $line[0] )) {
                    $structure[] = $line;
                    $line        = '';
                }
                break;
            }
            if ($command[$i] === '"' && $i && $command[$i - 1] !== '\\') {
                $quotes = !$quotes;
                if (!$quotes && !isset( $line[0] ) && $i + 1 === $length) {
                    $structure[] = $line;
                    $line        = '';
                }
            } elseif ($command[$i] === ' ' && !$quotes) {
                if (isset( $command[$i + 1] ) && trim( $command[$i + 1] )) {
                    if (count( $structure ) || isset( $line[0] )) {
                        $structure[] = $line;
                        $line        = '';
                    }
                }
            } else {
                $line .= $command[$i];
            }
        }
        array_walk( $structure, function (&$line) {
            $line = str_replace( '\\"', '"', $line );
        } );

        return $structure;
    }

    public function returnCommand(array $command, array $params = null, $parserId = null)
    {
        return $this->executeCommand( $command, $params, $parserId );
    }

    public function subscribeCommand(array $subCommand, array $unsubCommand, array $params = null, $callback)
    {
        $this->getDriver()->subscribe( $this->getStructure( $subCommand, $params ), $callback );

        return $this->executeCommand( $unsubCommand, $params );
    }

    protected function executeCommand(array $command, array $params = null, $parserId = null)
    {
        $response = $this->getDriver()->send( $this->getStructure( $command, $params ) );
        if ($response instanceof \ErrorException) {
            throw $response;
        }
        if (isset( $parserId )) {
            return Response::parse( $parserId, $response );
        }

        return $response;
    }

    protected function getDriver()
    {
        if ($this->driver == null) {
            $this->driver = new Stream( sprintf( 'tcp://%s:%d', $this->host, $this->port ), $this->lifetime );
        }

        return $this->driver;
    }

    /**
     * @param string[]   $command
     * @param array|null $params
     * @return string[]
     */
    protected function getStructure(array $command, array $params = null)
    {
        if ($params == null) {
            return $command;
        }

        foreach ($params as $param) {
            if (is_array( $param )) {
                foreach ($param as $p) {
                    $command[] = $p;
                }
            } else {
                $command[] = $param;
            }
        }

        return $command;
    }
}

/* End of file ClientAbstract.php */
