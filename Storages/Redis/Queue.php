<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis;


/**
 * Class Queue
 * @subpackage Avant\Storages\Redis
 */
class Queue extends Client
{
    protected $node = 'queue';

    /**
     * @return int
     */
    public function deleteAll()
    {
        $node = $this->getNode();

        // Do the primary first, to try to avoid issues if someone uses/recreates the queue while we're nuking it.
        $this->del( "$node:primary" );

        // Delete other(all)
        foreach (($keys = $this->keys( "$node:*" )) as $key) {
            $this->del( $key );
        }

        return count( $keys );
    }

    /**
     * @param $key
     * @return bool
     */
    public function deleteMessage($key)
    {
        $node = $this->getNode();
        if ($this->lrem( "$node:primary", 0, $key )) {
            $this->del( "$node:fetched:$key" );
            $this->del( "$node:value:$key" );

            return true;
        }

        return false;
    }

    /**
     * Fetch message
     * @param int $ttl Time To Lock, time pause where received message(s) will be not available for receive
     * @return mixed
     */
    public function fetch($ttl = 60)
    {
        $result    = null;
        $node      = $this->getNode();
        $threshold = time() - $ttl;

        /*
        $count = min($this->getMessageCount(), $limit);
        while ($count--) {
            $id = $this->rpoplpush("$node:primary", "$node:primary");
            if (empty($id)) break;

            $now     = time();
            $fetched = $this->getset("$node:fetched:$id", $now);

            if ($fetched < $threshold) {
                $result[] = $this->getMessage($id);
                continue;
            }

            if ($fetched < $now) {
                $this->set("$node:fetched:$id", $fetched);
            }
        }
        */

        $count = $this->getMessageCount();
        while ($count--) {
            $id = $this->rpoplpush( "$node:primary", "$node:primary" );
            if (empty( $id )) {
                break;
            }

            $now     = time();
            $fetched = $this->getset( "$node:fetched:$id", $now );

            if ($fetched < $threshold) {
                $result = $this->getMessage( $id );
                break;
            }

            if ($fetched < $now) {
                $this->set( "$node:fetched:$id", $fetched );
            }
        }

        return $result;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getMessage($key)
    {
        $node = $this->getNode();

        return unserialize( base64_decode( $this->get( "$node:value:$key" ) ) );
    }

    /**
     * @return int
     */
    public function getMessageCount()
    {
        $node = $this->getNode();

        return $this->llen( "$node:primary" );
    }

    /**
     * @return string
     */
    public function getNode()
    {
        return $this->node;
    }

    /**
     * @param            $message
     * @param bool|FALSE $override
     * @return mixed|string
     */
    public function setMessage($message, $override = false)
    {
        $node  = $this->getNode();
        $key   = ($message instanceof QueueMessage) ? $message->getId() : QueueMessage::createId();
        $value = base64_encode( serialize( $message ) );

        if ($this->exists( "$node:value:$key" )) {
            if ($override == true) {
                $this->set( "$node:value:$key", $value );
                $this->set( "$node:fetched:$key", 0 );
            }

            return $key;
        }

        $this->set( "$node:value:$key", $value );
        $this->set( "$node:fetched:$key", 0 );
        $this->lpush( "$node:primary", $key );

        return $key;
    }

    /**
     * @param string $node
     */
    public function setNode($node)
    {
        $this->node = $node;
    }
}

/* End of file Queue.php */
