<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis;

use Avant\Base\Predicable;


/**
 * Class QueueMessage
 * @subpackage Avant\Storages\Redis
 */
class QueueMessage extends Predicable implements QueueMessageInterface
{
    static $offset = 0;

    /**
     * @return string
     */
    public static function createId()
    {
        return md5( uniqid( __CLASS__, true ).'-'.++self::$offset );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        if (!$this->hasAttribute( 'id' )) {
            $this->setAttribute( 'id', self::createId() );
        }

        return $this->getAttribute( 'id', false );
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->setAttribute( 'id', $id );
    }
}

/* End of file QueueMessage.php */
