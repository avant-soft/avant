<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\Redis;


/**
 * Interface IODriverInterface
 * @subpackage Avant\Storages\Redis
 */
interface IODriverInterface
{
    /**
     * @param string[] $structure
     * @return mixed
     */
    public function send(array $structure);

    /**
     * @param array[] $structures
     * @return mixed
     */
    public function sendMulti(array $structures);

    /**
     * @param string[]              $structure
     * @param \Closure|string|array $callback
     * @return mixed
     */
    public function subscribe(array $structure, $callback);
}