<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO;

use Avant\Base\Configurable;
use Avant\Cache\Cache;
use Avant\Exception\EDatabaseError;
use Avant\Exception\EMatchError;
use Avant\Helpers\ArrayHelper;
use Avant\Http\LoggerTrait;
use Avant\Stdlib\Benchmark;
use Avant\Storages\PDO\Builder\BuilderQueryAbstract;
use Avant\Storages\PDO\Builder\BuilderSchemaAbstract;
use Avant\Storages\PDO\Exception\ESQLError;


/**
 * Class Database
 * @subpackage Avant\Storages\PDO
 *
 *
 * @property Driver $driver
 */
class Database extends Configurable
{
    use LoggerTrait;

    /**
     * @var Benchmark
     */
    protected $benchmark;
    /**
     * @var Cache
     */
    protected $cache;
    /**
     * @var bool
     */
    protected $cacheEnabled = false;
    /**
     * @var String
     */
    protected $cacheID;
    /**
     * @var array
     */
    protected $connections;
    /**
     * @var Driver
     */
    protected $driver;
    /**
     * @var string
     */
    protected $driverKey;
    /**
     * @var bool
     */
    protected $logging = true;
    /**
     * Total count of executed queries
     * @var integer
     */
    protected $queryCounter = 0;
    /**
     * @var array
     */
    protected $queryParams = [];
    /**
     * @var array
     */
    protected $queryRowsAffected;
    /**
     * String of last query
     * @var string
     */
    protected $queryStr = '';

    /**
     * Last prepared database query, and after the query is executed, the corresponding result set.
     * @var \PDOStatement
     */
    protected $sth;

    protected $logID = 'system';

    public function __construct(array $config = null, $strict = false, $context = null)
    {
        $this->benchmark = new Benchmark( ['collector' => false, 'notify' => false] );
        $this->cache     = new Cache();

        parent::__construct( $config, $strict, $context );
    }


    public function addConnection($key, $dsn, $user = null, $password = null, $options = [])
    {
        if (isset( $this->connections[$key] )) {
            throw new EDatabaseError( sprintf( 'A database has already be specified for key "%s"', $key ) );
        }

        $driverType = substr( $dsn, 0, strpos( $dsn, ':' ) );
        if (!$this->isSupported( $driverType )) {
            throw new EDatabaseError( sprintf( 'PDO Driver "%s" now is not supported.', $driverType ) );
        }

        $this->connections[$key] = new Driver( [
          'dsn'      => $dsn,
          'user'     => $user,
          'password' => $password,
          'options'  => $options,
        ] );
    }

    public function begin()
    {
        return $this->driver->begin();
    }

    /**
     * @param mixed $type
     *
     * @return Builder\BuilderQueryAbstract|mixed
     * @throws EDatabaseError
     */
    public function builderQuery($type = null)
    {
        $driverName = strtolower( $type !== null ? $type : $this->driver->getPDODriver() );

        return BuilderQueryAbstract::create( $driverName, ['owner' => $this] );
    }

    /**
     * @param mixed $type
     *
     * @return Builder\BuilderSchemaAbstract|mixed
     * @throws EDatabaseError
     */
    public function builderSchema($type = null)
    {
        $driverName = strtolower( $type !== null ? $type : $this->driver->getPDODriver() );

        return BuilderSchemaAbstract::create( $driverName, ['owner' => $this] );
    }

    public function commit()
    {
        return $this->driver->commit();
    }

    public function execute()
    {
        return $this->queryInternal( null );
    }

    public function fetch($args = [])
    {
        return $this->queryInternal( 'fetch', $args );
    }

    public function fetchAll($args = [])
    {
        return $this->queryInternal( 'fetchAll', $args );
    }

    public function fetchColumn($args = [])
    {
        return $this->queryInternal( 'fetchColumn', $args );
    }

    /**
     * @return Cache
     */
    public function getCache()
    {
        return $this->cache;
    }

    public function getCacheKey()
    {
        $result = '';
        $args   = func_get_args();
        foreach ($args as $arg) {
            if (is_array( $arg )) {
                $result .= serialize( $arg );
            } else {
                $result .= $arg;
            }
        }

        return md5( $result );
    }

    /**
     * @return Driver
     */
    public function getDriver()
    {
        return $this->driver;
    }

    public function getDriverName()
    {
        return $this->driver->getPDODriver();
    }

    public function getLastInsertID($sequence = null)
    {
        return $this->driver->getLastInsertID( $sequence );
    }

    /**
     * Get last query info
     * @return array
     */
    public function getLastQuery()
    {
        return [$this->queryStr, $this->queryParams, $this->queryRowsAffected];
    }

    /**
     * @param mixed $sql
     * @param mixed $params
     *
     * @return mixed|null|string
     */
    public function getSql($sql = null, $params = null)
    {
        if (empty( $sql )) {
            $sql = $this->queryStr;
        }

        if (empty( $params )) {
            $params = $this->queryParams;
        }

        if (count( $params ) > 0) {
            foreach ($params as $key => $value) {
                $pattern = is_integer( $key ) ? ':'.($key + 1) : $key;
                $sql     = str_replace( $pattern, $value, $sql );
            }
        }

        return $sql;
    }

    /**
     * @return boolean
     */
    public function isCacheEnabled()
    {
        return ($this->cacheEnabled && $this->getCache()->isEnabled());
    }

    public function isCacheable($sql)
    {
        return preg_match( '/^\s*(SELECT|SHOW|DESCRIBE)\b/i', $sql ) > 0;
    }

    /**
     * @return boolean
     */
    public function isLogging()
    {
        return $this->logging;
    }

    public function isSupported($driver)
    {
        static $drivers;
        if ($drivers == null) {
            $drivers = \PDO::getAvailableDrivers();
        }

        return in_array( $driver, $drivers );
    }

    public function loadSql($sql, $params = [])
    {

        foreach ($params as $key => &$value) {
            if (is_array( $value )) {
                $array  = array_values( $value );
                $pieces = [];
                array_walk( $array, function ($element) use (&$pieces) {
                    if (is_scalar( $element )) {
                        //$pieces[] = is_numeric($element) ? $element : $this->driver->getPDO()->quote($element);
                        $pieces[] = $this->driver->getPDO()->quote( $element );
                    }
                } );

                $value = join( ',', $pieces );
            } elseif (is_string( $value )) {
                $value = $this->driver->getPDO()->quote( $value );
            } else {
                continue;
            }

        }

        $this->queryStr    = $sql;
        $this->queryParams = $params;

        return $this;
    }

    public function prepare($sql = null, array $params = [], array $options = [])
    {
        return $this->sth = $this->driver->prepare( $sql, $params, $options );
    }

    public function rollback()
    {
        return $this->driver->rollback();
    }

    public function selectConnection($key)
    {
        if ($this->driverKey === $key) {
            return false;
        }

        if (!isset( $this->connections[$key] )) {
            throw new EMatchError( sprintf( 'Database "%s" not found in registry', $key ) );
        }

        $this->driver = $this->connections[$this->driverKey = $key];

        return true;
    }

    /**
     * @param $cacheEnabled
     *
     * @return $this
     */
    public function setCacheEnabled($cacheEnabled = false)
    {
        $this->cacheEnabled = $cacheEnabled;

        return $this;
    }

    /**
     * @param $logging
     *
     * @return $this
     */
    public function setLogging($logging)
    {
        $this->logging = $logging;

        return $this;
    }

    protected function queryInternal($method, $fetchArgs = null)
    {
        $sql = $this->getSql();

        if ($this->isCacheEnabled() && $this->isCacheable( $sql )) {
            $cacheKey = $this->getCacheKey( $sql, $method, $this->driver->getSignature() );
            if ($cached = $this->getCache()->get( $cacheKey )) {
                return $cached;
            }
        }

        try {
            $this->beforeQuery();
            $sth = $this->prepare( $sql, [] );
            $sth->execute();
            $result                  = empty( $method ) ? $sth->rowCount() : call_user_func_array( [$sth, $method], (array)$fetchArgs );
            $this->queryRowsAffected = is_array( $result ) ? count( $result ) : $sth->rowCount();
            $sth->closeCursor();
            $this->afterQuery();
        } catch (\PDOException $exception) {
            $errorInfo = $this->sth->errorInfo();
            $error     = new ESQLError( $errorInfo[2], $errorInfo[1] );
            if ($errorInfo[0] != $errorInfo[1]) {
                $error->setSQLState( $errorInfo[0] );
            }

            throw $error;
        }

        if ($this->isCacheEnabled() && isset( $cacheKey )) {
            $this->getCache()->save( $cacheKey, $result );
            $this->setCacheEnabled();
        }

        return $result;
    }

    protected function setCache($config)
    {
        if (is_array( $config )) {
            $this->cache->initialize( $config );
        }
    }

    protected function setConnections($connections)
    {
        if (is_array( $connections ) && count( $connections )) {
            foreach ($connections as $config) {
                list( $key, $dsn, $user, $password, $options ) = ArrayHelper::elements( ['key', 'dsn', 'user', 'password', 'options'], $config, [false, false, null, null, []] );
                if (!$key || !$dsn) {
                    continue;
                }
                $this->addConnection( $key, $dsn, $user, $password, $options );
            }
        }
    }

    protected function setDefault($key)
    {
        if (!empty( $key )) {
            $this->selectConnection( $key );
        }
    }

    private function afterQuery()
    {
        if ($this->logging == true) {
            $index = $this->queryCounter;
            $this->benchmark->stop( $key = 'query'.$index.'_execute' );

            $logMsg = [
              'id'            => $index,
              'sql'           => $this->getSql(),
              'elapsed'       => $this->benchmark->elapsed( $key ),
              'rows_affected' => $this->queryRowsAffected,
            ];

            /** Basic log */
            $this->logDebug( 'Database execute: '.join( ',', $logMsg ) );
        }
    }

    private function beforeQuery()
    {
        $this->queryCounter++;

        if ($this->logging == true) {
            $index = $this->queryCounter;
            $this->benchmark->start( 'query'.$index.'_execute' );
        }
    }

    public function getLogID(): string
    {
        return $this->logID;
    }

    public function setLogID(string $logID): self
    {
        $this->logID = $logID;
        return $this;
    } 
}

/* End of file Database.php */
