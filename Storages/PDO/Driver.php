<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO;

use Avant\Base\Predicable;
use Avant\Exception\EDatabaseError;
use Avant\Storages\PDO\Exception\ESQLError;


/**
 * Class PDO Driver
 * @subpackage Avant\Storages\PDO
 *
 * @property string $dsn
 * @property string $user
 * @property string $password
 * @property bool   $connected
 * @property array  $options
 * @property \PDO   $pdo
 */
class Driver extends Predicable
{
    protected $identifierDelimiter = "`";

    public function begin()
    {
        $this->connect();

        return $this->pdo->beginTransaction();
    }

    public function commit()
    {
        $this->connect();

        return $this->pdo->commit();
    }

    public function connect()
    {
        if ($this->connected == true) {
            return true;
        }

        try {
            $this->pdo = new \PDO( $this->dsn, $this->user, $this->password, $this->options );
            $this->pdo->setAttribute( \PDO::ATTR_STRINGIFY_FETCHES, true );
            $this->pdo->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $this->pdo->setAttribute( \PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC );
            $this->setEncoding();
            $this->connected = true;
        } catch (\PDOException $exception) {
            $matches = [];
            $dbname  = (preg_match( '/dbname=(\w+)/', $this->dsn, $matches )) ? $matches[1] : 'undefined';
            throw new EDatabaseError( 'Could not connect to database ('.$dbname.').', $exception->getCode() );
        }

        return $this->connected;
    }

    public function disconnect()
    {
        $this->pdo       = null;
        $this->connected = false;
    }

    /**
     * @param             $sql
     * @param             $params
     * @param array       $options
     * @return \PDOStatement|mixed
     * @throws EDatabaseError
     * @var \PDOStatement $statement
     */
    public function execute($sql, array $params = [], array $options = [])
    {
        $this->connect();

        try {
            $statement = $this->prepare( $sql, $options );

            if (count( $params )) {
                $this->bindParams( $statement, $params );
            }

            $statement->execute();

            return $statement;
        } catch (\PDOException $exception) {
            $err = new ESQLError( $exception->getMessage(), 0 );
            $err->setSQLState( $exception->getCode() );
            throw $err;
        }
    }

    public function getLastInsertID($sequence = null)
    {
        $this->connect();

        return (int)$this->pdo->lastInsertId( $sequence );
    }

    /**
     * Returns the underlying PHP PDO instance.
     *
     * @return \PDO
     */
    public function getPDO()
    {
        $this->connect();

        return $this->pdo;
    }

    /**
     * Returns the name of database driver for PDO.
     * Uses the PDO attribute DRIVER NAME to obtain the name of the
     * PDO driver.
     *
     * @return string
     */
    public function getPDODriver()
    {
        $this->connect();

        return $this->pdo->getAttribute( \PDO::ATTR_DRIVER_NAME );
    }

    public function getParamType($value)
    {
        if (is_null( $value )) {
            return \PDO::PARAM_NULL;
        } elseif (is_bool( $value )) {
            return \PDO::PARAM_BOOL;
        } elseif (is_int( $value )) {
            return \PDO::PARAM_INT;
        } else {
            return \PDO::PARAM_STR;
        }
    }

    public function getSignature()
    {
        return md5( $this->dsn.serialize( $this->params ) );
    }

    /**
     * Returns the version number of the database.
     *
     * @return mixed
     */
    public function getVersion()
    {
        $this->connect();

        return $this->pdo->getAttribute( \PDO::ATTR_CLIENT_VERSION );
    }

    public function prepare($sql, $params = [], $options = [])
    {
        $this->connect();

        $sth = $this->pdo->prepare( $sql, $options );
        if (count( $params )) {
            $this->bindParams( $sth, $params );
        }

        return $sth;
    }

    public function rollback()
    {
        $this->connect();

        return $this->pdo->rollBack();
    }

    /**
     * @param \PDOStatement $statement
     * @param array         $params
     */
    protected function bindParams($statement, $params = [])
    {
        foreach ($params as $key => &$value) {
            if (is_integer( $key )) {
                if (is_null( $value )) {
                    $statement->bindValue( $key + 1, null, \PDO::PARAM_NULL );
                } else {
                    $statement->bindParam( $key + 1, $value, $this->getParamType( $value ) );
                }
            } else {
                if (is_null( $value )) {
                    $statement->bindValue( $key, null, \PDO::PARAM_NULL );
                } else {
                    $statement->bindParam( $key, $value, $this->getParamType( $value ) );
                }
            }
        }
    }

    /**
     * Try to fix MySQL character encoding problems.
     * MySQL < 5.5 does not support proper 4 byte unicode but they
     * seem to have added it with version 5.5 under a different label: utf8mb4.
     * We try to select the best possible charset based on your version data.
     *
     * @return void
     */
    protected function setEncoding()
    {
        $driver  = $this->pdo->getAttribute( \PDO::ATTR_DRIVER_NAME );
        $version = floatval( $this->pdo->getAttribute( \PDO::ATTR_SERVER_VERSION ) );
        if ($driver === 'mysql') {
            $encoding = ($version >= 5.5) ? 'utf8mb4' : 'utf8';
            $this->pdo->setAttribute( \PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES '.$encoding ); //on every re-connect
            $this->pdo->exec( ' SET NAMES '.$encoding );                                       //also for current connection
        }
    }

}

/* End of file Driver.php */
