<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Exception;

use Avant\Exception\EDatabaseError;


/**
 * Class ESqlError
 * @subpackage Avant\Storages\PDO\Exception
 */
class ESQLError extends EDatabaseError
{
    /**
     * @var string
     */
    private $sqlState;

    /**
     * To String prints both code and SQL state.
     *
     * @return string
     */
    public function __toString()
    {
        return '['.$this->getSQLState().'] - '.$this->getMessage()."\n".$this->getTraceAsString();
    }

    /**
     * Returns an ANSI-92 compliant SQL state.
     *
     * @return string
     */
    public function getSQLState()
    {
        return $this->sqlState;
    }

    /**
     * Returns the raw SQL STATE, possibly compliant with
     * ANSI SQL error codes - but this depends on database driver.
     *
     * @param string $sqlState SQL state error code
     *
     * @return void
     */
    public function setSQLState($sqlState)
    {
        $this->sqlState = $sqlState;
    }
}

/* End of file ESqlError.php */
