<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder;

use Avant\Storages\PDO\Exception\ESQLError;


/**
 * Class BuilderQueryAbstract
 * @subpackage Avant\Storages\PDO\Builder
 */
abstract class BuilderQueryAbstract extends BuilderAbstract
{
    use BuilderTrait;

    const BRACKET_START = '(';
    const BRACKET_END   = ')';
    /**
     * Supported column builders
     * @var array
     */
    protected static $invokeBuilders = [
        'mysql' => 'Avant\Storages\PDO\Builder\Driver\MySQL\Query',
    ];
    protected        $bracketsPrev   = [];
    protected        $bracketsSource = null;
    protected        $calcFound      = false;
    protected        $distinct       = [];
    protected        $from           = [];
    protected        $groupBy        = [];
    protected        $having         = [];
    protected        $join           = [];
    protected        $like           = [];
    protected        $likeQuoteChr   = '';
    protected        $likeQuoteStr   = '';
    protected        $limit          = false;
    protected        $modifiers      = [];
    protected        $offset         = false;
    protected        $order          = false;
    protected        $orderBy        = [];
    protected        $randomKeyword  = '';
    protected        $select         = [];
    protected        $set            = [];
    protected        $tableAliased   = [];
    protected        $where          = [];
    protected        $whereKey       = null;

    /**
     * @param bool|TRUE $value
     *
     * @return $this
     * @deprecated
     */
    public function calcFound($value = true)
    {
        $this->calcFound = (is_bool($value)) ? $value : true;

        return $this;
    }

    public function composeArray() {}

    public function composeString() {}

    /**
     * @param string $table
     * @param string $where
     * @param mixed   $limit
     *
     * @return bool
     * @throws ESQLError
     */
    public function delete($table = '', $where = '', $limit = null)
    {

        if ($table == '') {
            if (!isset($this->from[0])) {
                return false;
            }
            $table = $this->from[0];
        } elseif (is_array($table)) {
            $deleteBatch = [];
            foreach ($table as $single_table) {
                $sql           = $this->delete($single_table, $where, $limit);
                $sql           = rtrim($sql, ';') . ';';
                $deleteBatch[] = $sql;
            }

            if (count($deleteBatch) > 0) {
                return implode("\n", $deleteBatch);
            }

            throw new ESQLError('SQL query is empty');
        } else {
            $table = $this->protectIdentifiers($table);
        }

        if ($where != '') {
            $this->where($where);
        }

        if ($limit != null) {
            $this->limit($limit);
        }

        if (count($this->where) == 0 && count($this->like) == 0) {
            throw new ESQLError('SQL query(delete) must use where condition');
        }

        $sql = $this->composeDelete($table, $this->where, $this->like, $this->limit);

        return $this->realize($sql);
    }

    /**
     * @param bool|TRUE $val
     *
     * @return $this
     * @deprecated
     */
    public function distinct($val = true)
    {
        $this->distinct = (is_bool($val)) ? $val : true;

        return $this;
    }

    /**
     * Execute sql
     *
     * @param string $sql
     * @param array  $params
     * @param string $method [execute|fetch|fetchAll]
     * @param array  $args
     *
     * @return string
     */
    public function execute($sql, array $params = [], $method = 'execute', array $args = [])
    {
        return $this->realize($sql, $params, $method, $args);
    }


    public function flush()
    {
        parent::flush();

        $this->from           = [];
        $this->groupBy        = [];
        $this->having         = [];
        $this->join           = [];
        $this->like           = [];
        $this->likeQuoteChr   = '';
        $this->likeQuoteStr   = '';
        $this->limit          = false;
        $this->offset         = false;
        $this->order          = false;
        $this->orderBy        = [];
        $this->randomKeyword  = '';
        $this->select         = [];
        $this->set            = [];
        $this->tableAliased   = [];
        $this->where          = [];
        $this->whereKey       = null;
        $this->bracketsSource = null;
        $this->bracketsPrev   = [];
        $this->modifiers = [];
    }

    /**
     * @param $from
     *
     * @return BuilderQueryAbstract
     */
    public function from($from)
    {
        foreach ((array)$from as $val) {
            if (strpos($val, ',') !== false) {
                foreach (explode(',', $val) as $v) {
                    $v = trim($v);
                    $this->tableAliases($v);
                    $this->from[] = $this->protectIdentifiers($v);
                }
            } else {
                $val = trim($val);

                // Extract any aliases that might exist.  We use this information
                // in the protectIdentifiers to know whether to add a table prefix
                $this->tableAliases($val);
                $this->from[] = $this->protectIdentifiers($val);
            }
        }

        return $this;
    }

    /**
     * @param $by
     *
     * @return BuilderQueryAbstract
     */
    public function groupBy($by)
    {
        if (is_string($by)) {
            $by = explode(',', $by);
        }

        foreach ($by as $val) {
            $val = trim($val);

            if ($val != '') {
                $this->groupBy[] = $this->protectIdentifiers($val);
            }
        }

        return $this;
    }

    /**
     * @param $str
     *
     * @return bool
     */
    public function hasOperator($str)
    {
        $str = trim($str);
        if (!preg_match("/(\s|<|>|!|=|is null|is not null)/i", $str)) {
            return false;
        }

        return true;
    }

    /**
     * @param           $key
     * @param string    $value
     * @param bool|TRUE $quote
     *
     * @return BuilderQueryAbstract
     */
    public function having($key, $value = '', $quote = true)
    {
        return $this->composeHaving($key, $value, 'AND ', $quote);
    }

    /**
     * @return BuilderQueryAbstract
     */
    public function havingBrackets()
    {
        return $this->brackets($this->having);
    }

    /**
     * @return BuilderQueryAbstract
     */
    public function havingBracketsEnd()
    {
        return $this->bracketsEnd($this->having);
    }

    /**
     * @param string     $table
     * @param mixed       $set
     * @param bool|FALSE $replaceIfExists
     *
     * @return bool
     */
    public function insert($table = '', $set = null, $replaceIfExists = false)
    {

        if (!is_null($set)) {
            if (is_array($set) && isset($set[0])) {
                $this->setAsBatch($set);
            } else {
                $this->set($set);
            }
        }

        if (count($this->set) == 0) {
            return false;
        }

        if ($table == '') {
            if (!isset($this->from[0])) {
                return false;
            }
            $table = $this->from[0];
        }

        if (isset($this->set[0])) {
            $sql = $this->composeInsertBatch($this->protectIdentifiers($table), $this->set, $replaceIfExists);
        } else {
            $sql = $this->composeInsert($this->protectIdentifiers($table), array_keys($this->set), array_values($this->set), $replaceIfExists);
        }

        return $this->realize($sql);
    }

    /**
     * @param        $table
     * @param        $rule
     * @param string $type
     *
     * @return BuilderQueryAbstract
     */
    public function join($table, $rule, $type = '')
    {
        if ($type != '') {
            $type = strtoupper(trim($type));

            if (!in_array($type, ['LEFT', 'RIGHT', 'OUTER', 'INNER', 'LEFT OUTER', 'RIGHT OUTER'])) {
                $type = '';
            } else {
                $type .= ' ';
            }
        }

        // Extract any aliases that might exist.  We use this information
        // in the _protect_identifiers to know whether to add a table prefix
        $this->tableAliases($table);

        // Strip apart the condition and protect the identifiers
        if (preg_match('/([\w\.]+)([\W\s]+)(.+)/', $rule, $match)) {
            $match[1] = $this->protectIdentifiers($match[1]);
            $match[3] = $this->protectIdentifiers($match[3]);

            $rule = $match[1] . $match[2] . $match[3];
        }

        // Assemble the JOIN statement
        $join = $type . 'JOIN ' . $this->protectIdentifiers($table) . ' ON ' . $rule;

        $this->join[] = $join;

        return $this;
    }

    /**
     * @param $table
     * @param $rule
     *
     * @return BuilderQueryAbstract
     */
    public function joinInner($table, $rule)
    {
        return $this->join($table, $rule, 'INNER');
    }

    /**
     * @param $table
     * @param $rule
     *
     * @return BuilderQueryAbstract
     */
    public function joinLeft($table, $rule)
    {
        return $this->join($table, $rule, 'LEFT');
    }

    /**
     * @param $table
     * @param $rule
     *
     * @return BuilderQueryAbstract
     */
    public function joinOuter($table, $rule)
    {
        return $this->join($table, $rule, 'OUTER');
    }

    /**
     * @param $table
     * @param $rule
     *
     * @return BuilderQueryAbstract
     */
    public function joinRight($table, $rule)
    {
        return $this->join($table, $rule, 'RIGHT');
    }

    /**
     * @param        $field
     * @param string $match
     * @param string $side
     *
     * @return mixed
     */
    public function like($field, $match = '', $side = 'both')
    {
        return $this->composeLike($field, $match, 'AND ', $side);
    }

    /**
     * @return BuilderQueryAbstract
     */
    public function likeBrackets()
    {
        return $this->brackets($this->like);
    }

    /**
     * @return BuilderQueryAbstract
     */
    public function likeBracketsEnd()
    {
        return $this->bracketsEnd($this->like);
    }

    /**
     * @param      $value
     * @param mixed $offset
     *
     * @return BuilderQueryAbstract
     */
    public function limit($value, $offset = null)
    {
        if (is_numeric($value) && $value > 0) {
            $this->limit = (int)$value;

            if (!empty($offset)) {
                $this->offset = (int)$offset;
            }
        } else {
            $this->limit  = false;
            $this->offset = false;
        }

        return $this;
    }

    /**
     * @param mixed $modifiers
     *
     * @return BuilderQueryAbstract
     */
    public function modifiers($modifiers = null)
    {
        if ($modifiers !== null) {
            if (is_string($modifiers)) {
                $modifiers = explode(' ', $modifiers);
            }

            foreach ($modifiers as $modifier) {
                $this->modifiers[] = trim(strtoupper($modifier));
            }
        }

        return $this;
    }

    /**
     * @param        $field
     * @param string $match
     * @param string $side
     *
     * @return BuilderQueryAbstract
     */
    public function notLike($field, $match = '', $side = 'both')
    {
        return $this->composeLike($field, $match, 'AND ', $side, 'NOT');
    }

    /**
     * @param $offset
     *
     * @return BuilderQueryAbstract
     */
    public function offset($offset)
    {
        $this->offset = (int)$offset;

        return $this;
    }

    /**
     * @param           $key
     * @param string    $value
     * @param bool|TRUE $quote
     *
     * @return BuilderQueryAbstract
     */
    public function orHaving($key, $value = '', $quote = true)
    {
        return $this->composeHaving($key, $value, 'OR ', $quote);
    }

    /**
     * @param        $field
     * @param string $match
     * @param string $side
     *
     * @return BuilderQueryAbstract
     */
    public function orLike($field, $match = '', $side = 'both')
    {
        return $this->composeLike($field, $match, 'OR ', $side);
    }

    /**
     * @param        $field
     * @param string $match
     * @param string $side
     *
     * @return BuilderQueryAbstract
     */
    public function orNotLike($field, $match = '', $side = 'both')
    {
        return $this->composeLike($field, $match, 'OR ', $side, 'NOT');
    }

    /**
     * @param           $key
     * @param mixed      $value
     * @param bool|TRUE $escape
     *
     * @return BuilderQueryAbstract
     */
    public function orWhere($key, $value = null, $escape = true)
    {
        return $this->composeWhere($key, $value, 'OR ', $escape);
    }

    /**
     * @param string       $key
     * @param string|array $values
     *
     * @return BuilderQueryAbstract
     */
    public function orWhereIn($key = null, $values = null)
    {
        return $this->composeWhereIn($key, $values, false, 'OR ');
    }

    /**
     * @param string       $key
     * @param string|array $values
     *
     * @return BuilderQueryAbstract
     */
    public function orWhereNotIn($key = null, $values = null)
    {
        return $this->composeWhereIn($key, $values, true, 'OR ');
    }

    /**
     * @param        $order
     * @param string $direction
     *
     * @return $this
     */
    public function orderBy($order, $direction = '')
    {
        if (strtolower($direction) == 'random') {
            $order     = '';
            $direction = $this->randomKeyword;
        } elseif (trim($direction) != '') {
            $direction = (in_array(strtoupper(trim($direction)), ['ASC', 'DESC'], true)) ? ' ' . $direction : ' ASC';
        }


        if (strpos($order, ',') !== false) {
            $temp = [];
            foreach (explode(',', $order) as $part) {
                $part = trim($part);
                if (!in_array($part, $this->tableAliased)) {
                    $part = $this->protectIdentifiers(trim($part));
                }

                $temp[] = $part;
            }

            $order = implode(', ', $temp);
        } elseif ($direction != $this->randomKeyword) {
            $order = $this->protectIdentifiers($order);
        }

        $statement       = $order . $direction;
        $this->orderBy[] = $statement;

        return $this;
    }

    /**
     * @param bool|FALSE $value
     *
     * @return $this
     */
    public function prepare($value = false)
    {
        $this->prepare = $value;

        return $this;
    }

    /**
     * @param array $args
     *
     * @return mixed
     */
    public function row(array $args = [])
    {
        return $this->realize($this->composeSelect(), [], 'fetch', $args);
    }

    /**
     * @param array $args
     *
     * @return mixed
     */
    public function rowColumn(array $args = [])
    {
        return $this->realize($this->composeSelect(), [], 'fetchColumn', $args);
    }

    /**
     * @param array $args
     *
     * @return mixed
     */
    public function rows(array $args = [])
    {
        return $this->realize($this->composeSelect(), [], 'fetchAll', $args);
    }

    /**
     * @param string $select
     * @param mixed   $quoted
     *
     * @return $this
     */
    public function select($select = '*', $quoted = null)
    {
        if (is_string($select)) {
            $select = explode(',', $select);
        }

        foreach ($select as $identifier) {
            $identifier = trim($identifier);
            if ($identifier != '') {
                $this->select[$identifier] = $quoted;
            }
        }

        return $this;
    }

    /**
     * @param            $key
     * @param mixed       $value
     * @param bool|TRUE  $quote
     * @param bool|FALSE $isBatch
     *
     * @return $this
     */
    public function set($key, $value = null, $quote = true, $isBatch = false)
    {
        $key = $this->objectToArray($key);

        if (!is_array($key)) {
            $key = [(string)$key => $value];
        }

        if ($isBatch == true) {
            $record = [];
            foreach ($key as $_key => $_value) {
                $record[$this->protectIdentifiers($_key)] = $quote !== false ? $this->quote($_value) : $_value;
            }
            ksort($record);
            $this->set[] = $record;
        } else {
            foreach ($key as $_key => $_value) {
                $this->set[$this->protectIdentifiers($_key)] = $quote !== false ? $this->quote($_value) : $_value;
            }
        }


        return $this;
    }

    /**
     * @param           $key
     * @param mixed      $value
     * @param bool|TRUE $quote
     *
     * @return BuilderQueryAbstract
     */
    public function setAsBatch($key, $value = null, $quote = true)
    {
        if (is_array($key) && isset($key[0])) {
            foreach ($key as $batch) {
                $this->setAsBatch($batch, null, $quote);
            }

            return $this;
        }

        return $this->set($key, $value, $quote, true);
    }

    /**
     * @param string $table
     *
     * @return bool
     */
    public function truncate($table = '')
    {
        if ($table == '') {
            if (!isset($this->from[0])) {
                return false;
            }

            $table = $this->from[0];
        } else {
            $table = $this->protectIdentifiers($table);
        }

        $sql = $this->composeTruncate($table);

        return $this->realize($sql);
    }

    /**
     * @param string $table
     * @param mixed   $set
     * @param mixed   $where
     * @param mixed   $whereKey
     * @param mixed   $limit
     *
     * @return bool
     */
    public function update($table = '', $set = null, $where = null, $whereKey = null, $limit = null)
    {
        if (!is_null($set)) {
            if (is_array($set) && isset($set[0])) {
                $this->setAsBatch($set);
            } else {
                $this->set($set);
            }
        }

        if (count($this->set) == 0) {
            return false;
        }

        if ($table == '') {
            if (!isset($this->from[0])) {
                return false;
            }
            $table = $this->from[0];
        }

        if ($where != null) {
            $this->where($where);
        }

        if ($limit != null) {
            $this->limit($limit);
        }

        if ($whereKey != null) {
            $this->whereKey($whereKey);
        }

        if (isset($this->set[0])) {
            $sql = $this->composeUpdateBatch($this->protectIdentifiers($table), $this->set, $this->where, $this->protectIdentifiers($this->whereKey), $this->orderBy, $this->limit);
        } else {
            $sql = $this->composeUpdate($this->protectIdentifiers($table), $this->set, $this->where, $this->orderBy, $this->limit);
        }

        return $this->realize($sql);
    }

    /**
     * @param           $key
     * @param mixed      $value
     * @param bool|TRUE $escape
     *
     * @return mixed|BuilderQueryAbstract
     */
    public function where($key, $value = null, $escape = true)
    {
        return $this->composeWhere($key, $value, 'AND ', $escape);
    }

    /**
     * @return BuilderQueryAbstract
     */
    public function whereBrackets()
    {
        return $this->brackets($this->where);
    }

    /**
     * @return BuilderQueryAbstract
     */
    public function whereBracketsEnd()
    {
        return $this->bracketsEnd($this->where);
    }

    /**
     * @param string       $key
     * @param string|array $values
     *
     * @return BuilderQueryAbstract
     */
    public function whereIn($key = null, $values = null)
    {
        return $this->composeWhereIn($key, $values);
    }

    /**
     * @param $key
     *
     * @return BuilderQueryAbstract
     */
    public function whereKey($key)
    {
        if (is_string($key)) {
            $this->whereKey = $key;
        }

        return $this;
    }

    /**
     * @param string       $key
     * @param string|array $values
     *
     * @return BuilderQueryAbstract
     */
    public function whereNotIn($key = null, $values = null)
    {
        return $this->composeWhereIn($key, $values, true);
    }

    /**
     * @param $source
     *
     * @return BuilderQueryAbstract
     */
    protected function brackets(&$source)
    {
        $source[] = self::BRACKET_START;

        $this->bracketsPrev[] = $this->bracketsSource;
        $this->bracketsSource = &$source;

        return $this;
    }

    /**
     * @param $value
     */
    protected function bracketsAccept($value)
    {
        if ($this->bracketsSource !== null) {
            $this->bracketsSource[] = $value;
        }
    }

    /**
     * @param $source
     *
     * @return BuilderQueryAbstract
     */
    protected function bracketsEnd(&$source)
    {
        $source[] = self::BRACKET_END;

        $bracketsPrev         = array_pop($this->bracketsPrev);
        $this->bracketsSource = &$bracketsPrev;

        return $this;
    }

    abstract protected function composeDelete($tableName, $where = [], $like = [], $limit = false);

    abstract protected function composeHaving($key, $value = '', $type = 'AND ', $quote = true);

    abstract protected function composeInsert($tableName, $keys, $values, $replaceIfExists = false);

    abstract protected function composeInsertBatch($tableName, $rows, $replaceIfExists = false);

    abstract protected function composeLike($field, $match = '', $type = 'AND ', $side = 'both', $not = '');

    abstract protected function composeLimit($sql, $limit, $offset);

    abstract protected function composeSelect();

    abstract protected function composeTruncate($tableName);

    abstract protected function composeUpdate($tableName, $values, $where = null, $orderBy = [], $limit = false);

    abstract protected function composeUpdateBatch($tableName, $values, $where = null, $whereKey = null, $orderBy = [], $limit = false);

    abstract protected function composeWhere($key, $value = null, $type = 'AND ', $quote = null);

    /**
     * @param string       $key
     * @param string|array $values
     * @param bool|FALSE   $not
     * @param string       $type
     *
     * @return mixed
     */
    abstract protected function composeWhereIn($key, $values = null, $not = false, $type = 'AND ');

    /**
     * @return bool
     */
    protected function hasBrackets()
    {
        return ($this->bracketsSource !== null);
    }

    protected function objectToArray($object)
    {
        if (!is_object($object)) {
            return $object;
        }

        $array = [];
        foreach (get_object_vars($object) as $key => $val) {
            // There are some built in keys we need to ignore for this conversion
            if (!is_object($val) && !is_array($val) && $key != '_parent_name') {
                $array[$key] = $val;
            }
        }

        return $array;
    }

    protected function objectToArrayBatch($object)
    {
        if (!is_object($object)) {
            return $object;
        }

        $array  = [];
        $vars   = get_object_vars($object);
        $fields = array_keys($vars);

        foreach ($fields as $val) {
            // There are some built in keys we need to ignore for this conversion
            if ($val != '_parent_name') {

                $i = 0;
                foreach ($vars[$val] as $data) {
                    $array[$i][$val] = $data;
                    $i++;
                }
            }
        }

        return $array;
    }

    protected function tableAliases($table)
    {
        if (is_array($table)) {
            foreach ($table as $t) {
                $this->tableAliases($t);
            }

            return true;
        }

        // Does the string contain a comma?  If so, we need to separate
        // the string into discreet statements
        if (strpos($table, ',') !== false) {
            return $this->tableAliases(explode(',', $table));
        }

        // if a table alias is used we can recognize it by a space
        if (strpos($table, " ") !== false) {
            // if the alias is written with the AS keyword, remove it
            $table = preg_replace('/\s+AS\s+/i', ' ', $table);

            // Grab the alias
            $table = trim(strrchr($table, " "));

            // Store the alias, if it doesn't already exist
            if (!in_array($table, $this->tableAliased)) {
                $this->tableAliased[] = $table;
            }

            return true;
        }

        return false;
    }
}

/* End of file BuilderQueryAbstract.php */
