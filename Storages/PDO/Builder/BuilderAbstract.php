<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder;

use Avant\Base\Configurable;
use Avant\Exception\EDatabaseError;
use Avant\Storages\PDO\Database;


/**
 * Class BuilderAbstract
 * @subpackage Avant\Storages\PDO
 */
abstract class BuilderAbstract extends Configurable implements BuilderInterface
{
    /**
     * Supported builders
     * @var array
     */
    protected static $invokeBuilders = [];
    protected        $owner;
    protected        $prepare        = false;

    /**
     * Factory of builders
     *
     * @param string $driverType
     * @param array  $config
     *
     * @return BuilderAbstract|mixed
     * @throws EDatabaseError
     */
    public static function create($driverType, $config = [])
    {
        $driverType = strtolower( $driverType );
        if (isset( static::$invokeBuilders[$driverType] )) {
            $class = static::$invokeBuilders[$driverType];

            return new $class( $config );
        }
        throw new EDatabaseError( sprintf( 'Driver type "%s" currently is not supported', $driverType ) );
    }

    /**
     * @return array
     */
    public function __toArray()
    {
        return $this->composeArray();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->composeString();
    }

    abstract public function composeArray();

    abstract public function composeString();

    /**
     * @return Database
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param Database $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    protected function flush()
    {
        $this->prepare = false;
    }

    protected function realize($sql, array $params = [], $method = 'execute', array $args = [])
    {
        $result = $this->prepare == true ? $sql : $this->getOwner()->loadSql( $sql, $params )->$method( $args );
        $this->flush();

        return $result;
    }


}

/* End of file BuilderAbstract.php */
