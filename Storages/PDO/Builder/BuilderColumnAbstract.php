<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder;

/**
 * Class BuilderColumnAbstract
 *
 * @subpackage Avant\Storages\PDO\Builder
 */
abstract class BuilderColumnAbstract extends BuilderAbstract
{
    const TYPE_PK          = 'pk';
    const TYPE_BIGPK       = 'bigpk';
    const TYPE_STRING      = 'string';
    const TYPE_TEXT        = 'text';
    const TYPE_MEDIUM_TEXT = 'mediumtext';
    const TYPE_LONG_TEXT   = 'longtext';
    const TYPE_TINYINT     = 'tinyint';
    const TYPE_SMALLINT    = 'smallint';
    const TYPE_INTEGER     = 'integer';
    const TYPE_BIGINT      = 'bigint';
    const TYPE_FLOAT       = 'float';
    const TYPE_DOUBLE      = 'double';
    const TYPE_DECIMAL     = 'decimal';
    const TYPE_DATETIME    = 'datetime';
    const TYPE_TIMESTAMP   = 'timestamp';
    const TYPE_TIME        = 'time';
    const TYPE_DATE        = 'date';
    const TYPE_BINARY      = 'binary';
    const TYPE_BOOLEAN     = 'boolean';
    const TYPE_MONEY       = 'money';
    const TYPE_JSON        = 'json';

    const LOCATE_FIRST = 1;
    const LOCATE_AFTER = 2;

    /**
     * Supported column builders
     *
     * @var array
     */
    protected static $invokeBuilders
      = [
        'mysql' => 'Avant\Storages\PDO\Builder\Driver\MySQL\Column',
      ];
    /**
     * Column name
     *
     * @var string
     */
    protected $columnName;

    /**
     * The name of the previous column (CHANGE | MODIFY)
     *
     * @var string
     */
    protected $columnPrevious;

    /**
     * Comment for column
     *
     * @var null|string
     */
    protected $comment = null;
    /**
     * @var integer|string|array column size or precision definition. This is what goes into the parenthesis after
     * the column type. This can be either a string, an integer or an array. If it is an array, the array values will
     * be joined into a string separated by comma.
     */
    protected $constraint;
    /**
     * @var mixed default value of the column.
     */
    protected $default;
    /**
     * @var boolean whether the column is not nullable. If this is `true`, a `NOT NULL` constraint will be added.
     */
    protected $isNotNull = false;
    /**
     * @var boolean whether the column values should be unique. If this is `true`, a `UNIQUE` constraint will be added.
     */
    protected $isUnique = false;
    /**
     * @var bool for column type definition such as INTEGER, SMALLINT, etc.
     */
    protected $isUnsigned = false;
    /**
     * @var int
     */
    protected $location;
    /**
     * @var string the column type definition such as INTEGER, VARCHAR, DATETIME, etc.
     */
    protected $type;

    /**
     * @param string $columnName
     *
     * @return $this
     */
    public function after($columnName)
    {
        $this->location       = self::LOCATE_AFTER;
        $this->columnPrevious = $columnName;

        return $this;
    }

    /**
     * @param string $str
     *
     * @return $this
     */
    public function comment($str)
    {
        $this->comment = $str;

        return $this;
    }

    public function composeArray()
    {
        return [];
    }

    public function composeString()
    {
        return
          $this->composeName().
          $this->composeType().
          $this->composeNotNull().
          $this->composeUnique().
          $this->composeDefault().
          $this->composeComment().
          $this->composeLocation();
    }

    public function defaultValue($default, $quoted = true)
    {
        $default       = str_replace( "'", "\\'", $default );
        $this->default = $quoted == true ? "'$default'" : $default;

        return $this;
    }

    public function first()
    {
        $this->location       = self::LOCATE_FIRST;
        $this->columnPrevious = null;

        return $this;
    }

    public function name($str)
    {
        $this->columnName = $str;

        return $this;
    }

    public function notNull($val = true)
    {
        $this->isNotNull = $val;

        return $this;
    }

    public function unique($val = true)
    {
        $this->isUnique = $val;

        return $this;
    }

    public function unsigned($val = true)
    {
        $this->isUnsigned = $val;

        return $this;
    }

    abstract protected function composeComment();

    abstract protected function composeDefault();

    abstract protected function composeLocation();

    abstract protected function composeName();

    abstract protected function composeNotNull();

    abstract protected function composeType();

    abstract protected function composeUnique();
}

/* End of file BuilderColumnAbstract.php */
