<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder\Driver\MySQL;

use Avant\Exception\EDatabaseError;
use Avant\Storages\PDO\Builder\BuilderColumnAbstract;
use Avant\Storages\PDO\Builder\BuilderIndexAbstract;
use Avant\Storages\PDO\Builder\BuilderSchemaAbstract;
use Avant\Storages\PDO\Builder\BuilderTrait;


/**
 * Class Forge
 * @subpackage Avant\Storages\PDO\Driver\MySQL
 */
final class Schema extends BuilderSchemaAbstract
{
    use BuilderTrait;

    protected function composeAlterTableColumns($alterSpec, $tableName, $columns)
    {
        $alterSpec = strtoupper( $alterSpec );
        $sql       = sprintf( 'ALTER TABLE %s %s;', $this->protectIdentifiers( $tableName ), $this->composeColumns( $columns, $alterSpec ) );

        return $sql;
    }

    protected function composeAlterTableKeys($alterSpec, $tableName, $keys)
    {
        $alterSpec = strtoupper( $alterSpec );
        $sql       = sprintf( 'ALTER TABLE %s %s;', $this->protectIdentifiers( $tableName ), $this->composeKeys( $keys, $alterSpec ) );

        return $sql;
    }

    protected function composeAlterTableName($fromTableName, $toTableName)
    {
        $sql = sprintf( 'ALTER TABLE %s RENAME TO %s;', $this->protectIdentifiers( $fromTableName ), $this->protectIdentifiers( $toTableName ) );

        return $sql;
    }

    protected function composeColumn($column, $attributes = [], $alterSpec = null)
    {
        $sql = "\n\t";

        if ($alterSpec != null) {
            $sql .= $alterSpec.' ';
        }

        $sql .= $this->protectIdentifiers( $column );

        if ($alterSpec != null && strstr( $alterSpec, 'DROP' ) !== false) {
            return $sql;
        }

        if (is_array( $attributes ) && !empty( $attributes )) {

            $attributes = array_change_key_case( $attributes, CASE_UPPER );

            if (array_key_exists( 'NAME', $attributes )) {
                $sql .= ' '.$this->protectIdentifiers( $attributes['NAME'] ).' ';
            }

            if (array_key_exists( 'TYPE', $attributes )) {
                $sql .= ' '.$attributes['TYPE'];

                if (array_key_exists( 'CONSTRAINT', $attributes )) {
                    switch ($attributes['TYPE']) {
                        case 'decimal':
                        case 'float':
                        case 'numeric':
                            $sql .= '('.implode( ',', $attributes['CONSTRAINT'] ).')';
                            break;

                        case 'enum':
                        case 'set':
                            $sql .= '("'.implode( '","', $attributes['CONSTRAINT'] ).'")';
                            break;

                        default:
                            $sql .= '('.$attributes['CONSTRAINT'].')';
                    }
                }

                if (array_key_exists( 'UNSIGNED', $attributes ) && $attributes['UNSIGNED'] === true) {
                    $sql .= ' UNSIGNED';
                }

                if (array_key_exists( 'DEFAULT', $attributes )) {
                    $sql .= ' DEFAULT \''.$attributes['DEFAULT'].'\'';
                }

                if (array_key_exists( 'NULL', $attributes ) && $attributes['NULL'] === true) {
                    $sql .= ' NULL';
                } else {
                    $sql .= ' NOT NULL';
                }

                if (array_key_exists( 'AUTO_INCREMENT', $attributes ) && $attributes['AUTO_INCREMENT'] === true) {
                    $sql .= ' AUTO_INCREMENT PRIMARY KEY';
                }
            }
        } elseif ($attributes instanceof BuilderColumnAbstract) {
            $sql .= ' '.$this->protectIdentifiers( $attributes );
        }


        return $sql;
    }

    protected function composeColumns($columns, $alterSpec = null)
    {
        $columns_count = 0;
        $sql           = '';

        foreach ($columns as $column => $attributes) {
            // Numeric field names aren't allowed in databases, so if the key is
            // numeric, we know it was assigned by PHP and the developer manually
            // entered the field information, so we'll simply add it to the list
            if (is_numeric( $column )) {
                $sql .= $this->composeColumn( $attributes, [], $alterSpec );
            } else {
                $sql .= $this->composeColumn( $column, $attributes, $alterSpec );
            }

            // don't add a comma on the end of the last field
            if (++$columns_count < count( $columns )) {
                $sql .= ',';
            }
        }

        return $sql;
    }

    protected function composeCreateDatabase($dbName, $ifNotExists = true, $charSet = null, $collate = null)
    {
        if (empty( $dbName )) {
            throw new EDatabaseError( 'Database name can not be empty.' );
        }

        $sql = 'CREATE DATABASE ';

        if ($ifNotExists == true) {
            $sql .= 'IF NOT EXISTS ';
        }

        $sql .= $this->quoteIdentifier( $dbName );

        if (!empty( $charSet )) {
            $sql .= ' CHARACTER SET '.$charSet;
        }

        if (!empty( $collate )) {
            $sql .= ' COLLATE '.$collate;
        }

        return $sql.';';
    }

    protected function composeCreateTable($tableName, $columns, $keys, $ifNotExists = true)
    {
        $sql = 'CREATE TABLE ';

        if ($ifNotExists === true) {
            $sql .= 'IF NOT EXISTS ';
        }

        $sql .= $this->quoteIdentifier( $tableName )." (";
        $sql .= $this->composeColumns( $columns );
        $sql .= $this->composeKeys( $keys );
        $sql .= ");";

        return $sql;
    }

    protected function composeDescribeTable($tableName)
    {
        return "DESCRIBE $tableName";
    }

    protected function composeDropDatabase($dbName, $ifNotExists = true)
    {
        if (empty( $dbName )) {
            throw new EDatabaseError( 'Database name can not be empty.' );
        }

        $sql = 'DROP DATABASE ';
        if ($ifNotExists == true) {
            $sql .= 'IF EXISTS ';
        }
        $sql .= $this->quoteIdentifier( $dbName ).';';

        return $sql;
    }

    protected function composeDropIndex($indexName, $tableName, $indexPrimaryKey = false)
    {
        if ($indexPrimaryKey == true) {
            return 'ALTER TABLE '.$this->protectIdentifiers( $tableName ).' DROP CONSTRAINT '.$this->protectIdentifiers( $indexName );
        }

        return 'DROP INDEX '.$this->protectIdentifiers( $indexName ).' ON '.$this->protectIdentifiers( $tableName );
    }

    protected function composeDropTable($tableName, $ifNotExists = true)
    {
        if (empty( $tableName )) {
            throw new EDatabaseError( 'Table name can not be empty.' );
        }

        $sql = 'DROP TABLE ';
        if ($ifNotExists == true) {
            $sql .= 'IF EXISTS ';
        }
        $sql .= $this->quoteIdentifier( $tableName );

        return $sql.";\n";
    }

    protected function composeExistsTable($tableName)
    {
        if (empty( $tableName )) {
            throw new EDatabaseError( 'Table name can not be empty.' );
        }

        $sql = "SHOW TABLES LIKE '{$tableName}'";

        return $sql.";\n";
    }

    protected function composeKey($key, $alterSpec = null)
    {
        $sql = "\n\t";

        if ($alterSpec != null) {
            $sql .= $alterSpec.' ';
        }

        // if key is array
        if (is_array( $key )) {
            $attributes = array_change_key_case( $key, CASE_UPPER );

            $column = [];
            if (array_key_exists( 'COLUMN', $attributes )) {
                if (is_array( $attributes['COLUMN'] )) {
                    $column = $attributes['COLUMN'];
                } elseif (is_string( $attributes['COLUMN'] )) {
                    $column[] = $attributes['COLUMN'];
                }
            }

            $indexName = implode( '_', $column );
            if (array_key_exists( 'INDEXNAME', $attributes )) {
                $indexName = $attributes['INDEXNAME'];
            }

            $type      = 'INDEX';
            $isPrimary = false;
            if (array_key_exists( 'TYPE', $attributes )) {
                if (strpos( 'PRIMARY', strtoupper( $attributes['TYPE'] ) ) === true || strtoupper( $attributes['TYPE'] ) == 'PRIMARY') {
                    $type      = 'PRIMARY KEY';
                    $isPrimary = true;
                } else {
                    $type = strtoupper( $attributes['TYPE'] );
                }
            }

            $indexName = $this->protectIdentifiers( $indexName );
            $column    = $this->protectIdentifiers( $column );

            $sql .= ' '.$type.($isPrimary != true ? ' '.$indexName : '').' ('.implode( ',', $column ).')';
        } //if key is IndexBuilder
        elseif ($key instanceof BuilderIndexAbstract) {
            $sql .= ' '.$key;
        }

        return $sql;
    }

    protected function composeKeys($keys, $alterSpec = null)
    {
        $keys_count = 0;
        $sql        = '';

        foreach ($keys as $key => $attributes) {
            if (is_numeric( $key )) {
                $sql .= $this->composeKey( $attributes, $alterSpec );
            } else {
                $sql .= $this->composeKey( $key, $alterSpec );
            }

            // don't add a comma on the end of the last field
            if (++$keys_count < count( $keys )) {
                $sql .= ',';
            }
        }

        return ($alterSpec == null && $keys_count > 0 ? ",\n\t" : "").$sql;
    }

    protected function composeShowDatabases()
    {
        return 'SHOW DATABASES';
    }

    protected function composeShowIndexes($tableName, $dbName = null)
    {
        $sql = 'SHOW INDEX FROM '.$tableName;
        if (is_string( $dbName )) {
            $sql .= ' FROM '.$dbName;
        }

        return $sql;
    }

    protected function composeShowTables($dbName = null)
    {
        $sql = 'SHOW TABLES';
        if (is_string( $dbName )) {
            $sql .= ' FROM '.$dbName;
        }

        return $sql;
    }
}

/* End of file Forge.php */
