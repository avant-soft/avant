<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder\Driver\MySQL;

use Avant\Exception\EDatabaseError;
use Avant\Storages\PDO\Builder\BuilderColumnAbstract;
use Avant\Storages\PDO\Builder\BuilderTrait;

/**
 * Class BuilderColumn
 *
 * @package  Avant
 */
final class Column extends BuilderColumnAbstract
{
    use BuilderTrait;

    protected $mapType
      = [
        self::TYPE_PK          => 'int(%s)%s NOT NULL AUTO_INCREMENT PRIMARY KEY',
        self::TYPE_BIGPK       => 'bigint(%s)%s NOT NULL AUTO_INCREMENT PRIMARY KEY',
        self::TYPE_STRING      => 'varchar(%s)',
        self::TYPE_TEXT        => 'text',
        self::TYPE_MEDIUM_TEXT => 'mediumtext',
        self::TYPE_LONG_TEXT   => 'longtext',
        self::TYPE_TINYINT     => 'tinyint(%s)%s',
        self::TYPE_SMALLINT    => 'smallint(%s)%s',
        self::TYPE_INTEGER     => 'int(%s)%s',
        self::TYPE_BIGINT      => 'bigint(%s)%s',
        self::TYPE_FLOAT       => 'float(%s)%s',
        self::TYPE_DOUBLE      => 'double(%s)%s',
        self::TYPE_DECIMAL     => 'decimal(%s)%s',
        self::TYPE_DATETIME    => 'datetime',
        self::TYPE_TIMESTAMP   => 'timestamp',
        self::TYPE_TIME        => 'time',
        self::TYPE_DATE        => 'date',
        self::TYPE_BINARY      => 'blob',
        self::TYPE_BOOLEAN     => 'tinyint(%s)%s',
        self::TYPE_MONEY       => 'decimal(%s)%s',
        self::TYPE_JSON        => 'json',
      ];

    protected $mapTypeDefaults
      = [
        self::TYPE_PK          => 11,
        self::TYPE_BIGPK       => 20,
        self::TYPE_STRING      => 255,
        self::TYPE_TEXT        => null,
        self::TYPE_MEDIUM_TEXT => null,
        self::TYPE_LONG_TEXT   => null,
        self::TYPE_TINYINT     => 3,
        self::TYPE_SMALLINT    => 6,
        self::TYPE_INTEGER     => 11,
        self::TYPE_BIGINT      => 20,
        self::TYPE_FLOAT       => [10, 0],
        self::TYPE_DOUBLE      => [10, 0],
        self::TYPE_DECIMAL     => [10, 0],
        self::TYPE_DATETIME    => null,
        self::TYPE_TIMESTAMP   => null,
        self::TYPE_TIME        => null,
        self::TYPE_DATE        => null,
        self::TYPE_BINARY      => null,
        self::TYPE_BOOLEAN     => 1,
        self::TYPE_MONEY       => [19, 4],
        self::TYPE_JSON        => null,
      ];

    protected function composeComment()
    {
        if (empty( $this->comment ) || is_string( $this->comment ) != true) {
            return '';
        }

        return " COMMENT '".addslashes( $this->comment )."'";
    }

    protected function composeConstraint($type)
    {
        $result = $this->constraint;

        if ($result === null || $result === []) {
            $result = isset( $this->mapTypeDefaults[$type] ) ? $this->mapTypeDefaults[$type] : '';
        }

        if (is_array( $result )) {
            $result = implode( ',', array_slice( $result, 0, 2 ) );
        } elseif (is_float( $result )) {
            $result = implode( ',', array_slice( explode( '.', $result ), 0, 2 ) );
        }

        return $result;
    }

    protected function composeDefault()
    {
        if ($this->default === null) {
            return '';
        }

        $result = ' DEFAULT ';
        switch (gettype( $this->default )) {
            case 'integer':
                $result .= (string)$this->default;
                break;
            case 'double':
            case 'float':
                $result .= str_replace( ',', '.', (string)$this->default );
                break;
            case 'boolean':
                $result .= $this->default ? 'TRUE' : 'FALSE';
                break;
            default:
                $result .= $this->default;
        }

        return $result;
    }

    protected function composeLocation()
    {
        $result = '';
        if ($this->location === self::LOCATE_FIRST) {
            $result = ' FIRST';
        } elseif ($this->location === self::LOCATE_AFTER && !empty( $this->columnPrevious )) {
            $result = ' AFTER '.$this->protectIdentifiers( $this->columnPrevious );
        }

        return $result;
    }

    protected function composeName()
    {
        if ($this->columnName === null) {
            return '';
        }

        return $this->columnName.' ';
    }

    protected function composeNotNull()
    {
        return $this->isNotNull == true ? ' NOT NULL' : ($this->type != self::TYPE_PK && $this->type != self::TYPE_BIGPK ? ' NULL' : '');
    }

    protected function composeType()
    {
        if (isset( $this->mapType[$this->type] )) {
            return sprintf( $this->mapType[$this->type], $this->composeConstraint( $this->type ), $this->isUnsigned ? ' UNSIGNED' : '' );
        }

        throw new EDatabaseError( sprintf( 'Column type "%s" is not supported', $this->type ) );
    }

    protected function composeUnique()
    {
        return $this->isUnique ? ' UNIQUE' : '';
    }
}

/* End of file BuilderColumn.php */
