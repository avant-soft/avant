<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder\Driver\MySQL;

use Avant\Exception\EDatabaseError;
use Avant\Storages\PDO\Builder\BuilderIndexAbstract;
use Avant\Storages\PDO\Builder\BuilderTrait;


/**
 * Class IndexBuilder
 * @subpackage Avant\Storages\PDO\Adapter\MySQL
 */
final class Index extends BuilderIndexAbstract
{
    use BuilderTrait;

    protected $mapType = [
      self::TYPE_PK       => 'PRIMARY KEY',
      self::TYPE_INDEX    => 'INDEX',
      self::TYPE_UNIQUE   => 'UNIQUE',
      self::TYPE_FULLTEXT => 'FULLTEXT',
    ];

    protected function composeIndex()
    {
        $result = '';

        if (empty( $this->column )) {
            throw new EDatabaseError( 'Column information is required for that operation.' );
        }

        if (empty( $this->indexName )) {
            $this->indexName = implode( '_', $this->column );
        }

        $indexName = $this->protectIdentifiers( $this->indexName );
        $column    = $this->protectIdentifiers( $this->column );

        $result .= $this->mapType[$this->type];

        if ($this->type == self::TYPE_PK) {
            $result .= ' ('.implode( ',', $column ).')';
        } else {
            $result .= ' '.$indexName.' ('.implode( ',', $column ).')';
        }

        return $result;
    }
}

/* End of file IndexBuilder.php */
