<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder;


/**
 * Class BuilderIndexAbstract
 * @subpackage Avant\Storages\PDO\Builder
 */
abstract class BuilderIndexAbstract extends BuilderAbstract
{
    const TYPE_PK       = 'primary';
    const TYPE_INDEX    = 'index';
    const TYPE_UNIQUE   = 'unique';
    const TYPE_FULLTEXT = 'fulltext';
    /**
     * Supported column builders
     * @var array
     */
    protected static $invokeBuilders = [
      'mysql' => 'Avant\Storages\PDO\Builder\Driver\MySQL\Index',
    ];
    protected        $column         = [];
    protected        $indexName;
    protected        $type;

    public function column($column)
    {
        if (is_string( $column )) {
            if (!in_array( $column, $this->column )) {
                $this->column[] = $column;
            }
        } elseif (is_array( $column )) {
            foreach ($column as $key => $val) {
                $this->column( $val );
            }
        }

        return $this;
    }

    public function composeArray()
    {
        return [
          'type'      => $this->type,
          'column'    => $this->column,
          'indexName' => $this->indexName,
        ];
    }

    public function composeString()
    {
        return $this->composeIndex();
    }

    public function name($indexName)
    {
        if (!empty( $indexName ) && is_string( $indexName ) && $this->indexName != $indexName) {
            $this->indexName = $indexName;
        }

        return $this;
    }

    abstract protected function composeIndex();

    protected function setColumn($column)
    {
        return $this->column( $column );
    }

    protected function setIndexName($indexName)
    {
        return $this->name( $indexName );
    }
}

/* End of file BuilderIndexAbstract.php */
