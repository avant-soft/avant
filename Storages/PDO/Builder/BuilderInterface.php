<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder;


/**
 * Interface BuilderInterface
 * @subpackage Avant\Storages\PDO
 */
interface BuilderInterface
{

}