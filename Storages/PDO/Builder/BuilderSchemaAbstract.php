<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO\Builder;

use Avant\Exception\EDatabaseError;


/**
 * Class BuilderSchemaAbstract
 * @subpackage Avant\Storages\PDO\Builder
 */
abstract class BuilderSchemaAbstract extends BuilderAbstract
{

    /**
     * Supported column builders
     * @var array
     */
    protected static $invokeBuilders = [
      'mysql' => 'Avant\Storages\PDO\Builder\Driver\MySQL\Schema',
    ];
    protected        $columns        = [];
    protected        $keys           = [];


    public function addColumn($column)
    {
        return $this->createColumn( $column );
    }

    public function addIndex($key)
    {
        return $this->createIndex( $key );
    }

    public function columnBigInt($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_BIGINT, $length );
    }

    public function columnBigPk($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_BIGPK, $length );
    }

    public function columnBinary($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_BINARY, $length );
    }

    public function columnBoolean()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_BOOLEAN );
    }

    public function columnDate()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_DATE );
    }

    public function columnDateTime()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_DATETIME );
    }

    public function columnDecimal($precision = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_DECIMAL, $precision );
    }

    public function columnDouble($precision = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_DOUBLE, $precision );
    }

    public function columnFloat($precision = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_FLOAT, $precision );
    }

    public function columnInt($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_INTEGER, $length );
    }

    public function columnJson()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_JSON );
    }

    public function columnLongText()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_LONG_TEXT );
    }

    public function columnMediumText()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_MEDIUM_TEXT );
    }

    public function columnMoney($precision = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_MONEY, $precision );
    }

    public function columnPrimaryKey($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_PK, $length );
    }

    public function columnSmallint($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_SMALLINT, min( 5, $length ) );
    }

    public function columnString($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_STRING, $length );
    }

    public function columnText()
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_TEXT );
    }

    public function columnTime($precision = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_TIME, $precision );
    }

    public function columnTimestamp($precision = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_TIMESTAMP, $precision );
    }

    public function columnTinyint($length = null)
    {
        return $this->createColumnBuilder( BuilderColumnAbstract::TYPE_TINYINT, min( 3, $length ) );
    }

    public function composeArray()
    {
    }

    public function composeString()
    {
    }

    public function createColumn($column, $tableName = null)
    {
        if (is_string( $column )) {
            if (strpos( $column, ' ' ) === false) {
                throw new EDatabaseError( 'Column information is required for that operation.' );
            }
            $this->columns[] = $column;
        } elseif ($column instanceof BuilderColumnAbstract) {
            $this->columns[] = $column;
        } elseif (is_array( $column )) {
            $this->columns = array_merge( $this->columns, $column );
        }

        if (empty( $tableName )) {
            return $this;
        }

        $sql = $this->composeAlterTableColumns( 'ADD', $tableName, $this->columns );

        return $this->execute( $sql );
    }

    public function createDatabase($dbName, $ifNotExists = true, $charSet = null, $collate = null)
    {
        $sql = $this->composeCreateDatabase( $dbName, $ifNotExists, $charSet, $collate );

        return $this->execute( $sql );
    }

    public function createIndex($key, $tableName = null)
    {
        if (is_string( $key ) || $key instanceof BuilderIndexAbstract) {
            $this->keys[] = $key;
        } elseif (is_array( $key )) {
            $this->keys = array_merge( $this->keys, $key );
        }

        if (empty( $tableName )) {
            return $this;
        }

        $sql = $this->composeAlterTableKeys( 'ADD', $tableName, $this->keys );

        return $this->execute( $sql );
    }

    public function createTable($tableName, $ifNotExists = true)
    {
        if (empty( $tableName )) {
            throw new EDatabaseError( 'A table name is required for that operation.' );
        }

        if (count( $this->columns ) == 0) {
            throw new EDatabaseError( 'Field information is required.' );
        }

        $sql = $this->composeCreateTable( $tableName, $this->columns, $this->keys, $ifNotExists );

        return $this->execute( $sql );
    }

    public function describeTable($tableName)
    {
        $sql = $this->composeDescribeTable( $tableName );

        return $this->realize( $sql, [], 'fetchAll' );
    }

    public function dropColumn($column, $tableName)
    {
        if (is_string( $column )) {
            $this->columns[] = $column;
        } elseif (is_array( $column )) {
            $this->columns = array_merge( $this->columns, $column );
        }

        $sql = $this->composeAlterTableColumns( 'DROP', $tableName, $this->columns );

        return $this->execute( $sql );
    }

    public function dropDatabase($dbName, $ifNotExists = true)
    {
        $sql = $this->composeDropDatabase( $dbName, $ifNotExists );

        return $this->execute( $sql );
    }

    public function dropIndex($indexName, $tableName)
    {
        $sql = $this->composeDropIndex( $indexName, $tableName );

        return $this->execute( $sql );
    }

    public function dropPrimaryKey($indexName, $tableName)
    {
        $sql = $this->composeDropIndex( $indexName, $tableName, true );

        return $this->execute( $sql );
    }

    public function dropTable($tableName, $ifNotExists = true)
    {
        $sql = $this->composeDropTable( $tableName, $ifNotExists );

        return $this->execute( $sql );
    }

    public function dropTableIfExists($tableName)
    {
        return $this->dropTable( $tableName, true );
    }

    public function existsTable($tableName)
    {
        $sql = $this->composeExistsTable( $tableName );

        return $this->execute( $sql ) > 0;
    }

    public function index($column, $indexName = null)
    {
        return $this->createIndexBuilder( BuilderIndexAbstract::TYPE_INDEX, $column, $indexName );
    }

    public function indexPrimary($column, $indexName = null)
    {
        return $this->createIndexBuilder( BuilderIndexAbstract::TYPE_PK, $column, $indexName );
    }

    public function indexFulltext($column, $indexName = null)
    {
        return $this->createIndexBuilder( BuilderIndexAbstract::TYPE_FULLTEXT, $column, $indexName );
    }

    public function indexUnique($column, $indexName = null)
    {
        return $this->createIndexBuilder( BuilderIndexAbstract::TYPE_UNIQUE, $column, $indexName );
    }

    public function modifyColumn($column, $tableName)
    {
        if (is_string( $column )) {
            if (strpos( $column, ' ' ) === false) {
                throw new EDatabaseError( 'Column information is required for that operation.' );
            }
            $this->columns[$column] = [];
        } elseif ($column instanceof BuilderColumnAbstract) {
            $this->columns[] = $column;
        } elseif (is_array( $column )) {
            $this->columns = array_merge( $this->columns, $column );
        }

        $sql = $this->composeAlterTableColumns( 'MODIFY', $tableName, $this->columns );

        return $this->execute( $sql );
    }

    public function prepare($value = false)
    {
        $this->prepare = $value;

        return $this;
    }

    public function primaryKey($column)
    {
        return $this->createIndexBuilder( BuilderIndexAbstract::TYPE_PK, $column );
    }

    public function renameTable($fromTableName, $toTableName)
    {
        $sql = $this->composeAlterTableName( $fromTableName, $toTableName );

        return $this->execute( $sql );
    }

    public function showDatabases()
    {
        $sql = $this->composeShowDatabases();

        return $this->realize( $sql, [], 'fetchAll' );
    }

    public function showIndex($tableName, $dbName = null)
    {
        $sql = $this->composeShowIndexes( $tableName, $dbName );

        return $this->realize( $sql, [], 'fetchAll' );
    }

    public function showTables($dbName = null)
    {
        $sql = $this->composeShowTables( $dbName );

        return $this->realize( $sql, [], 'fetchAll' );
    }

    abstract protected function composeAlterTableColumns($alterSpec, $tableName, $columns);

    abstract protected function composeAlterTableKeys($alterSpec, $tableName, $keys);

    abstract protected function composeAlterTableName($fromTableName, $toTableName);

    abstract protected function composeCreateDatabase($dbName, $ifNotExists = true, $charSet = null, $collate = null);

    abstract protected function composeCreateTable($tableName, $columns, $keys, $ifNotExists = true);

    abstract protected function composeDescribeTable($tableName);

    abstract protected function composeDropDatabase($dbName, $ifNotExists = true);

    abstract protected function composeDropIndex($indexName, $tableName, $indexPrimaryKey = false);

    abstract protected function composeDropTable($tableName, $ifNotExists = true);

    abstract protected function composeExistsTable($tableName);

    abstract protected function composeShowDatabases();

    abstract protected function composeShowIndexes($tableName, $dbName = null);

    abstract protected function composeShowTables($dbName = null);

    /**
     * @param      $type
     * @param mixed $constraint
     *
     * @return BuilderColumnAbstract|mixed
     * @throws EDatabaseError
     */
    protected function createColumnBuilder($type, $constraint = null)
    {
        return BuilderColumnAbstract::create( $this->getOwner()->getDriverName(), ['type' => $type, 'constraint' => $constraint, 'owner' => $this->owner] );
    }

    /**
     * @param      $type
     * @param      $column
     * @param mixed $indexName
     *
     * @return BuilderIndexAbstract|mixed
     * @throws EDatabaseError
     */
    protected function createIndexBuilder($type, $column, $indexName = null)
    {
        return BuilderIndexAbstract::create( $this->getOwner()->getDriverName(), ['type' => $type, 'column' => $column, 'indexName' => $indexName, 'owner' => $this->owner] );
    }

    /**
     * @param $sql
     *
     * @return mixed
     */
    protected function execute($sql)
    {
        return $this->realize( $sql, [], 'execute' );
    }

    protected function flush()
    {
        parent::flush();
        $this->keys    = [];
        $this->columns = [];
    }
}

/* End of file BuilderSchemaAbstract.php */
