<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Storages\PDO;

use Avant\Exception\EComponentError;
use Avant\Http\Application;

/**
 * Trait DatabaseProxyTrait
 * @subpackage Avant\Storages\PDO
 */
trait DatabaseTrait
{
    /**
     * Get Database component
     *
     * @param mixed $key
     *
     * @return Database
     * @throws EComponentError
     */
    public function getDatabase($key = 'db')
    {
        static $db_list = [];

        if (!isset( $db_list[$key] ) || (!$db_list[$key] instanceof Database)) {
            if (($db = Application::getInstance()->getComponent( $key )) instanceof Database) {
                $db_list[$key] = $db;
            } else {
                throw new EComponentError( sprintf( "%s: database component '%s' is not found", __METHOD__, $key ) );
            }
        }

        return $db_list[$key];
    }

}