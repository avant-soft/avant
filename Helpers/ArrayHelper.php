<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Helpers;


/**
 * Class ArrayHelper
 * @subpackage Avant\Helpers
 */
class ArrayHelper
{
    public static function element($key, array $haystack, $default = false, $expected_type = null)
    {
        if (!is_array( $haystack )) {
            return $default;
        }

        $result = $default;
        if (array_key_exists( $key, $haystack )) {
            if (in_array( $expected_type, TypeHelper::TYPES )) {
                if (TypeHelper::isType( $haystack[$key], $expected_type )) {
                    $result = $haystack[$key];
                }
            } else {
                $result = $haystack[$key];
            }
        }

        return $result;
    }

    /**
     * @param string|array $keys
     * @param array        $haystack
     * @param mixed|array  $default
     * @param mixed|array  $expected_type
     * @param bool|FALSE   $keys_preserve
     *
     * @return array
     */
    public static function elements($keys, $haystack, $default = false, $expected_type = null, $keys_preserve = false)
    {
        if (!is_array( $haystack )) {
            return $haystack;
        }

        $result = [];

        if (!is_array( $keys )) {
            $keys = [$keys];
        }
        $keysCount = count( $keys );

        if (!is_array( $default )) {
            $default = array_fill( 0, $keysCount, $default );
        }

        if (!is_array( $expected_type )) {
            $expected_type = array_fill( 0, $keysCount, $expected_type );
        }

        for ($i = 0; $i < $keysCount; $i++) {
            $keyPlaceholder = isset( $default[$i] ) ? $default[$i] : null;
            if (array_key_exists( $keys[$i], $haystack )) {
                $keyValue = $haystack[$keys[$i]];
                if (isset( $expected_type[$i] ) && in_array( $expected_type[$i], TypeHelper::TYPES )) {
                    if (!TypeHelper::isType( $haystack[$keys[$i]], $expected_type[$i] )) {
                        $keyValue = $keyPlaceholder;
                    }
                }

                if ($keys_preserve == true) {
                    $result[$keys[$i]] = $keyValue;
                } else {
                    $result[] = $keyValue;
                }
            } else {
                if ($keys_preserve == true) {
                    $result[$keys[$i]] = $keyPlaceholder;
                } else {
                    $result[] = $keyPlaceholder;
                }
            }
        }

        /*
        foreach ($keys as $key) {
            if (array_key_exists($key, $haystack)) {
                if ($keys_preserve == TRUE) {
                    $result[$key] = $haystack[$key];
                }
                else
                    $result[] = $haystack[$key];
            }
            else {
                if ($keys_preserve == TRUE) {
                    $result[$key] = $default;
                }
                else
                    $result[] = $default;
            }
        }
        */

        return $result;
    }

    public static function filter($keys, $haystack)
    {
        if (!is_array( $haystack )) {
            return $haystack;
        }

        $result = [];

        if (!is_array( $keys )) {
            $keys = [$keys];
        }

        foreach ($haystack as $key => $value) {
            if (!in_array( $key, $keys )) {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Gets the first key of an array
     *
     * @param array $array
     *
     * @return mixed
     */
    public static function keyFirst(array $array)
    {
        if (is_array( $array ) && count( $array )) {
            reset( $array );

            return key( $array );
        }

        return null;
    }

    /**
     * Get the last key of the given array without affecting
     * the internal array pointer.
     *
     * @param $array
     *
     * @return mixed|null
     */
    public static function keyLast($array)
    {
        $result = null;
        if (is_array( $array )) {
            end( $array );
            $result = key( $array );
        }

        return $result;
    }

    public static function keysExists(array $keys, array $haystack)
    {
        return count( array_intersect_key( array_flip( $keys ), $haystack ) ) > 0;
    }

    /**
     * Searches the array for a given value(by column) and returns the first corresponding key if successful
     *
     * @param            $needle
     * @param array      $haystack
     * @param bool|FALSE $strict
     * @param mixed       $column
     *
     * @return mixed
     */
    public static function search($needle, array $haystack, $strict = false, $column = null)
    {
        return array_search( $needle, $column !== null ? array_column( $haystack, $column ) : $haystack, $strict );
    }
}

/* End of file ArrayHelper.php */
