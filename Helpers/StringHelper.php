<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Helpers;

use Exception;

/**
 * Class StringHelper_framework
 * @subpackage Avant\Helpers
 */
class StringHelper
{
    public static function isSerialized($str)
    {
        try {
            return $str === 'b:0;' || @unserialize($str) !== false;
        } catch (Exception $exception) {
            return false;
        }
    }

    function isAscii($str)
    {
        return (bool)!preg_match('/[\x80-\xFF]/', $str);
    }
}

/* End of file StringHelper.php */
