<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Helpers;


/**
 * Class Json
 * @subpackage Application\Helper
 */
class JsonHelper
{
    public static function isJson($string)
    {
        return is_string( $string ) && is_array( json_decode( $string, true ) ) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}

/* End of file Json.php */
