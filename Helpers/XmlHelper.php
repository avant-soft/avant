<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Helpers;


/**
 * Class XmlHelper_framework
 * @subpackage Avant\Helpers
 */
class XmlHelper
{
    public static function isXml($str)
    {
        $result = false;
        libxml_use_internal_errors( true );
        if (($xml = simplexml_load_string( $str )) !== false) {
            $result = true;
        }
        libxml_clear_errors();

        return $result;
    }
}

/* End of file XmlHelper.php */
