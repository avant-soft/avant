<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Helpers;

/**
 * Class BytesHelper
 */
class BytesHelper
{
    public static function bytesAssoc($size)
    {
        $suffix = ['b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb', 'zb', 'yb'];

        return $size ? array(round( $size / pow( 1024, ($i = floor( log( $size, 1024 ) )) ), 2 ), $suffix[$i]) : false;
    }

    public static function bytesNumber($str)
    {
        $str    = trim( strtolower( $str ) );
        $num    = str_replace( range( 'a', 'z' ), '', $str );
        $suffix = str_replace( range( 0, 9 ), '', $str );

        $s = [
          'b'  => 'b', 'kb' => 'k',
          'mb' => 'm', 'gb' => 'g',
          'tb' => 't', 'pb' => 'p',
          'eb' => 'e', 'zb' => 'z',
          'yb' => 'y',
        ];

        if (array_key_exists( $suffix, $s )) {
            $from = $suffix;
        } else {
            $s = array_flip( $s );
            if (array_key_exists( $suffix, $s )) {
                $from = $s[$suffix];
            }
        }

        if (!isset( $from )) {
            return 0;
        }

        return self::bytesTo( $num, $from, 'b' );
    }

    public static function bytesStr($size, $format = '%d%s')
    {
        if ($bytes = self::bytesAssoc( $size )) {
            return sprintf( $format, $bytes[0], $bytes[1] );
        }

        return false;
    }

    public static function bytesTo($size, $from = 'b', $to = 'kb')
    {
        $sizes = [
          'b'  => $b = 1,
          'kb' => $kb = 1024,
          'mb' => $mb = 1024 * $kb,
          'gb' => $gb = 1024 * $mb,
          'tb' => $tb = 1024 * $gb,
          'pb' => $pb = 1024 * $tb,
          'eb' => $eb = 1024 * $pb,
          'zb' => $zb = 1024 * $eb,
          'yb' => $yb = 1024 * $zb,
        ];

        isset( $sizes[strtolower( $from )] ) || $from = 'b';
        isset( $sizes[strtolower( $to )] ) || $to = 'kb';

        return round( ($size * $sizes[$from]) / $sizes[$to], 3 );
    }
}