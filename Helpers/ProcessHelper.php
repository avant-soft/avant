<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Helpers;

use Avant\Base\Constants;

/**
 * Class ProcessHelper
 * @subpackage Application\Helper
 */
class ProcessHelper
{
    public static function execute($command)
    {
        if (self::isWinOS()) {
            pclose( popen( 'start "Run command..." /MIN cmd.exe /C "'.$command.'"', Constants::FOPEN_READ ) );
        } else {
            system( $command.' &' );
        }
    }

    public static function getInterpreter()
    {
        $result = 'php.exe';
        if (self::isUnixOS()) {
            $result = '/usr/bin/env php';
        }

        return $result;
    }

    public static function getStarted()
    {
        $result = [];
        if (self::isWinOS()) {
            if (preg_match_all( '/"[^\,]+"\,"([\d]+)"\,.*/i', `tasklist /FO "CSV" /NH`, $matches )) {
                $result = $matches[1];
            }
        } else {
            $result = explode( PHP_EOL, `ps -e | gawk '{print $1}'` );
        }

        return $result;
    }

    public static function isStarted($pid = null)
    {
        if ($pid === null) {
            $pid = getmypid();
        }

        return in_array( $pid, self::getStarted() );
    }

    /**
     * Return TRUE if OS is *nix
     * @return bool
     */
    public static function isUnixOS()
    {
        return self::isWinOS() === false;
    }

    /**
     * Return TRUE if OS is Windows
     * @return bool
     */
    public static function isWinOS()
    {
        return strtoupper( substr( PHP_OS, 0, 3 ) ) === 'WIN';
    }
}

/* End of file ProcessHelper.php */
