<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;

use Avant\Base\ComponentAbstract;
use Avant\Exception\EInOutError;
use Avant\Helpers\FileHelper;
use Avant\Helpers\ProcessHelper;


/**
 * Class LockManager
 * @subpackage Application\Components
 */
class LockManager extends ComponentAbstract
{
    protected $location;

    public function getFilename($lockName)
    {
        if (!FileHelper::isReallyWritable( $location = FileHelper::prepLocation( $this->getLocation() ) )) {
            throw new EInOutError( sprintf( 'Folder "%s" was not found or is not writable', $location ) );
        }

        $result = FileHelper::prepFilename( $location.'/'.pathinfo( $lockName, PATHINFO_FILENAME ).'.lock' );

        return $result;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    public function lock($lockName)
    {
        if (!$this->locked( $lockName )) {
            if (!FileHelper::writeFile( $lockFilename = $this->getFilename( $lockName ), getmypid() )) {
                throw new EInOutError( sprintf( 'File "%s" can not be updated', $lockFilename ) );
            }

            return true;
        }

        return false;
    }

    public function locked($lockName)
    {
        if (file_exists( $lockFilename = $this->getFilename( $lockName ) )) {
            if (($pid = FileHelper::readFile( $lockFilename )) && ProcessHelper::isStarted( $pid )) {
                return $pid;
            }
        }

        return false;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function unlock($lockName)
    {
        if (file_exists( $lockFilename = $this->getFilename( $lockName ) )) {
            unlink( $lockFilename );
        }

        return true;
    }
}

/* End of file LockManager.php */
