<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\Requests;

use Avant\Stdlib\Common;

/**
 * Class Response
 * @subpackage Avant\Stdlib\Request
 */
class Response
{
    protected $contents    = null;
    protected $headers     = [];
    protected $status      = 'Unknown';
    protected $status_code = 0;

    /**
     * Response constructor.
     * @param string|null $data
     */
    public function __construct($data = null)
    {
        if (is_string( $data )) {
            $this->setContents( $data );
        }
    }

    public function getContentType()
    {
        return $this->getHeader( 'Content-Type' );

    }

    /**
     * @return mixed
     */
    public function getContents()
    {
        return $this->contents;
    }

    public function getHeader($header)
    {
        if ($this->hasHeader( $header )) {
            return $this->headers[$header];
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    public function hasHeader($header)
    {
        return array_key_exists( $header, $this->headers );
    }

    /**
     * @param mixed $data
     */
    public function setContents($data)
    {

        if (is_string( $data )) {
            if (strpos( $data, $delimiter = "\r\n\r\n" )) {
                list( $headers, $contents ) = explode( $delimiter, $data, 2 );
                $this->setHeaders( $headers );
                $data = $contents;
            }
        }

        $this->contents = $data;
    }

    public function setHeader($header, $value)
    {
        $value = trim( $value );
        if (!empty( $value )) {
            // normalize header name
            $header = ucwords( preg_replace( '/[\s_-]+/', ' ', $header ) );
            $header = str_replace( ' ', '-', $header );

            // add or update
            $this->headers[$header] = $value;
        }
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        // split headers, one per array element
        if (is_string( $headers )) {

            if (strpos( $headers, "\r\n\r\n" ) !== false) {
                $header_parts = explode( "\r\n\r\n", $headers );
                $headers      = $header_parts[count( $header_parts ) - 1];
            }

            // tolerate line terminator: CRLF = LF (RFC 2616 19.3)
            $headers = str_replace( "\r\n", "\n", $headers );
            // unfold folded header fields. LWS = [CRLF] 1*( SP | HT ) <US-ASCII SP, space (32)>, <US-ASCII HT, horizontal-tab (9)> (RFC 2616 2.2)
            $headers = preg_replace( '/\n[ \t]/', ' ', $headers );
            // create the headers array
            $headers = explode( "\n", $headers );

        }

        if (!is_array( $headers )) {
            return;
        }

        foreach ($headers as $header) {
            if (empty( $header )) {
                continue;
            }

            if (strpos( $header, ':' ) === false) {
                list( , $code, $status ) = explode( ' ', $header, 3 );
                $this->setStatusCode( $code );
                $this->setStatus( $status );
                continue;
            }

            list( $name, $value ) = explode( ':', $header, 2 );
            $this->setHeader( $name, $value );
        }
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode($status_code)
    {
        $this->setStatus( Common::statusToStr( $this->status_code = $status_code ) );
    }
}

/* End of file Response.php */
