<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\Requests;

use Avant\Base\Configurable;


/**
 * Class RequestAbstract
 * @subpackage Avant\Stdlib\Request
 */
abstract class RequestsAbstract extends Configurable implements RequestsInterface
{
    protected $auth_type;
    protected $blocking         = false;
    protected $content          = null;
    protected $headers          = [];
    protected $method           = 'GET';
    protected $password;
    protected $protocol_version = '1.0';
    protected $redirects_count  = 5;
    protected $timeout          = 5;
    protected $user;

    public static function flatten($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[] = sprintf( '%s: %s', $key, $value );
        }

        return $result;
    }

    public function delete($url, $headers = null)
    {
        return $this->request( $url, 'DELETE', $headers );
    }

    public function get($url, $headers = null)
    {
        return $this->request( $url, 'GET', $headers );
    }

    /**
     * @return mixed
     */
    public function getAuthType()
    {
        return $this->auth_type;
    }

    /**
     * @return null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getProtocolVersion()
    {
        return $this->protocol_version;
    }

    /**
     * @return int
     */
    public function getRedirectsCount()
    {
        return $this->redirects_count;
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    public function hasHeader($header)
    {
        return array_key_exists( $header, $this->headers );
    }

    public function head($url, $headers = null)
    {
        return $this->request( $url, 'HEAD', $headers );
    }

    /**
     * @return boolean
     */
    public function isBlocking()
    {
        return $this->blocking;
    }

    public function options($url, $headers = null, $content = null)
    {
        return $this->request( $url, 'OPTIONS', $headers, $content );
    }

    public function patch($url, $headers = null, $content = null)
    {
        return $this->request( $url, 'PATCH', $headers, $content );
    }

    public function post($url, $headers = null, $content = null)
    {
        return $this->request( $url, 'POST', $headers, $content );
    }

    public function put($url, $headers = null, $content = null)
    {
        return $this->request( $url, 'PUT', $headers, $content );
    }

    /**
     * @param string            $url
     * @param mixed|string       $method
     * @param mixed|array        $headers
     * @param mixed|array|object $content
     * @param mixed|Response     $response
     * @return mixed
     */
    abstract public function request($url, $method = null, $headers = null, $content = null, $response = null);

    /**
     * @param mixed $auth_type
     */
    public function setAuthType($auth_type)
    {
        $this->auth_type = $auth_type;
    }

    /**
     * @param $blocking
     * @return $this
     */
    public function setBlocking($blocking)
    {
        $this->blocking = $blocking;

        return $this;
    }

    /**
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @param $header
     * @param $value
     * @return $this
     */
    public function setHeader($header, $value)
    {
        $value = trim( $value );
        if (!empty( $value )) {
            $this->headers[$header] = $value;
        }

        return $this;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $method       = strtoupper( $method );
        $this->method = $method;

        return $this;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param $protocol_version
     * @return $this
     */
    public function setProtocolVersion($protocol_version)
    {
        $this->protocol_version = $protocol_version;

        return $this;
    }

    /**
     * @param $redirects_count
     * @return $this
     */
    public function setRedirectsCount($redirects_count)
    {
        $this->redirects_count = $redirects_count;

        return $this;
    }

    /**
     * @param $timeout
     * @return $this
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function trace($url, $headers = null)
    {
        return $this->request( $url, 'TRACE', $headers );
    }
}

/* End of file RequestAbstract.php */
