<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\Requests;

use Avant\Http\Exception\EHttpException;
use Avant\Http\Helpers\UrlHelper;

/**
 * Class StreamRequests
 * @subpackage Avant\Stdlib\Request
 */
class StreamRequests extends RequestsAbstract
{
    public function isSupported()
    {
        return (function_exists( 'fopen' ) && ini_get( 'allow_url_fopen' ) == true);
    }

    public function request($url, $method = null, $headers = null, $content = null, $response = null)
    {

        if (!$response instanceof Response) {
            $response = new Response();
        }

        try {
            if (!$urlParts = UrlHelper::parse( $url )) {
                throw new EHttpException( 501, sprintf( 'Malformed URL: %s', $url ) );
            }

            if (!in_array( $urlParts[1], ['http', 'https'] )) {
                $url = str_replace( $urlParts[1], 'http', $url );
            }

            $headers = is_array( $headers ) ? $headers : [];
            $headers = array_merge( $headers, $this->getHeaders() );

            $content = $content !== null ? $content : $this->getContent();
            $content = (is_array( $content ) || is_object( $content )) ? http_build_query( $content, '', '&' ) : $content;

            if (!is_null( $content )) {
                $headers['Content-Type']   = 'application/x-www-form-urlencoded; charset=utf-8';
                $headers['Content-Length'] = strlen( $content );
            }

            $headers['Host']       = $urlParts[2];
            $headers['Connection'] = 'Close';

            $resource = stream_context_create( [
              'http' => [
                'method'           => strtoupper( $method !== null ? $method : $this->getMethod() ),
                'header'           => implode( "\r\n", self::flatten( $headers ) ),
                'content'          => $content,
                'max_redirects'    => $this->getRedirectsCount(),
                'protocol_version' => $this->getProtocolVersion(),
                'timeout'          => $this->getTimeout(),
              ],
            ] );

            $handle = fopen( $url, 'rb', false, $resource );

            if ($this->isBlocking() == false) {
                fclose( $handle );
                $response->setStatusCode( 200 );

                return $response;
            }

            $contents = "";
            while (!feof( $handle )) {
                $contents .= fread( $handle, 4096 );
            }

            $response->setContents( $contents );

            if (function_exists( 'stream_get_meta_data' )) {
                $meta = stream_get_meta_data( $handle );
                if (isset( $meta['wrapper_data'] )) {
                    $response->setHeaders( $meta['wrapper_data'] );
                }
            }

            fclose( $handle );

        } catch (\Exception $exception) {
            $response->setStatusCode( $exception->getCode() );
            $response->setStatus( sprintf( '%s caused an exception: %s', __METHOD__, $exception->getMessage() ) );
        }

        return $response;
    }
}

/* End of file StreamRequests.php */
