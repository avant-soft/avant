<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\Requests;

use Avant\Http\Exception\EHttpException;
use Avant\Http\Helpers\UrlHelper;


/**
 * Class SocketRequests
 * @subpackage Avant\Stdlib\Requests
 */
class SocketRequests extends RequestsAbstract
{
    public function isSupported()
    {
        return function_exists( 'fsockopen' );
    }

    public function request($url, $method = null, $headers = null, $content = null, $response = null)
    {
        if (!$response instanceof Response) {
            $response = new Response();
        }

        try {
            if (!$urlParts = UrlHelper::parse( $url )) {
                throw new EHttpException( 501, sprintf( 'Malformed URL: %s', $url ) );
            }

            $fsockopen_host = $urlParts[2];

            if (!isset( $urlParts[3] ) || empty( $urlParts[3] )) {
                if (($urlParts[1] == 'ssl' || $urlParts[1] == 'https') && extension_loaded( 'openssl' )) {
                    $fsockopen_host = "ssl://$fsockopen_host";
                    $urlParts[3]    = 443;
                } else {
                    $urlParts[3] = 80;
                }
            }

            /*  поддрежка совместимости с IPv6  */
            if ('localhost' == strtolower( $fsockopen_host )) {
                $fsockopen_host = '127.0.0.1';
            }

            $err     = 0;
            $errText = '';
            if (($handle = fsockopen( $fsockopen_host, (int)$urlParts[3], $err, $errText, $this->getTimeout() )) === false) {
                throw new EHttpException( 501, $errText );
            }

            $request_path = $urlParts[1].'://'.$urlParts[2].$urlParts[4].(isset( $urlParts[5] ) && !empty( $urlParts[5] ) ? $urlParts[5] : '').(isset( $urlParts[6] ) ? '?'.$urlParts[6] : '');

            if (empty( $request_path )) {
                $request_path = '/';
            }

            $headers = is_array( $headers ) ? $headers : [];
            $headers = array_merge( $headers, $this->getHeaders() );

            $content = $content !== null ? $content : $this->getContent();
            $content = (is_array( $content ) || is_object( $content )) ? http_build_query( $content, '', '&' ) : $content;

            if (!is_null( $content )) {
                $headers['Content-Type']   = 'application/x-www-form-urlencoded; charset=utf-8';
                $headers['Content-Length'] = strlen( $content );
            }

            if ($urlParts[3] == 443) {
                $headers['Host'] = $urlParts[2].':'.$urlParts[3];
            } else {
                $headers['Host'] = $urlParts[2];
            }


            if ($authType = $this->getAuthType()) {
                $headers['Authorization'] = sprintf( '%s %s', $authType, base64_encode( $this->getUser().":".$this->getPassword() ) );
            }

            $headers['Connection'] = 'Close';

            $output = sprintf( "%s %s HTTP/%.1f\r\n", $method !== null ? $method : $this->getMethod(), $request_path, $this->getProtocolVersion() );;
            if (is_array( $headers )) {
                $output .= implode( "\r\n", self::flatten( $headers ) );
            }

            if (!is_null( $content )) {
                $output .= "\r\n".$content;
            }

            fwrite( $handle, $output );
            stream_set_timeout( $handle, $this->getTimeout() );

            if ($this->isBlocking() == false) {
                fclose( $handle );
                $response->setStatusCode( 200 );

                return $response;
            }

            $contents = "";
            while (!feof( $handle )) {
                $contents .= fgets( $handle );
            }

            $response->setContents( $contents );
            fclose( $handle );

            if ($method != 'HEAD' && ($location = $response->getHeader( 'Location' )) !== false) {
                $redirect = $this->getRedirectsCount();
                if ($redirect-- > 0) {
                    $this->setRedirectsCount( $redirect );

                    return $this->request( $location, $method, $headers, $content );
                } else {
                    throw new EHttpException( 501, sprintf( 'Lot of redirects for %s', $location ) );
                }
            }

        } catch (\Exception $exception) {
            $response->setStatusCode( $exception->getCode() );
            $response->setStatus( sprintf( '%s caused an exception: %s', __METHOD__, $exception->getMessage() ) );
        }

        return $response;
    }
}

/* End of file SocketRequests.php */
