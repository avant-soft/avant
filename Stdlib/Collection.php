<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;

class Collection implements \Iterator, \ArrayAccess, \Countable
{
    /**
     * The number of elements in the Collection
     * @var int
     */
    public $length = 0;
    /**
     * The inner array
     * @var array
     */
    protected $elements = [];

    /**
     * Create a collection of objects
     *
     * @param array|collection $array [optional]
     */
    public function  __construct($array = null)
    {
        $this->merge($array, true);
    }

    /**
     * Returns as an array
     * @return array
     */
    public function  __toArray()
    {
        return $this->elements;
    }

    /**
     * serialize the collection
     * @return string
     */
    public function  __toString()
    {
        return "" . serialize($this->elements) . "";
    }

    /**
     * Return the xml of this collection
     * @return string
     */
    public function  __toXml()
    {
        $xml = "\n<Collection>";
        foreach ($this->elements as $k => $v) {
            $xml .= "\n\t<element key=\"{$k}\">";
            if (is_string($v)) {
                $xml .= $v;
            } else {
                if (is_object($v) and method_exists($v, "__toXml()")) {
                    $xml .= $v->__toXml();
                } else {
                    if (is_object($v) and method_exists($v, "__toString()")) {
                        $xml .= $v->__toString();
                    }
                }
            }
            $xml .= "</element>";
        }
        $xml .= "\n</Collection>";
        $this->rewind();

        return $xml;
    }

    /**
     * Adds an item object into key(array)
     *
     * @param            $key
     * @param mixed|mixed $item
     *
     * @return mixed
     */
    public function  append($key, $item = null)
    {
        if ($item !== null) {
            if (!array_key_exists($key, $this->elements)) {
                $this->elements[$key] = [];
            } elseif (!is_array($this->elements[$key])) {
                $this->elements[$key] = [$this->elements[$key]];
            }

            $this->elements[$key][] = $item;
        } else {
            array_unshift($this->elements, $key);
        }

        $this->count();
        $this->rewind();

        return $this;
    }

    /**
     * Sort by values, keeping keys
     *
     * @param mixed $flags
     *
     * @return Collection
     */
    public function  asort($flags = null)
    {
        asort($this->elements, $flags);

        return $this;
    }

    /**
     * Moves the cursor a step back
     * @return bool
     */
    public function  back()
    {
        return prev($this->elements);
    }

    /**
     * Null all the collection, including keys
     * @return Collection $this;
     */
    public function  clear()
    {
        $this->elements = [];
        $this->length   = 0;

        return $this;
    }

    /**
     * Check if given object exists in collection
     * Type safe
     *
     * @param mixed $obj
     *
     * @return bool
     */
    public function  contains($obj)
    {
        foreach ($this->elements as $element) {
            if ($element === $obj) {
                $this->rewind();

                return true;
            }
        }
        $this->rewind();

        return false;
    }

    /**
     * size number of items;
     * @return int
     */
    public function count(): int
    {
        $this->length = count($this->elements);

        return $this->length;
    }

    /**
     * Return the item in the cursor point
     * @return mixed Current Item
     */
    public function  current(): mixed
    {
        return current($this->elements);
    }

    /**
     * Returns cursor key
     * @return mixed current key
     */
    public function  currentKey(): mixed
    {
        return key($this->elements);
    }

    /**
     * Verifies if this key is filled
     *
     * @param mixed $k
     *
     * @return bool
     */
    public function  exists($k)
    {
        return array_key_exists($k, $this->elements);
    }

    /**
     * Fetchs basic
     */

    public function  fetch()
    {
        if ($this->valid()) {
            $r = $this->current();
            $this->next();

            return $r;
        } else {
            return false;
        }
    }

    /**
     * Puts cursor at the end
     * @return bool
     */
    public function  forward()
    {
        return end($this->elements);
    }

    /**
     * Returns object for given position
     * @param      $key
     * @param mixed $default
     *
     * @return mixed|null
     */
    public function  get($key, $default = null)
    {
        $result = $default;
        if ($this->exists($key)) {
            $result = $this->elements[$key];
        }

        return $result;
    }

    /**
     * Alias to toArray()
     * @return array
     */

    public function  getArrayCopy()
    {
        return $this->toArray();
    }

    /**
     * NOT WORKING YET
     */
    public function  getIterator()
    {
        return $this;
    }

    /**
     *  If next element is valid
     * @return bool
     */
    public function  hasNext()
    {
        $this->next();
        $v = $this->valid();
        $this->back();

        return $v;
    }

    /**
     * Return the (first) index(key) of given object
     *
     * @param mixed $obj
     *
     * @return mixed key of the obj
     */
    public function  indexOf($obj)
    {
        foreach ($this->elements as $k => $element) {
            if ($element === $obj) {
                $this->rewind();

                return $k;
            }
        }
        $this->rewind();

        return null;
    }

    /**
     * returns if the collection is empty or not
     * @return bool
     */
    public function  isEmpty()
    {
        if ($this->count() < 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * same as valid()
     * @return bool
     */
    public function  isValid()
    {
        return $this->valid();
    }

    /**
     * Actual cursor key
     * @return mixed current key
     */
    public function  key(): mixed
    {
        return $this->currentKey();
    }

    /**
     * Sort by key
     *
     * @param mixed $flags
     *
     * @return Collection
     *
     */
    public function  ksort($flags = null)
    {
        ksort($this->elements, $flags);

        return $this;
    }

    /**
     * returns last index(key) of given object
     *
     * @param mixed $obj
     *
     * @return mixed
     */
    public function  lastIndexOf($obj)
    {
        $return = null;
        foreach ($this->elements as $k => $element) {
            if ($element === $obj) {
                $return = $k;
            }
        }
        $this->rewind();

        return $return;
    }

    /**
     * Join(merge) an array or a collection into this one
     *
     * @param array|collection $array
     * @param bool             $keepKeys
     *
     * @return Collection
     */
    public function  merge($array, $keepKeys = false)
    {
        if (is_array($array) or $array instanceof \ArrayAccess) {
            if (count($array) >= 1) {
                foreach ($array as $key => $value) {
                    if ($keepKeys) {
                        $this->set($key, $value);
                    } else {
                        $this->set($value);
                    }
                }
            }
        } else {
            if ($array instanceof \Iterator) {
                while ($array->valid()) {
                    if ($keepKeys) {
                        $this->set($array->key(), $array->current());
                    } else {
                        $this->set($array->current());
                    }
                    $array->next();
                }
            }
        }
        $this->count();

        return $this;
    }

    /**
     * Moves the cursor a step foward
     * @return void
     */
    public function next(): void
    {
        next($this->elements);
    }

    /**
     * ArrayAccess, check if offset (key) exists
     *
     * @param $offset
     *
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return $this->exists($offset);
    }

    /**
     * ArrayAccess, get element
     *
     * @param $offset
     *
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        return $this->get($offset);
    }

    /**
     * ArrayAccess, set element
     *
     * @param mixed $offset
     * @param mixed $value
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    /**
     * ArrayAccess, remove element
     *
     * @param mixed $offset
     * @return void
     */
    public function  offsetUnset($offset): void
    {
        $this->remove($offset);
    }

    /**
     * Remove the $key item
     *
     * @param mixed $key
     *
     * @return Collection
     */
    public function  remove($key)
    {
        if (array_key_exists($key, $this->elements)) {
            unset($this->elements[$key]);
            $this->count();
        }

        return $this;
    }

    /**
     * Puts the cursor at start
     * @return void
     */
    public function rewind(): void
    {
        reset($this->elements);
    }

    /**
     * Moves cursor to given key
     *
     * @param mixed $key
     *
     * @return bool
     */
    public function  seek($key)
    {
        $this->rewind();
        while ($this->valid()) {
            if ($this->key() == $key) {
                return true;
            }
            $this->next();
        }

        return false;
    }

    /**
     * Set the value of the given key
     *
     * @param            $key
     * @param mixed       $item
     * @param bool|FALSE $atFirst
     *
     * @return mixed
     */
    public function  set($key, $item = null, $atFirst = false)
    {
        if ($item !== null) {
            // merge if array
            if ($this->exists($key) && is_array($item_current = $this->get($key))) {
                $item = !is_array($item) ? [$item] : $item;
                $item = array_replace_recursive($item_current, $item);
            }
            $this->elements[$key] = $item;
        } else {
            if ($atFirst == true) {
                array_unshift($this->elements, $key);
            } else {
                array_push($this->elements, $key);
            }
        }
        $this->count();
        $this->rewind();

        return $this;
    }

    /**
     * @return integer the size of the collection
     */
    public function  size()
    {
        return $this->count();
    }

    /**
     * sort by natural order
     *
     * @param CONST mixed
     *
     * @return Collection
     */
    public function  sort($flags = null)
    {
        sort($this->elements, $flags);

        return $this;
    }

    /**
     * Return a new collection with a part of this collection
     *
     * @param int $start
     * @param int $end
     *
     * @return Collection
     */
    public function  subCollection($start, $end)
    {
        $new = new Collection();
        $i   = 0;
        foreach ($this->elements as $k => $element) {
            if ($i <= $end && $i >= $start) {
                $new->append($element);
            }
            $i++;
        }
        $this->rewind();

        return $new;
    }

    /**
     * A alias to __toArray()
     * @return array
     */
    public function  toArray()
    {
        return $this->__toArray();
    }

    /**
     * alias to __toString()
     * @return string
     */
    public function  toString()
    {
        return $this->__toString();
    }

    /**
     * A alias to __toXMl()
     * @return string
     */
    public function  toXML()
    {
        return $this->__toXML();
    }

    /**
     * Cut the array to given size
     *
     * @param int $size
     *
     * @return Collection
     */
    public function  trimToSize($size)
    {
        $t              = array_chunk($this->elements, $size, true);
        $this->elements = $t[0];
        $this->count();

        return $this;
    }

    /**
     * sort via callback
     *
     * @param $callback
     *
     * @return $this
     */
    public function  uasort($callback)
    {
        if (is_callable($callback)) {
            uasort($this->elements, $callback);
        }

        return $this;
    }

    /**
     * Check if cursor is at a valid item
     * @return bool
     */
    public function valid(): bool
    {
        if (!is_null($this->key())) {
            return true;
        } else {
            return false;
        }
    }
}
