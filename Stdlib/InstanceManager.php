<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;

use Avant\Exception\EComponentError;
use Avant\Exception\EMatchError;

/**
 * Class InstanceManager
 * @subpackage Avant\Stdlib
 */
class InstanceManager
{
    protected $configs          = [];
    protected $instances        = [];
    protected $invokableClasses = [];
    protected $normalized       = [];

    /**
     * @param       $name
     * @param array $config
     * @return null|object
     * @throws EMatchError
     */
    public function get($name, $config = [])
    {

        if (isset( $this->normalized[$name] )) {
            $_name = $this->normalized[$name];
        } else {
            $_name = $this->prepName( $name, false );
        }

        if (isset( $this->instances[$_name] )) {
            return $this->instances[$_name];
        }

        $instance = null;

        if (isset( $this->invokableClasses[$_name] )) {
            $instance = $this->createInstance( $_name, $config );
        }

        if ($instance === null) {
            throw new EMatchError( sprintf( 'No valid instance was found for "%s"', $_name ) );
        }


        return $this->instances[$_name] = $instance;
    }

    /**
     * @return array
     */
    public function getInstances()
    {
        return $this->instances;
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        if (isset( $this->normalized[$name] )) {
            $_name = $this->normalized[$name];
        } else {
            $_name = $this->prepName( $name, false );
        }

        if (isset( $this->invokableClasses[$_name] ) || isset( $this->instances[$_name] )) {
            return $_name;
        }

        return false;
    }

    public function set($name, $var, $override = false, $return = false)
    {
        if (!empty( $name )) {

            if ($override == false && $this->has( $name )) {
                return false;
            }

            $_name = $this->prepName( $name );

            // Class(with[out] namespace)
            if (is_string( $var )) {
                $className = $var;
            } // Array [class[,config]]
            elseif (is_array( $var )) {
                if (!isset( $var['class'] ) /*|| !isset($var['config'])*/) {
                    throw new EComponentError( sprintf( "Class '%s' cannot be registered", $name ) );
                }
                $className             = $var['class'];
                $this->configs[$_name] = isset( $var['config'] ) ? $var['config'] : array_diff_key( $var, array_fill_keys( ['class', 'config'], 'empty' ) );
            } // Object
            elseif (is_object( $var )) {
                $this->instances[$_name] = $var;
                $className               = get_class( $var );
            } else {
                throw new EComponentError( sprintf( "Class '%s' cannot be registered", $name ) );
            }

            $this->invokableClasses[$_name] = $className;

            if ($return == true) {
                return $this->get( $_name );
            }

            return true;
        }

        return false;
    }

    public function unlink($name)
    {
        if ($invoked = $this->has( $name )) {
            unset( $this->invokableClasses[$invoked], $this->instances[$invoked] );
        }
    }

    protected function createInstance($normalizeName, $config = [])
    {
        $invokable = $this->invokableClasses[$normalizeName];

        if (!class_exists( (string)$invokable )) {
            throw new EMatchError( sprintf( '%s: failed retrieving "%s" via invokable class "%s"; class does not exist', __METHOD__, $normalizeName, $invokable ) );
        }

        if (isset( $this->configs[$normalizeName] )) {
            $config = array_merge( $config, $this->configs[$normalizeName] );
        }

        if (!empty( $config )) {
            $instance = new $invokable( $config );
        } else {
            $instance = new $invokable();
        }

        return $instance;
    }

    protected function prepName($name, $remember = true)
    {
        if (isset( $this->normalized[$name] )) {
            return $this->normalized[$name];
        }

        $_name = Common::normalizeName( $name );

        if ($remember == true) {
            $this->normalized[$name] = $_name;
        }

        return $_name;
    }


}

/* End of file InstanceManager.php */
