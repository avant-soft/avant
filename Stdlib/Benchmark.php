<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;

use Avant\Event\Event;
use Avant\Event\EventTrait;


/**
 * Class Benchmark
 * @subpackage Avant\Stdlib
 */
class Benchmark
{
    use EventTrait, InitTrait;

    const EVENT_START = 'benchmark:Start';
    const EVENT_STOP  = 'benchmark:Stop';

    const SUFFIX_START = '_start';
    const SUFFIX_STOP  = '_stop';

    protected $collector = true;
    protected $markers   = [];
    protected $notify    = true;

    public function __construct($config = [])
    {
        $this->initialize( $config );

        if ($this->collector) {
            $this->bind( self::EVENT_START, function (Event $event) {
                if (($context = $event->getContext()) && $context != $this) {
                    $this->setPoint( $this->prepPoint( $event->getParam( 'point', 'execute' ), self::SUFFIX_START ), $event->getParam( 'timer', null ) );
                }
            }, -9999 );

            $this->bind( self::EVENT_STOP, function (Event $event) {
                if (($context = $event->getContext()) && $context != $this) {
                    $this->setPoint( $this->prepPoint( $event->getParam( 'point', 'execute' ), self::SUFFIX_STOP ), $event->getParam( 'timer', null ) );
                }
            }, -9999 );
        }
    }

    /**
     * Calc elapsed time by markers
     *
     * @param string $start - value of microtime()
     * @param string $end   - value of microtime()
     * @param int    $decimals
     * @return string
     */
    public static function elapsedCalc($start, $end, $decimals = 4)
    {
        list( $start_msec, $start_sec ) = explode( ' ', $start );
        list( $end_msec, $end_sec ) = explode( ' ', $end );

        return number_format( ($end_msec + $end_sec) - ($start_msec + $start_sec), $decimals );
    }

    function elapsed($name, $decimals = 4)
    {

        $pointStart = $this->prepPoint( $name, self::SUFFIX_START );
        $pointEnd   = $this->prepPoint( $name, self::SUFFIX_STOP );

        if (!isset( $this->markers[$pointStart] )) {
            return '';
        }

        if (!isset( $this->markers[$pointEnd] )) {
            $this->markers[$pointEnd] = @microtime();
        }

        return self::elapsedCalc( $this->markers[$pointStart], $this->markers[$pointEnd], $decimals );
    }

    public function getMarkers()
    {
        $suffix     = self::SUFFIX_START;
        $suffix_len = strlen( $suffix );

        return array_map( function ($point) use ($suffix_len) {
            return substr( $point, 0, strlen( $point ) - $suffix_len );
        }, array_keys( array_filter( $this->markers, function ($key) use ($suffix) {
            return (strpos( $key, $suffix ) !== false);
        }, ARRAY_FILTER_USE_KEY ) ) );
    }

    public function start($name, $timer = null)
    {
        if (is_array( $name )) {
            foreach ($name as $pointName) {
                $this->start( $pointName );
            }

            return $this;
        }

        if ($this->notify == true) {
            $this->trigger( self::EVENT_START, $this, ['point' => $name] );
        }
        $this->setPoint( $this->prepPoint( $name, self::SUFFIX_START ), $timer );

        return $this;
    }

    public function stop($name, $timer = null)
    {
        if (is_array( $name )) {
            foreach ($name as $pointName) {
                $this->stop( $pointName );
            }

            return $this;
        }

        if ($this->notify == true) {
            $this->trigger( self::EVENT_STOP, $this, ['point' => $name] );
        }
        $this->setPoint( $this->prepPoint( $name, self::SUFFIX_STOP ), $timer );

        return $this;
    }

    protected function prepPoint($name, $suffix = self::SUFFIX_START)
    {
        $name = strtolower( $name );
        $name = str_replace( ['\\', '/', '//', ' '], '_', $name );

        if ($suffix != substr( $name, -1, strlen( $suffix ) )) {
            $name .= $suffix;
        }

        return $name;
    }

    protected function setPoint($name, $time = null)
    {
        $this->markers[$name] = $time == null ? @microtime() : $time;
    }
}

/* End of file Benchmark.php */
