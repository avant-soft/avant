<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;


use Avant\Exception\EObjectError;

class CallbackInstance
{
    protected $callback;
    protected $params = [];

    public function __construct($callback)
    {
        $this->setCallback( $callback );
    }

    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    protected function setCallback($callback)
    {
        if (!is_callable( $callback )) {
            // Callback is array, [class,action]
            if (is_array( $callback )) {
                if (isset( $callback['class'] ) && isset( $callback['method'] )) {
                    $className = $callback['class'];
                    $method    = $callback['method'];
                    $class     = new $className();

                    $this->callback = [$class, $method];
                } else {
                    throw new EObjectError( sprintf( '%s: expected a "callable"; received "%s"', __METHOD__, gettype( $callback ) ) );
                }

                if (isset( $callback['params'] )) {
                    $this->setParams( $callback['params'] );
                }
            }
        }
        $this->callback = $callback;
    }


} 