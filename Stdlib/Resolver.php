<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;

use Avant\Base\ComponentAbstract;
use Avant\Helpers\FileHelper;

/**
 * Class PathResolver
 * @subpackage Avant\Http
 */
class Resolver extends ComponentAbstract
{
    protected $locations = [];
    protected $paths     = [];

    public function __init()
    {
        if (empty( $this->paths ) && defined( 'APP_PATH' )) {
            $this->addPath( constant('APP_PATH') );
        }
    }

    public function addLocation($locations, $unshift = true)
    {
        if (is_array( $locations )) {
            foreach ($locations as $location) {
                $this->addLocation( $location );
            }

            return $this;
        }

        if (!in_array( $locations, $this->locations )) {
            if ($unshift === true) {
                array_unshift( $this->locations, FileHelper::prepLocation( $locations ) );
            } else {
                $this->locations[] = FileHelper::prepLocation( $locations );
            }
        }

        return $this;
    }

    public function addPath($paths, $unshift = true)
    {
        if (is_array( $paths )) {
            foreach ($paths as $path) {
                $this->addPath( $path );
            }

            return $this;
        }

        $paths = FileHelper::prepPath( $paths );
        if (!in_array( $paths, $this->paths )) {
            if ($unshift === true) {
                array_unshift( $this->paths, $paths );
            } else {
                $this->paths[] = $paths;
            }
        }

        return $this;
    }

    public function findFile($filename, $basePath = null)
    {
        // if $filename already exists do not check & find
        if (is_file( $filename )) {
            return $filename;
        }

        $fileNormalize = FileHelper::prepFilename( $filename );
        $basePath      = FileHelper::prepPath( ($basePath ? $basePath : ''), false );
        foreach ($this->getPaths() as $path) {
            if (is_file( $location = FileHelper::prepLocation( $path.$basePath.$fileNormalize ) )) {
                return $location;
            }
        }

        return false;
    }

    public function getPaths()
    {
        $paths = [];
        foreach ($this->paths as $path) {
            foreach ($this->locations as $location) {
                $paths[] = FileHelper::prepPath( $path.$location );
            }
            $paths[] = $path;
        }

        return $paths;
    }

    public function removePath($paths)
    {
        if (is_array( $paths )) {
            foreach ($paths as $path) {
                $this->removePath( $path );
            }

            return $this;
        }

        $paths = FileHelper::prepPath( $paths );
        if (in_array( $paths, $this->paths )) {
            unset( $this->paths[$paths] );
        }

        return $this;
    }
}

/* End of file PathResolver.php */
