<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;


/**
 * Class GetOpts
 * @anchor app_console_getopts
 */
class GetOpts
{
    use InitTrait;

    const OPT_REQUIRED     = 1 << 1;
    const OPT_NOT_REQUIRED = 1 << 2;
    const OPT_EMPTY        = 1 << 3;

    /**
     * Arguments
     * internal storage of the arguments to parse
     * @var array
     * @access private
     */
    private $args;
    /**
     * Argument Count
     * The number of arguments passed to the class
     * @var integer
     * @access private
     */
    private $args_count;
    /**
     * Options
     * an array of acceptable short options (-o)
     * @var array
     * @access private
     */
    private $opts = [];
    /**
     * Long Options
     * an array of acceptable long options (--option)
     * @var mixed
     */
    private $opts_long = [];

    /**
     * Parsed options
     * @var array
     */
    private $opts_parsed = [];

    public function __construct(array $config = null)
    {
        if ($config !== null) {
            $this->initialize( $config );
            $this->parse();
        }
    }

    public function fetch($option, $default = null)
    {
        if (isset( $this->opts_parsed[$option] )) {
            return $this->opts_parsed[$option];
        }

        return $default;
    }

    public function fetchAll()
    {
        return $this->opts_parsed;
    }

    public function getOpts()
    {
        return $this->opts;
    }

    public function getOptsLong()
    {
        return $this->opts_long;
    }

    /**
     * Parses the options passed to the class into an array and stores them locally
     * @return mixed
     */
    public function parse()
    {
        $this->opts_parsed = $opts_parsed = [];

        // Parse options
        foreach ([$this->opts, $this->opts_long] as $key => $opts) {
            if (is_array( $opts )) {
                foreach ($opts as $opt) {
                    if (substr( $opt, -2 ) == '::') {
                        $opts_parsed[substr( $opt, 0, -2 )] = self::OPT_NOT_REQUIRED;
                    } elseif (substr( $opt, -1 ) == ':') {
                        $opts_parsed[substr( $opt, 0, -1 )] = self::OPT_REQUIRED;
                    } else {
                        $opts_parsed[$opt] = self::OPT_EMPTY;
                    }
                }
            }
        }

        $offset = (isset( $_SERVER['argv'][0] ) && $_SERVER['argv'][0] == $_SERVER['SCRIPT_NAME']) ? 0 : -1;

        // Check args
        if (empty( $this->args )) {
            global $argv;
            $this->setArgs( $argv );
        }

        // Check if we're done
        if (++$offset >= $this->args_count) {
            return null;
        }

        // Fill
        for ($i = 0; $i < $this->args_count; $i++) {

            // Check if it's a long option '--'
            if ($this->args[$i][0] == '-' && $this->args[$i][1] == '-') {
                // If there's an equal sign in the argument
                if ($pos = strpos( $this->args[$i], '=' )) {
                    $arg    = substr( $this->args[$i], 2, ($pos - 2) );
                    $argVal = substr( $this->args[$i], $pos + 1 );
                } else {
                    $arg = substr( $this->args[$i], 2 );
                    if ($this->args_count > ($i + 1)) {
                        $argVal = $this->args[$i + 1][0] == '-' ? null : $this->args[$i + 1];
                    } else {
                        $argVal = null;
                    }

                }
            } // Check if it's a short option '-' or '/'
            elseif (in_array( $this->args[$i][0], ['-', '/'] )) {
                $arg    = $this->args[$i][1];
                $argVal = strlen( $this->args[$i] ) == 2 ? (($this->args_count > ($i + 1) ? ($this->args[$i + 1][0] == '-' ? null : $this->args[$i + 1]) : null)) : substr( $this->args[$i], 2 );
            } // Option is not found
            else {
                continue;
            }

            // Is it in the approved list
            if (array_key_exists( $arg, $opts_parsed )) {
                switch ($opts_parsed[$arg]) {
                    case self::OPT_NOT_REQUIRED:
                        $this->opts_parsed[$arg] = $this->clear( $argVal );
                        break;
                    case self::OPT_REQUIRED:
                        if ($argVal !== null) {
                            $this->opts_parsed[$arg] = $this->clear( $argVal );
                        }
                        break;
                    case self::OPT_EMPTY:
                        $this->opts_parsed[$arg] = true;
                }
            } else {
                continue;
            }
        }

        return $this->opts_parsed;
    }

    public function setArgs($args)
    {
        if (is_array( $args ) && !empty( $args )) {
            $this->args = [];
            foreach ($args as $arg) {
                $this->args[] = $arg;
            }
            $this->args_count = count( $this->args );
        }

        return $this;
    }

    public function setOpts(array $opts)
    {
        array_walk( $opts, function (&$value) { $value = $this->clear( $value ); } );
        $this->opts = $opts;

        return $this;
    }

    public function setOptsLong(array $opts)
    {
        array_walk( $opts, function (&$value) { $value = $this->clear( $value ); } );
        $this->opts_long = $opts;

        return $this;
    }

    private function clear($val)
    {
        return preg_replace( '/^[\-\=\s]+/', '', $val );
    }
}