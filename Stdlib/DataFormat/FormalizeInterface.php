<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\DataFormat;


/**
 * Interface FormalizeInterface
 */
interface FormalizeInterface
{
    const FTYPE_ARRAY   = 'array';
    const FTYPE_BOOL    = 'boolean';
    const FTYPE_INT     = 'integer';
    const FTYPE_FLOAT   = 'float';
    const FTYPE_NULL    = 'null';
    const FTYPE_OBJECT  = 'object';
    const FTYPE_STRING  = 'string';
    const FTYPE_UNKNOWN = 'unknown';
}