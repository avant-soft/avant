<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\DataFormat\IO;

use Avant\Helpers\JsonHelper;
use Avant\Stdlib\DataFormat\FormalizeAbstract;


/**
 * Class JsonFormalize
 */
class JsonFormalize extends FormalizeAbstract
{
    public function toArray($source = null)
    {
        if ($this->getSourceType( $source = $source === null ? $this->getSource() : $source ) === self::FTYPE_STRING) {
            if (JsonHelper::isJson( $source )) {
                $array = json_decode( $source, true );
                if ($this->getSourceType( $array ) === self::FTYPE_ARRAY) {
                    return $array;
                }
            }
        }

        return false;
    }

    public function toString($source = null)
    {
        switch ($this->getSourceType( $source = $source === null ? $this->getSource() : $source )) {
            case self::FTYPE_STRING && !JsonHelper::isJson( $source ):
            case self::FTYPE_ARRAY:
            case self::FTYPE_OBJECT:
                return json_encode( $source, JSON_UNESCAPED_SLASHES | JSON_HEX_QUOT );
                break;
            default:
                return false;
        }
    }
}

/* End of file JsonFormalize.php */
