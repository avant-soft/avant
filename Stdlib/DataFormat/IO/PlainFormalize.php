<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\DataFormat\IO;

use Avant\Stdlib\DataFormat\FormalizeAbstract;


/**
 * Class PlainFormalize
 */
class PlainFormalize extends FormalizeAbstract
{
    public function toArray($source = null)
    {
        return var_export( $source = $source === null ? $this->getSource() : $source, true );
    }

    public function toString($source = null)
    {
        switch ($this->getSourceType( $source = $source === null ? $this->getSource() : $source )) {
            case self::FTYPE_ARRAY:
            case self::FTYPE_OBJECT:
                return var_export( $source, true );
                break;
            case self::FTYPE_STRING:
                return $source;
            default:
                return false;
        }
    }
}

/* End of file PlainFormalize.php */
