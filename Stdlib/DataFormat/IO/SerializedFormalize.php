<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\DataFormat\IO;

use Avant\Helpers\StringHelper;
use Avant\Stdlib\DataFormat\FormalizeAbstract;


/**
 * Class SerializedFormalize
 */
class SerializedFormalize extends FormalizeAbstract
{
    public function toArray($source = null)
    {
        if ($source === null) {
            $source = $this->getSource();
        }

        if (StringHelper::isSerialized( $source )) {
            $source = @unserialize( $source );
            if ($this->getSourceType( $source ) === self::FTYPE_ARRAY) {
                return $source;
            }
        }

        return false;
    }

    public function toString($source = null)
    {
        if (!in_array( $this->getSourceType( $source = $source === null ? $this->getSource() : $source ), [self::FTYPE_ARRAY, self::FTYPE_OBJECT] )) {
            return false;
        }

        return serialize( $source );
    }

}

/* End of file SerializedFormalize.php */
