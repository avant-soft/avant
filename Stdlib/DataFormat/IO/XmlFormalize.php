<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\DataFormat\IO;

use Avant\Helpers\XmlHelper;
use Avant\Stdlib\DataFormat\FormalizeAbstract;
use Avant\Stdlib\InflectorTrait;

/**
 * Class XmlFormalize
 */
class XmlFormalize extends FormalizeAbstract
{
    use InflectorTrait;

    public function toArray($source = null)
    {
        if ($this->getSourceType( $source = $source === null ? $this->getSource() : $source ) === self::FTYPE_STRING) {
            if (XmlHelper::isXml( $source )) {
                $array = $this->decodeSource( $source );
                if ($this->getSourceType( $array ) === self::FTYPE_ARRAY) {
                    return $array;
                }
            }
        }

        return false;
    }

    public function toString($source = null, $schema = null, $defaultNode = null)
    {
        if (!in_array( $this->getSourceType( $source = $source === null ? $this->getSource() : $source ), [self::FTYPE_ARRAY, self::FTYPE_OBJECT] )) {
            return false;
        }

        return $this->encodeSource( $source, $schema, $defaultNode );
    }

    protected function decodeSource($source)
    {
        $xml = simplexml_load_string( $source, 'SimpleXMLElement', LIBXML_NOCDATA );

        return json_decode( json_encode( (array)$xml ), true );
    }

    protected function encodeSource($source, $schema = null, $defaultNode = null)
    {
        if (ini_get( 'zend.ze1_compatibility_mode' ) == 1) {
            ini_set( 'zend.ze1_compatibility_mode', 0 );
        }

        !is_null( $defaultNode ) or $defaultNode = 'document';
        !is_null( $schema ) or $schema = simplexml_load_string( "<?xml version='1.0' encoding='utf-8'?><$defaultNode />" );

        foreach ((array)$source as $key => $value) {
            //change false/true to 0/1
            if (is_bool( $value )) {
                $value = (int)$value;
            }

            // no numeric keys in our xml please!
            if (is_numeric( $key )) {
                // make string key...
                $key = ($this->singular( $defaultNode ) != $defaultNode) ? $this->singular( $defaultNode ) : 'item';
            }

            // replace anything not alpha numeric
            $key = preg_replace( '/[^a-z_\-0-9]/i', '', $key );

            if ($key === 'attributes' && (is_array( $value ) || is_object( $value ))) {
                $attributes = $value;
                if (is_object( $attributes )) {
                    $attributes = get_object_vars( $attributes );
                }

                foreach ($attributes as $attributeName => $attributeValue) {
                    $schema->addAttribute( $attributeName, $attributeValue );
                }
            } // if there is another array found recursively call this function
            elseif (is_array( $value ) || is_object( $value )) {
                $node = $schema->addChild( $key );

                // recursive call.
                $this->encodeSource( $value, $node, $key );
            } else {
                // add single node.
                $value = htmlspecialchars( html_entity_decode( $value, ENT_QUOTES, 'UTF-8' ), ENT_QUOTES, "UTF-8" );
                $schema->addChild( $key, $value );
            }
        }

        return $schema->asXML();
    }
}

/* End of file XmlFormalize.php */
