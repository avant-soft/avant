<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib\DataFormat;


/**
 * Class FormalizeAbstract
 */
abstract class FormalizeAbstract implements FormalizeInterface
{
    protected $source;

    public function __construct($source = null)
    {
        $this->setSource( $source );
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    public function getSourceType($source = null)
    {
        $result = self::FTYPE_UNKNOWN;

        if ($source === null) {
            $source = $this->getSource();
        }

        if (is_array( $source )) {
            $result = self::FTYPE_ARRAY;
        } elseif (is_object( $source )) {
            $result = self::FTYPE_OBJECT;
        } elseif (is_bool( $source )) {
            $result = self::FTYPE_BOOL;
        } elseif (is_float( $source )) {
            $result = self::FTYPE_FLOAT;
        } elseif (is_integer( $source )) {
            $result = self::FTYPE_INT;
        } elseif (is_string( $source )) {
            $result = self::FTYPE_STRING;
        } elseif (is_null( $source )) {
            $result = self::FTYPE_NULL;
        }

        return $result;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    abstract public function toArray($source = null);

    abstract public function toString($source = null);

}

/* End of file FormalizeAbstract.php */
