<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Stdlib;

/**
 * Trait ActionsTrait
 * @subpackage Cargo\Base
 */
trait ActionsTrait
{
    /**
     * @var string
     */
    protected $actionPrefix = 'action';

    /**
     * Visibility of actions
     * protected $actionVisibility = [
     *  'admin' => ['actionName1', 'actionName2', 'actionName3'],
     *  'user'  => ['actionName1', 'actionName3'],
     * ];
     *
     * @var array
     */
    protected $actionVisibility = [];

    /**
     * Returns a list of actions (methods), which can be called as "action[Name]"
     * Agreement on the renaming of actions: all names of the methods of action must begin with the word/prefix "action";
     * This method returns the names of the action methods without the word/prefix "action"
     * @param mixed $visibility
     * @return array
     */
    public function actions($visibility = null)
    {
        $prefix = $this->getActionPrefix();
        $result = [];
        foreach (get_class_methods( $this ) as $method) {
            /** prevent fetch self as action */
            if ($method == __FUNCTION__) {
                continue;
            }

            /** check */
            if (substr( $method, 0, strlen( $prefix ) ) == $prefix) {
                if ($this->visibleAction( $method, $visibility )) {
                    $result[] = strtolower( substr( $method, strlen( $prefix ) ) );
                }
            }
        }

        return $result;
    }

    /**
     * Get real action method name
     * @param $action
     * @return bool|string
     */
    public function getAction($action)
    {
        if ($this->hasAction( $action )) {
            return $this->getActionMethod( $action );
        }

        return false;
    }

    /**
     * @return string
     */
    public function getActionPrefix()
    {
        return $this->actionPrefix;
    }

    /**
     * @param $action
     * @return bool
     */
    public function hasAction($action)
    {
        return method_exists( $this, $this->getActionMethod( $action ) );
    }

    /**
     * @param string $actionPrefix
     */
    public function setActionPrefix($actionPrefix)
    {
        $this->actionPrefix = $actionPrefix;
    }

    /**
     * @param string $action
     * @return string
     */
    protected function getActionMethod($action)
    {
        return $this->getActionPrefix().Common::normalizeName( $action );
    }

    protected function getActionName($actionMethod)
    {

    }

    protected function visibleAction($actionMethod, $visibility = null)
    {
        if (!empty( $this->actionVisibility ) && ($visibility !== null)) {
            if (isset( $this->actionVisibility[$visibility] ) && is_array( $this->actionVisibility[$visibility] )) {
                return in_array( $actionMethod, $this->actionVisibility[$visibility] );
            } else {
                return false;
            }
        }

        return true;
    }
}