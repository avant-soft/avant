<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Event;

use Avant\Stdlib\CallbackInstance;

class Listener extends CallbackInstance
{
    protected $event    = '*';
    protected $one_off  = false;
    protected $priority = 1;

    public function __construct($callback, $event = '*', $priority = 1)
    {
        parent::__construct( $callback );

        $this->setEvent( $event );
        $this->setPriority( $priority );
    }

    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Get the value of one_off
     */
    public function getOneOff()
    {
        return $this->one_off;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function isOneOff()
    {
        return $this->one_off == true;
    }

    public function setEvent($event = '*')
    {
        $this->event = $event;
    }

    /**
     * Set the value of one_off
     *
     * @return  self
     */
    public function setOneOff($one_off)
    {
        $this->one_off = $one_off;

        return $this;
    }

    public function setPriority($priority = 1)
    {
        $this->priority = $priority;
    }
}