<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */
namespace Avant\Event;


use Avant\Exception\ETypeError;
use Avant\Http\Application;

trait EventTrait
{


    public function bind($event, $callback = null, $priority = 1)
    {
        if ($events = $this->getEventManager()) {
            return $events->bind( $event, $callback, $priority );
        }
    }

    public function getEventManager()
    {
        if (Application::getInstance()->hasComponent( 'events' ) == false) {
            return $this->setEventManager( new EventManager() );
        }

        return Application::getInstance()->getComponent( 'events' );
    }

    public function remove($listener)
    {
        if ($events = $this->getEventManager()) {
            return $events->remove( $listener );
        }
    }

    public function setEventManager($value)
    {
        if ($value instanceof EventManagerInterface) {
            throw new ETypeError( sprintf( '%s: expected a "EventManagerInterface"; received "%s"', __METHOD__, gettype( $value ) ) );
        }

        return Application::getInstance()->setComponent( 'events', $value );
    }

    public function trigger($event, $context = null, $params = [], $callback = null)
    {
        if ($events = $this->getEventManager()) {
            return $events->trigger( $event, $context, $params, $callback );
        }
    }

} 