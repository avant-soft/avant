<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Event;

use Avant\Base\Configurable;
use Avant\Exception\ETypeError;

class Event extends Configurable implements EventInterface
{
    protected $context;
    protected $fireStopped = false;
    protected $name;
    protected $params      = [];

    public function fireStop($flag = true)
    {
        $this->fireStopped = (bool)$flag;
    }

    public function fireStopped()
    {
        return $this->fireStopped;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getParam($name, $default = null)
    {
        if (is_array( $this->params )) {
            if (!array_key_exists( $name, $this->params )) {
                return $default;
            }

            return $this->params[$name];
        }

        if (!array_key_exists( $name, $this->params )) {
            return $default;
        }

        return $this->params->{$name};
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    public function setName($name)
    {
        $this->name = (string)$name;

        return $this;
    }

    public function setParam($name, $value)
    {
        if (is_array( $this->params )) {
            $this->params[$name] = $value;
        } else {
            $this->params->{$name} = $value;
        }

        return $this;
    }

    public function setParams($params)
    {
        if (!is_array( $params ) && !is_object( $params )) {
            throw new ETypeError( sprintf( 'Event parameters must be an array or object; received "%s"', gettype( $params ) ) );
        }

        $this->params = $params;

        return $this;
    }
}