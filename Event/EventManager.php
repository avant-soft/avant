<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Event;

use Avant\Base\ComponentAbstract;
use Avant\Exception\EObjectError;
use Avant\Exception\ETypeError;
use Avant\Stdlib\Collection;


class EventManager extends ComponentAbstract implements EventManagerInterface
{
    /** @var \Avant\Stdlib\Collection|array */
    protected $events = [];

    /**
     * Create listener for event
     *
     * @param mixed $event
     * @param mixed  $callback
     * @param int   $priority
     *
     * @return array|Listener
     * @throws ETypeError
     */
    public function bind($event, $callback = null, $priority = 1)
    {
        if ($callback === null) {
            throw new ETypeError( sprintf( '%s: expects a callback; null provided', __METHOD__ ) );
        }

        if (is_array( $event )) {
            $listeners = [];
            foreach ($event as $name) {
                $listeners[] = $this->bind( $name, $callback, $priority );
            }

            return $listeners;
        }

        if (empty( $this->events[$event] )) {
            $this->events[$event] = new Collection();
        }

        $listener = new Listener( $callback, $event, $priority );
        $this->events[$event]->append( $listener );

        return $listener;
    }

    /**
     * Create listener via array
     *
     * @param array $array
     *
     * @throws ETypeError
     */
    public function bindFromArray($array = [])
    {
        if (!is_array( $array ) || empty( $array )) {
            return;
        }
        if (!isset( $array['event'] ) || !isset( $array['callback'] )) {
            return;
        }

        $event    = $array['event'];
        $callback = $array['callback'];
        $priority = isset( $array['priority'] ) ? $array['priority'] : 1;

        $this->bind( $event, $callback, $priority );
    }

    public function getEvents()
    {
        return array_keys( $this->events );
    }

    /**
     * Get all listeners for event
     *
     * @param mixed $event
     *
     * @return Collection
     */
    public function getListeners($event)
    {
        $result = new Collection();

        foreach ([$event, '*'] as $key) {
            if (array_key_exists( $key, $this->events )) {
                $result->merge( $this->events[$key] );
            }
        }

        $result->uasort( function ($a, $b) {
            return $a->getPriority() - $b->getPriority();
        }
        );

        return $result;
    }

    /**
     * Remove listener event
     *
     * @param $listener
     *
     * @return bool
     * @throws EObjectError
     */
    public function remove($listener)
    {
        if (!$listener instanceof Listener) {
            throw new EObjectError( sprintf( '%s: expected a Listener; received "%s"', __METHOD__, (is_object( $listener ) ? get_class( $listener ) : gettype( $listener )) ) );
        }

        $event = $listener->getEvent();
        if (!$event || empty( $this->events[$event] )) {
            return false;
        }

        if (($key = $this->events[$event]->indexOf( $listener )) !== null) {
            $this->events[$event]->remove( $key );
            if ($this->events[$event]->count() == 0) {
                unset( $this->events[$event] );
            }

            return true;
        }

        return false;

    }

    /**
     * Call event
     *
     * @param       $event
     * @param mixed  $context
     * @param array $params
     * @param mixed  $callback
     *
     * @return Collection|mixed
     * @throws EObjectError
     */
    public function trigger($event, $context = null, $params = [], $callback = null)
    {
        if ($callback && !is_callable( $callback )) {
            throw new EObjectError( 'Invalid callback provided' );
        }

        if ($event instanceof Event) {
            return $this->triggerInternal( $event->getName(), $event, $callback );
        } else {
            return $this->triggerInternal( $event, new Event( ['name' => $event, 'context' => $context, 'params' => $params] ), $callback );
        }
    }

    /**
     * Processing event
     *
     * @param                $event
     * @param EventInterface $reference
     * @param mixed           $callback
     *
     * @return Collection|mixed
     */
    protected function triggerInternal($event, EventInterface $reference, $callback = null)
    {
        $effect    = new Collection();
        $listeners = $this->getListeners( $event );
        foreach ($listeners as $listener) {
            /** @var Listener $listener */
            $result = call_user_func( $listener->getCallback(), $reference );
            if (!empty( $result )) {
                $effect->append( $result );
                if ($callback && call_user_func( $callback, $result )) {
                    break;
                }
            }

            if ($listener->isOneOff()) {
                $this->remove( $listener );
            }

            if ($reference->fireStopped()) {
                break;
            }
        }

        return $effect;
    }
}