<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Event;


interface EventInterface
{
    public function fireStop($flag = true);

    public function fireStopped();

    public function getContext();

    public function getName();

    public function getParam($name, $default = null);

    public function getParams();

    public function setContext($context);

    public function setName($name);

    public function setParam($name, $value);

    public function setParams($params);

} 