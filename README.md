# AVANT #
Avant - is an application development framework - a set of tools, for people who build web applications using PHP. Its goal is to allow you to develop projects faster than if you were writing code from scratch by providing a rich set of libraries for frequently used tasks, as well as a simple interface and logical structure for accessing these libraries.

## License ##
Please see the [license agreement](https://bitbucket.org/avant-soft/avant/src/master/LICENSE.md).