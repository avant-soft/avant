<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config;

use Avant\Exception\EInOutError;
use Avant\Helpers\FileHelper;


/**
 * Class IOFactory
 * @subpackage Avant\Config
 */
class IOFactory
{
    public static    $invokeClasses = [
      'php'  => 'Avant\Config\IO\PhpArray',
      'xml'  => 'Avant\Config\IO\Xml',
      'json' => 'Avant\Config\IO\Json',
    ];
    protected static $dispatchers   = [];

    /**
     * @param $extension
     * @return IODriverAbstract|null
     */
    public static function dispatcher($extension)
    {
        $dispatcher = null;
        if (isset( self::$dispatchers[$extension] )) {
            return self::$dispatchers[$extension];
        }

        if (isset( self::$invokeClasses[$extension] )) {
            $className  = self::$invokeClasses[$extension];
            $dispatcher = self::$dispatchers[$extension] = new $className();
        }

        return $dispatcher;
    }

    public static function fromFile($filename, $type)
    {
        $result = [];
        $ext    = FileHelper::getFileExtension( $filename );
        $driver = self::dispatcher( $ext );
        if (($driver instanceof IODriverAbstract) === false) {
            throw new EInOutError( "Unsupported config file extension: '.{$ext}' for reading." );
        }

        if (file_exists( $filename )) {
            $driver->setConfigType( $type );
            $result = $driver->fromFile( $filename );

            // we have an environment directory?
            if ($env = getenv( 'APP_ENV' )) {
                $basename = basename( $filename );
                $f        = str_replace( $basename, $env."/".$basename, $filename );
                if (file_exists( $f )) {
                    $extended = $driver->fromFile( $f );
                    $result   = array_replace_recursive( $result, $extended );
                }
            }
        }


        return $result;
    }

    public static function toFile($filename, $data, $type)
    {
        $ext    = FileHelper::getFileExtension( $filename );
        $driver = IOFactory::dispatcher( $ext );
        if (($driver instanceof IODriverAbstract) === false) {
            throw new EInOutError( "Unsupported config file extension: '.{$ext}' for writing." );
        }

        $driver->setConfigType( $type );

        return $driver->toFile( $filename, $data );
    }
}

/* End of file IOFactory.php */
 