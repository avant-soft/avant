<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config\IO;

use Avant\Config\IODriverAbstract;


/**
 * Class Xml
 * @subpackage Avant\Config\Writer
 */
class Xml extends IODriverAbstract
{
    const indentStr = ' ';
    const tagName = 'item';

    public function decodeArray($string)
    {
        $this->reader = new \XMLReader();
        $this->reader->xml($string, null, LIBXML_XINCLUDE);
        $config = $this->decodeItem();
        $this->reader->close();

        return $config;
    }

    public function decodeItem()
    {
        $array = [];
        $text  = '';

        while ($this->reader->read())
        {
            if ($this->reader->nodeType === \XMLReader::ELEMENT)
            {
                if ($this->reader->depth === 0)
                {
                    return $this->decodeItem();
                }

                if ($this->reader->hasAttributes)
                {
                    $attributes = [];
                    while ($this->reader->moveToNextAttribute())
                    {
                        $attributes[$this->reader->localName] = $this->reader->value;
                    }
                    $this->reader->moveToElement();
                }

                if (isset($attributes['key']))
                {
                    $key = $attributes['key'];
                }
                else
                {
                    $key = $this->reader->name;
                }


                if ($this->reader->isEmptyElement)
                {
                    $value = null;
                }
                else
                {
                    $value = $this->decodeItem();
                }

                if (isset($array[$key]))
                {
                    if (!is_array($array[$key]) || !array_key_exists(0, $array[$key]))
                    {
                        $array[$key] = array( $array[$key] );
                    }
                    $array[$key][] = $value;
                }
                else
                {
                    $array[$key] = $value;
                }
            }
            elseif ($this->reader->nodeType === \XMLReader::END_ELEMENT)
            {
                break;
            }
            elseif (in_array($this->reader->nodeType, array( \XMLReader::TEXT, \XMLReader::CDATA, \XMLReader::WHITESPACE, \XMLReader::SIGNIFICANT_WHITESPACE )))
            {
                $text .= $this->reader->value;
            }
        }

        return $array ?: $text;
    }

    public function encodeArray(array $config)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= "\n<{$this->configType}>";
        $xml .= "\n" . trim($this->encodeItem($config));
        $xml .= "\n</{$this->configType}>";

        return $xml;
    }

    public function encodeItem($variable, $level = 0)
    {
        $search  = array( "\0", "\a", "\b", "\f", "\n", "\r", "\t", "\v" );
        $replace = array( '\0', '\a', '\b', '\f', '\n', '\r', '\t', '\v' );

        $str = '';

        switch (gettype($variable))
        {
            case 'boolean':
                $str .= $variable ? 'true' : 'false';
                break;
            case 'integer':
                $str .= $variable;
                break;
            case 'double':
                $str .= $variable;
                break;
            case 'resource':
                $str .= '[resource]';
                break;
            case 'resource (closed)':
                $str .= '[resource closed]';
                break;
            case 'NULL':
                $str .= "";
                break;
            case 'unknown type':
                $str .= '[unknown type]';
                break;
            case 'string':
                $variable = str_replace($search, $replace, $variable, $count);
                $str .= $variable;
                break;
            case 'array':
                if (count($variable) == 0)
                {
                    $str .= '';
                }
                else
                {
                    $spaces = str_repeat(self::indentStr, $level * 2);
                    $str .= "\n";

                    foreach ($variable as $key => $val)
                    {
                        if (empty($val))
                        {
                            $str .= $spaces . "<" . $this::tagName . " key=\"{$key}\"/>\n";
                        }
                        else
                        {
                            $str .= $spaces . "<" . $this::tagName . " key=\"{$key}\">";
                            $str .= $this->encodeItem($val, $level + 1);

                            if (is_array($val))
                            {
                                $str .= $spaces;
                            }

                            $str .= "</" . $this::tagName . ">" . $spaces . "\n";
                        }
                    }
                }
                break;
            case 'object':
                $array = (array) $variable;
                $spaces = str_repeat(self::indentStr, $level * 2);
                $str .= "\n";

                $properties = array_keys($array);
                foreach ($properties as $property)
                {
                    $name = str_replace("\0", ':', trim($property));
                    if (empty($array[$property]))
                    {
                        $str .= $spaces . "<" . $this::tagName . " key=\"{$name}\"/>\n";
                    }
                    else
                    {
                        $str .= $spaces . "<" . $this::tagName . " key=\"{$name}\">";
                        $str .= $this->encodeItem($array[$property], $level + 1);
                        if (is_array($array[$property]))
                        {
                            $str .= $spaces;
                        }

                        $str .= "</" . $this::tagName . ">" . $spaces . "\n";
                    }
                }
                break;
        }


        return $str;
    }
}

/* End of file Xml.php */
