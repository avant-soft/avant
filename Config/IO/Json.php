<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config\IO;

use Avant\Config\IODriverAbstract;


/**
 * Class Json
 * @subpackage Avant\Config\IO
 */
class Json extends IODriverAbstract
{
    public function decodeArray($string)
    {
        if (function_exists( 'json_decode' )) {
            return json_decode( $string, true );
        }

        return [];
    }

    public function encodeArray(array $config)
    {
        if (function_exists( 'json_encode' )) {
            return json_encode( $config );
        }

        return '';
    }
}

/* End of file Json.php */
 