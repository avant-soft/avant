<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config\IO;

use Avant\Config\IODriverAbstract;


/**
 * Class PhpArray
 * @subpackage Avant\Config\Writer
 */
class PhpArray extends IODriverAbstract
{
    const indentStr = '  ';

    public function decodeArray($string)
    {
    }

    public function encodeArray(array $config)
    {
        $php = "<?php if(!defined('BASE_PATH')) exit('No direct script access allowed');\n\n";
        if (!empty($config))
        {
            $php .= $this->encodeItem($config);
        }

        return $php;
    }

    function encodeItem($variable, $level = 0)
    {
        $search  = array( "\0", "\a", "\b", "\f", "\n", "\r", "\t", "\v" );
        $replace = array( '\0', '\a', '\b', '\f', '\n', '\r', '\t', '\v' );

        $str = '';

        switch (gettype($variable))
        {
            case 'boolean':
                $str .= $variable ? 'true' : 'false';
                break;
            case 'integer':
                $str .= $variable;
                break;
            case 'double':
                $str .= $variable;
                break;
            case 'resource':
                $str .= '[resource]';
                break;
            case 'resource (closed)':
                $str .= '[resource closed]';
                break;
            case 'NULL':
                $str .= "null";
                break;
            case 'unknown type':
                $str .= '[unknown type]';
                break;
            case 'string':
                $variable = str_replace($search, $replace, $variable, $count);
                $str .= '"' . str_replace("'", "\'", $variable) . '"';
                break;
            case 'array':
                if (count($variable) == 0)
                {
                    $str .= '[]';
                }
                else
                {
                    $spaces = str_repeat(self::indentStr, $level * 2);
                    if ($level > 0)
                    {
                        $str .= "[" . $spaces;
                    }
                    foreach ($variable as $key => $val)
                    {
                        $key_str = (is_int($key) ? $key : "'" . addslashes(trim($key)) . "'");
                        $str .= ($level > 0 ? "\n" : "") . $spaces . ($level == 0 ? '$' . $this->configType . "[$key_str] = " : "$key_str => ");
                        $str .= $this->encodeItem($val, $level + 1) . ($level == 0 ? ";\n" : ',');
                    }
                    if ($level > 0)
                    {
                        $str .= "\n" . $spaces . ']';
                    }
                }
                break;
            case 'object':
                $array = (array) $variable;
                $spaces = str_repeat(self::indentStr, $level * 2);
                if ($level > 0)
                {
                    $str .= "[" . $spaces;
                }
                $properties = array_keys($array);
                foreach ($properties as $property)
                {
                    $name = str_replace("\0", ':', trim($property));
                    $str .= ($level > 0 ? "\n" : "") . $spaces . ($level == 0 ? '$' . $this->configType . "['$name'] = " : "'$name' => ");
                    $str .= $this->encodeItem($array[$property], $level + 1) . ($level == 0 ? ";\n" : ',');
                }
                if ($level > 0)
                {
                    $str .= "\n" . $spaces . ']';
                }
                break;
        }

        return $str;
    }

    public function fromFile($filename)
    {
        if (!is_file($filename) || !is_readable($filename))
        {
            //throw new ConfigException(sprintf("File '%s' doesn't exist or not readable", $filename));
            return [];
        }

        include $filename;

        $type = $this->configType;
        if (!isset($$type) or !is_array($$type))
        {
            return [];
        }

        return $$type;
    }
}

/* End of file PhpArray.php */
