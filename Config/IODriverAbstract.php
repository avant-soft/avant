<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config;

use Avant\Exception\EArrayError;
use Avant\Exception\EBasicException;
use Avant\Exception\EFOpenError;
use Avant\Exception\EInOutError;
use Avant\Helpers\FileHelper;
use Avant\Stdlib\Collection;


/**
 * Class IODriverAbstract
 * @subpackage Avant\Config
 */
abstract class IODriverAbstract implements IODriverInterface
{
    protected $configType = 'config';

    public function fromFile($filename)
    {
        if (empty( $filename )) {
            throw new EBasicException( 'No file name specified' );
        }

        if (!is_file( $filename ) || !is_readable( $filename )) {
            throw new EInOutError( sprintf( "File '%s' doesn't exist or not readable", $filename ) );
        }

        if ($result = FileHelper::readFile( $filename )) {
            return $this->fromString( $result );
        }
        throw new EFOpenError( 'Failed to read file: '.$filename );
    }

    public function fromString($string)
    {
        if (empty( $string )) {
            return [];
        }

        return $this->decodeArray( $string );
    }

    public function getConfigType()
    {
        return $this->configType;
    }

    public function setConfigType($type)
    {
        $type             = preg_replace( '/[^a-z0-9_]/i', '', $type );
        $this->configType = $type;

        return $this;
    }

    public function toFile($filename, $config)
    {
        if (empty( $filename )) {
            throw new EInOutError( 'No file name specified' );
        }

        return FileHelper::writeFile( $filename, $this->toString( $config ) );
    }

    public function toString($config)
    {
        if ($config instanceof Collection) {
            $config = $config->toArray();
        }

        if (!is_array( $config )) {
            throw new EArrayError( __METHOD__.' expects an array config' );
        }

        return $this->encodeArray( $config );
    }

    abstract protected function decodeArray($string);

    abstract protected function encodeArray(array $config);
}

/* End of file IODriverAbstract.php */
 