<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config;

use Avant\Base\ComponentAbstract;
use Avant\Exception\EInOutError;
use Avant\Exception\ETypeError;
use Avant\Stdlib\Collection;


/**
 * Class Config
 * @subpackage Avant\Config
 */
class Config extends ComponentAbstract
{
    /** @var Collection */
    protected $config;
    /** @var string */
    protected $configType = 'config';


    public function __init()
    {
        $this->config = new Collection();
    }

    public function clear()
    {
        $this->config->clear();
    }

    function fromFile($filename, $type = null, $index = false)
    {
        if (empty( $filename )) {
            throw new ETypeError( sprintf( '%s: expected a "string"; received "%s"', __METHOD__, gettype( $filename ) ) );
        }

        if (!file_exists( $filename )) {
            throw new EInOutError( sprintf( "File '%s' doesn't exist or not readable", $filename ) );
        }

        if ($type === null) {
            $type = $this->configType;
        }

        $config = IOFactory::fromFile( $filename, $type );
        if (!empty( $config )) {
            if ($index) {
                $this->config->set( $index, $config );
            } else {
                $this->config->merge( $config, true );
            }
        }

        return $this;
    }

    public function getItem($item, $index = null, $default = false)
    {
        if ($index !== null && $this->config->exists( $index )) {
            if (array_key_exists( $item, $this->config->get( $index ) )) {
                return $this->config[$index][$item];
            }
        } else {
            if ($this->config->exists( $item )) {
                return $this->config[$item];
            }
        }

        return $default;
    }

    public function getItems($index = null, $default = false)
    {
        if ($index !== null) {
            if ($this->config->exists( $index )) {
                return $this->config[$index];
            }

            return $default;
        }

        return $this->config;
    }

    public function setItem($item, $value)
    {
        $this->config->set( $item, $value );
    }

    public function setItems($items, $index = false)
    {
        if (is_array( $items ) && !empty( $items )) {
            if ($index) {
                $this->config->set( $index, $items );
            } else {
                $this->config->merge( $items, true );
            }
        }
    }

    public function toFile($filename, $type = null)
    {
        if ($type === null) {
            $type = $this->configType;
        }

        return IOFactory::toFile( $filename, $this->config->toArray(), $type );
    }
}

/* End of file Config.php */
 