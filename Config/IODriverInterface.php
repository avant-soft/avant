<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Config;


/**
 * Interface IODriverInterface
 * @subpackage Avant\Config
 */
interface IODriverInterface
{
    public function fromFile($filename);

    public function fromString($string);

    public function toFile($filename, $config);

    public function toString($config);
} 