<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Log;

use Avant\Base\Constants;
use Avant\Exception\EFOpenError;
use Avant\Exception\EInOutError;
use Avant\Helpers\FileHelper;
use Avant\Helpers\PathHelper;


/**
 * Class LogFile
 * @subpackage Avant\Log
 * @deprecated deprecated since version 1.8.6
 */
class LogFile extends LogAbstract
{
    public $buffer = [];
    /**
     * Delimiter log-line columns
     * @var string
     */
    protected $delimiter = '~';
    /**
     * Extension log file
     * @var string
     */
    protected $ext = 'log';
    /**
     * File resource handle
     * @var resource
     */
    protected $fileHandler;
    /**
     * File name
     * @var string
     */
    protected $fileName;

    /**
     * File path
     * @var
     */
    protected $filePath;

    /**
     * Date format
     * @var string
     */
    protected $format = 'Y-m-d H:i:s';
    /**
     * Directory for log files
     * @var string
     */
    protected $path = '';

    /**
     * Max. count of log file (default 0)
     * @var int
     */
    protected $rotate = 0;

    /**
     * Max. size log file (default 0)
     * @var int
     */
    protected $size = 0;
    /**
     * Write immediately
     * @var bool
     */
    protected $write_directly = true;

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * Get extension of log file
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Get log filename
     * @return mixed
     */
    public function getFileName()
    {
        $result = $this->fileName;
        if (is_null( $result )) {
            $result = 'log';
        }

        $ext    = ($ext = pathinfo( $result, PATHINFO_EXTENSION )) ? $ext : $this->ext;
        $result = pathinfo( $result, PATHINFO_FILENAME );

        $indexFound = 0;
        $index      = 0;

        $location      = $this->getPath();
        $sizeThreshold = $this->getSize();
        $sizeData      = $this->dataSize();

        // fetch index
        $files = glob( "$location/*.$ext" );
        natsort( $files );
        foreach ($files as $file) {
            if (preg_match( '~('.$result.')\_([\d]+)\.'.$ext.'$~', $file, $matched )) {
                $indexFound = $matched[2];
                if ($sizeThreshold > 0) {
                    if (filesize( $file ) + $sizeData <= $sizeThreshold) {
                        $index = $indexFound;
                        break;
                    }
                }
            }
        }

        // index undefined
        if ($index == 0) {
            if (($limit = $this->getRotate())) {
                $reference = range( 1, $limit );
                $filesLogs = [];
                $files     = glob( "$location/*.$ext" );
                natsort( $files );
                foreach ($files as $file) {
                    if (preg_match( '~('.$result.')\_([\d]+)\.'.$ext.'$~', $file, $matched )) {
                        $suffix                        = $matched[2];
                        $filesLogs[filemtime( $file )] = $suffix;

                        // exclude index
                        unset( $reference[array_search( $suffix, $reference )] );
                    }
                }

                if (/*$indexFound >= $limit || */
                empty( $reference )
                ) {
                    ksort( $filesLogs );
                    $index = reset( $filesLogs );
                    if (file_exists( $fileLog = FileHelper::prepFilename( $location.'/'.$result.'_'.$index.'.'.$ext ) )) {
                        if ($fp = @fopen( $fileLog, 'r+' )) {
                            flock( $fp, LOCK_EX );
                            ftruncate( $fp, 0 );
                            fflush( $fp );
                            flock( $fp, LOCK_UN );
                            fclose( $fp );
                        }
                    }
                } else {
                    $index = reset( $reference );
                }
            } else {
                $index = ++$indexFound;
            }
        }

        $result = FileHelper::prepFilename( $location.'/'.$result.'_'.$index.'.'.$ext );

        return $result;
    }

    /**
     * Get date format
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Get log file folder
     * @return string
     * @throws EInOutError
     */
    public function getPath()
    {
        $result = FileHelper::prepLocation( $this->path );

        if ($result && !is_dir( $result )) {
            if (@mkdir( $result, Constants::DIR_WRITE_MODE, true ) == false) {
                throw new EInOutError( sprintf( 'Can not open folder "%s"', PathHelper::overlapPath( $result, defined( 'BASE_PATH' ) ? constant('BASE_PATH') : '/' ) ) );
            }
        }

        return $result;
    }

    /**
     * Max. count log file(s)
     * @return int
     */
    public function getRotate()
    {
        return $this->rotate && $this->getSize() ? $this->rotate : 0;
    }

    /**
     * Max. size log file
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Write directly
     * @return boolean
     */
    public function isWriteDirectly()
    {
        return $this->write_directly;
    }

    /**
     * Add log message
     *
     * @param string $message
     * @param int    $severity
     *
     * @return null
     * @throws EInOutError
     */
    public function log($message, $severity = self::LOG_INFO)
    {
        if (!$this->isEnabled() || $this->getThreshold() > $severity) {
            return;
        }

        $this->buffer[] = implode( $this->getDelimiter(), [$this->prepTime(), $this->prepSeverity( $severity ), $this->prepMessage( $message )] );
        if ($this->isWriteDirectly()) {
            $this->write();
        }
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * Set extension of log file
     *
     * @param string $ext
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
    }

    /**
     * Set the log filename-prefix
     *
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Set date format
     *
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * Set log file folder
     *
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Max. count log file(s)
     *
     * @param int $rotate
     */
    public function setRotate($rotate)
    {
        $this->rotate = $rotate;
    }

    /**
     * Max. size log file
     *
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * Write directly
     *
     * @param boolean $write_directly
     */
    public function setWriteDirectly($write_directly)
    {
        $this->write_directly = $write_directly;
    }

    /**
     * Write stacked message
     * @throws EFOpenError
     * @throws EInOutError
     */
    public function write()
    {
        if (!$this->isEnabled()) {
            return;
        }

        if ($this->open()) {
            flock( $this->fileHandler, LOCK_EX );
            fwrite( $this->fileHandler, $this->data( true ) );
            flock( $this->fileHandler, LOCK_UN );

            $this->close();
            @chmod( $this->filePath, Constants::FILE_WRITE_MODE );
        }
    }

    /**
     * Close log file
     */
    protected function close()
    {
        if ($this->fileHandler) {
            @fclose( $this->fileHandler );
            $this->fileHandler = null;
        }
    }

    /**
     * Get stacked log messages
     *
     * @param bool|FALSE $flush
     *
     * @return string
     */
    protected function data($flush = false)
    {
        $result = implode( "\n", $this->buffer )."\n";
        if ($flush === true) {
            $this->buffer = [];
        }

        return $result;
    }

    /**
     * Get size stacked log messages
     * @return int
     */
    protected function dataSize()
    {
        return strlen( $this->data() );
    }

    /**
     * Open log file
     * @throws EFOpenError
     */
    protected function open()
    {
        if ($this->fileHandler) {
            return true;
        }

        $this->filePath = $this->getFileName();
        if (!$this->fileHandler = @fopen( $this->filePath, Constants::FOPEN_WRITE_CREATE )) {
            throw new EFOpenError( 'Cannot open file on path: '.$this->filePath );
        }

        return true;
    }

    /**
     * Normalize message
     *
     * @param $message
     *
     * @return mixed
     */
    protected function prepMessage($message)
    {
        return str_replace( ["\n", '  '], '', $message );
    }

    /**
     * Re-present severity
     *
     * @param $value
     *
     * @return mixed
     */
    protected function prepSeverity($value)
    {
        return $value;
    }

    /**
     * Normalize time
     * @return string
     */
    protected function prepTime()
    {
        list( $msec, $sec ) = explode( ' ', @microtime() );

        return implode( $this->getDelimiter(), [
            date( $this->getFormat(), $sec ),
            str_pad( round( $msec * 1000, 0 ), 3, '0', STR_PAD_LEFT ),
          ]
        );
    }
}

/* End of file LogFile.php */
