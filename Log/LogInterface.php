<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Log;

/**
 * Interface LogInterface
 * @subpackage Avant\Log
 * @deprecated deprecated since version 1.8.6
 */
interface LogInterface
{
    const LOG_INFO    = 0;
    const LOG_DEBUG   = 1;
    const LOG_WARNING = 2;
    const LOG_ERROR   = 3;

    /**
     * Write log message
     * @param string $message
     * @param int    $severity
     * @return mixed
     */
    public function log($message, $severity = self::LOG_INFO);

}