<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Log;

use Avant\Base\ComponentAbstract;


/**
 * Class LogAbstract
 * @subpackage Avant\Log
 * @deprecated deprecated since version 1.8.6
 */
abstract class LogAbstract extends ComponentAbstract implements LogInterface
{
    protected $enabled   = false;
    protected $threshold = self::LOG_INFO;

    /**
     * @return int
     */
    public function getThreshold()
    {
        return $this->threshold;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @param int $threshold
     */
    public function setThreshold($threshold)
    {
        $this->threshold = $threshold;
    }


}

/* End of file LogAbstract.php */
