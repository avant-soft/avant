<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Loader;

require_once "ClassLoader.php";

/**
 * Class ClassAsset
 * @subpackage Avant\Loader
 */
class ClassAsset extends \ClassLoader
{
}

/* End of file ClassAsset.php */
