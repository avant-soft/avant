<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace
{

    use Avant\Base\Constants;

    /**
     * Class ClassLoader
     * @package  Avant

     */
    class ClassLoader
    {
        public static  $isRegistered  = false;
        private static $included      = [];
        private static $includedCount = 0;
        private static $loaded        = [];
        private static $separator     = '\\';
        private static $vendors       = [];

        private function __construct() { }

        public static function getIncluded()
        {
            return self::$included;
        }

        public static function getVendor($vendor)
        {
            if (isset( self::$vendors[$vendor] )) {
                return ['vendor' => $vendor, 'includePath' => self::$vendors[$vendor]];
            }
        }

        public static function getVendorName($className)
        {
            if ($className[0] != self::$separator) {
                $parts = explode( self::$separator, $className );

                return reset( $parts );
            }
        }

        public static function getVendors()
        {
            return self::$vendors;
        }

        public static function loadClass($className)
        {
            if ($className[0] != self::$separator) {
                $parts = explode( self::$separator, $className );
                if (isset( self::$vendors[reset( $parts )] )) {
                    $includePath  = self::$vendors[reset( $parts )];
                    $fileLocation = self::partsToPath( array_slice( $parts, 1 ) );
                    $fileName     = $includePath.DIRECTORY_SEPARATOR.$fileLocation.Constants::EXT;
                    if (is_readable( $fileName )) {
                        self::$included[++self::$includedCount] = $fileName;
                        require_once $fileName;
                    }
                }
            }
        }

        public static function loadInstance($classAlias, $instance = null, $override = false)
        {
            if (empty( $classAlias ) && empty( $instance )) {
                return null;
            }

            if (!is_null( $instance ) && is_object( $instance )) {
                if (!isset( self::$loaded[$classAlias] ) || $override == true) {
                    self::$loaded[$classAlias] =& $instance;
                }
            }

            if (isset( self::$loaded[$classAlias] )) {
                return self::$loaded[$classAlias];
            }

            return null;
        }

        public static function register()
        {
            if (self::$isRegistered === false) {
                // reg loader
                spl_autoload_register( '\ClassLoader::loadClass' );

                // self vendor register
                self::registerVendor( 'Avant', implode( '/', array_slice( explode( '/', str_replace( ['\\', '//'], '/', __DIR__ ) ), 0, -1 ) ) );

                self::$isRegistered = true;
            }
        }

        public static function registerVendor($vendor, $includePath)
        {
            if (!isset( self::$vendors[$vendor] )) {
                self::$vendors[$vendor] = rtrim( str_replace( ['\\', '//'], '/', $includePath ), '/' );
            }
        }

        public static function unregister()
        {
            if (self::$isRegistered === true) {
                spl_autoload_unregister( 'ClassLoader::loadClass' );
                self::$isRegistered = false;
            }
        }

        private static function partToPath($part)
        {
            /*  Any validations & checks   */
            return $part;
        }

        private static function partsToPath(array $parts)
        {
            if (($partCount = count( $parts )) == 1) {
                return empty( $parts ) ? $parts : self::partToPath( $parts[0] );
            }

            for ($i = 0; $i < $partCount - 1; $i++) {
                $parts[$i] = self::partToPath( $parts[$i] );
            }

            return implode( DIRECTORY_SEPARATOR, $parts );
        }

        // private function __clone() { }

        // private function __wakeup() { }
    }
}

/* End of file ClassLoader.php */
