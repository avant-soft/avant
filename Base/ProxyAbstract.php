<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Base;


/**
 * Class ProxyAbstract
 * @subpackage Avant\Base
 */
abstract class ProxyAbstract implements ProxyInterface
{
    public function __call($name, $args)
    {
        return $this->__callProxy( $name, $args );
    }

    public function __get($name)
    {
        return $this->__getProxy( $name );
    }

    abstract protected function __callProxy($method, $args);

    abstract protected function __getProxy($name);
}

/* End of file ProxyAbstract.php */
