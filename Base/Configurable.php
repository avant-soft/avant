<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Base;

use Avant\Stdlib\InitTrait;


/**
 * Class Configurable
 * @subpackage Avant\Base
 */
class Configurable
{
    use InitTrait;

    public function __construct($config = null, $strict = false, $context = null)
    {
        if (is_array( $config )) {
            $this->initialize( $config, $strict, $context );
        }
    }
}

/* End of file Configurable.php */
