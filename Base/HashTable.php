<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Base;


/**
 * Class HashTable
 * @subpackage Avant\Stdlib
 */
class HashTable
{
    protected $items = [];

    public function current()
    {
        return current( $this->items );
    }

    public function fetch()
    {
        if (!is_null( key( $this->items ) )) {
            $item = $this->current();
            $this->next();

            return $item;
        }

        return false;
    }

    public function first()
    {
        return reset( $this->items );
    }

    public function flush()
    {
        $this->items = [];
    }

    public function getItem($key)
    {
        if ($this->hasItem( $key )) {
            return $this->items[$key];
        }
    }

    /**
     * Find & return item index by property value
     *
     * @param $property
     * @param $value
     *
     * @return bool|int|string
     */
    public function getItemBy($property, $value)
    {
        $result = false;
        $method = 'get'.$property;
        foreach ($this->items as $key => $item) {
            if (method_exists( $item, $method )) {
                if ($item->{$method}() == $value) {
                    $result = $key;
                    break;
                }
            } elseif (property_exists( $item, $property )) {
                if ($item->{$property} == $value) {
                    $result = $key;
                    break;
                }
            }
        }

        return $result;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function hasItem($key)
    {
        return array_key_exists( $key, $this->items );
    }

    public function last()
    {
        return end( $this->items );
    }

    public function length()
    {
        return count( $this->items );
    }

    public function next()
    {
        return next( $this->items );
    }

    public function prev()
    {
        return prev( $this->items );
    }

    public function remove($key)
    {
        if ($this->hasItem( $key )) {
            unset( $this->items[$key] );
        }
    }

    public function setItem($key, $value)
    {
        $this->items[$key] = $value;

        return $this;
    }

    public function setItems(array $items)
    {
        foreach ($items as $key => $value) {
            $this->setItem( $key, $value );
        }

        return $this;
    }
}

/* End of file HashTable.php */
