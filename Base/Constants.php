<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Base;


/**
 * Class Constants
 * @subpackage Avant\Base
 */
class Constants
{
    // File and Directory Modes
    const FILE_READ_MODE  = 0644;
    const FILE_WRITE_MODE = 0666;
    const DIR_READ_MODE   = 0755;
    const DIR_WRITE_MODE  = 0777;

    // File Stream Modes
    const FOPEN_READ                          = 'rb';
    const FOPEN_READ_WRITE                    = 'r+b';
    const FOPEN_WRITE_CREATE_DESTRUCTIVE      = 'wb';  // truncates existing file
    const FOPEN_READ_WRITE_CREATE_DESTRUCTIVE = 'w+b'; // truncates existing file
    const FOPEN_WRITE_CREATE                  = 'ab';
    const FOPEN_READ_WRITE_CREATE             = 'a+b';
    const FOPEN_WRITE_CREATE_STRICT           = 'xb';
    const FOPEN_READ_WRITE_CREATE_STRICT      = 'x+b';

    // The PHP file extension
    const EXT = '.php';
}

/* End of file Constants.php */
