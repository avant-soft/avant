<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Base;


/**
 * Class Predicable
 * @subpackage Avant\Base
 */
class Predicable extends Configurable
{
    protected $attributes = [];

    public function __construct($config = null)
    {
        parent::__construct( $config, false );
    }

    public function __get($name)
    {
        return $this->getAttribute( $name );
    }

    public function __set($name, $value)
    {
        $this->setAttribute( $name, $value );
    }

    public function deleteAttribute($key)
    {
        if ($this->hasAttribute( $key )) {
            unset( $this->attributes[$key] );
        }
    }

    public function getAttribute($key, $default = null)
    {
        if ($this->hasAttribute( $key )) {
            return $this->attributes[$key];
        }

        return $default;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function hasAttribute($key)
    {
        return array_key_exists( $key, $this->attributes );
    }

    public function setAttribute($key, $value = null)
    {
        return $this->attributes[$key] = $value;
    }

    public function setAttributes(array $value)
    {
        $this->attributes = $value;
    }
}

/* End of file Predicable.php */
