<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */
namespace Avant\Base;

use Avant\Exception\EPropertyError;
use Avant\Stdlib\Common;


/**
 * Class Component
 * @subpackage Avant\Base
 */
abstract class ComponentAbstract extends Configurable implements ComponentInterface
{
    public function __construct($config = null)
    {
        parent::__construct( $config );
        $this->__init();
    }

    public static function className()
    {
        return get_called_class();
    }

    public function __get($name)
    {
        if ($this->hasMethod( $method = 'get'.Common::normalizeName( $name ) )) {
            return $this->$method();
        } else {
            throw new EPropertyError( 'Getting unknown property: '.get_class( $this ).'::'.$name );
        }
    }

    public function __init() { }

    public function __set($name, $value)
    {
        if ($this->hasMethod( $method = 'set'.Common::normalizeName( $name ) )) {
            return $this->$method( $value );
        } else {
            throw new EPropertyError( 'Setting unknown property: '.get_class( $this ).'::'.$name );
        }
    }

    public function hasMethod($name)
    {
        return method_exists( $this, $name );
    }

    public function hasProperty($name)
    {
        $normalizeName = Common::normalizeName( $name );

        return method_exists( $this, 'get'.$normalizeName ) || method_exists( $this, 'set'.$normalizeName ) || property_exists( $this, $name );
    }
}

/* End of file Component.php */
