<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Base;


/**
 * Class HashTableItem
 * @subpackage Avant\Stdlib
 */
abstract class HashTableItemAbstract extends Configurable
{
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        if ($this->id == null) {
            $this->id = uniqid();
        }

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    abstract public function toArray();
}

/* End of file HashTableItem.php */
