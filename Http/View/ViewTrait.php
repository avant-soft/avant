<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View;

use Avant\Http\Application;


/**
 * Trait ViewTrait
 * @subpackage Avant\Http\View
 */
trait ViewTrait
{
    private function getView()
    {
        $result = Application::getInstance()->get( 'view' );
        if (!$result instanceof ViewManager) {

        }
    }
}