<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Plugin;

use Avant\Event\EventTrait;
use Avant\Stdlib\InstanceManager;


/**
 * Class PluginManager
 * @subpackage Avant\Http\View
 */
class PluginManager extends InstanceManager
{
    use EventTrait;

    const EVENT_FETCH_PLUGINS = 'plugin:Fetch';
    const EVENT_DISPATCH      = 'plugin:Dispatch';
    const PI_REGISTERED       = 1;
    const PI_ACTIVE           = 2;
    const PI_NAMES            = 3;

    protected $invokableClasses = [
      'benchmark' => 'Avant\Http\View\Plugin\Benchmark',
      'headtitle' => 'Avant\Http\View\Plugin\HeadTitle',
    ];

    public function __construct()
    {
        /** Register custom plugins */
        foreach ($this->trigger( self::EVENT_FETCH_PLUGINS, $this ) as $result) {
            foreach ($result as $alias => $config) {
                $this->set( $alias, $config, true, true );
            }
        }
    }

    /**
     * Override native get()
     * @param       $name
     * @param array $config
     * @return null|object|callable
     * @throws \Avant\Exception\EMatchError
     */
    public function get($name, $config = [])
    {
        $plugin = parent::get( $name, $config );
        if ($plugin instanceof PluginInterface) {
            $plugin->setPluginManager( $this );
        }

        return $plugin;
    }

    /**
     * Return plugin(s) list
     * @return array
     */
    public function getRegistered()
    {
        return $this->invokableClasses;
    }
}

/* End of file PluginManager.php */
