<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Plugin;

use Avant\Http\Application;


/**
 * Class Benchmark
 * @subpackage Avant\Http\View\Plugin
 */
class Benchmark extends PluginAbstract
{
    protected $instance;

    public function __call($name, $arguments)
    {
        return call_user_func_array( [$this->getInstance(), $name], $arguments );
    }

    public function __invoke()
    {
        return $this;
    }

    public function getInstance()
    {
        if ($this->instance === null) {
            $this->instance = Application::getInstance()->getComponent( 'benchmark' );
        }

        return $this->instance;
    }
}

/* End of file Benchmark.php */
