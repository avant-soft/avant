<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Plugin;

use Avant\Exception\ETypeError;


/**
 * Class PluginAbstract
 * @subpackage Avant\Http\View\Plugin
 */
abstract class PluginAbstract implements PluginInterface
{
    /**
     * @var PluginManager
     */
    protected $plugin_manager;

    public function getPluginManager()
    {
        if ($this->plugin_manager === null) {
            $this->plugin_manager = new PluginManager();
        }

        return $this->plugin_manager;
    }

    public function setPluginManager($instance)
    {
        if (!$instance instanceof PluginManager) {
            throw new ETypeError( sprintf(
                'Expected \Avant\Http\View\Plugin\PluginManager, got type "%s" instead',
                (is_object( $instance ) ? get_class( $instance ) : gettype( $instance ))
              )
            );
        }
        $this->plugin_manager = $instance;
    }

}

/* End of file PluginAbstract.php */
