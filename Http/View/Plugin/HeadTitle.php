<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Plugin;


/**
 * Class HeadTitle
 * @subpackage Avant\Http\View\Plugin
 */
class HeadTitle extends PluginAbstract implements PluginInterface
{
    protected $title = 'Title';

    public function __invoke($title = null)
    {
        $title = (string)$title;

        if ($title !== '') {
            $this->title = $title;
        }

        return $this;
    }

    public function __toString()
    {
        return (string)'<title>'.$this->title.'</title>';
    }
}

/* End of file HeadTitle.php */
