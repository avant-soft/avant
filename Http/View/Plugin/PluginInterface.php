<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Plugin;


/**
 * Interface PluginInterface
 * @subpackage Avant\Http\View\Plugin
 */
interface PluginInterface
{
    public function getPluginManager();

    public function setPluginManager($instance);
}