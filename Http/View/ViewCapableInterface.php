<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View;


/**
 * Interface ViewCapableInterface
 * @subpackage Avant\Http\View
 */
interface ViewCapableInterface
{
    /**
     * @return ViewManager
     */
    public function getViewManager();

    /**
     * @param ViewManager $instance
     * @return mixed
     */
    public function setViewManager($instance);
}