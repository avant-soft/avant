<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View;

use Avant\Event\EventTrait;
use Avant\Event\Listener;

/**
 * Class Template
 * @package  Avant
 */
class Template
{
    use EventTrait;

    /**
     * @var mixed
     */
    protected $content;
    /**
     * @var string
     */
    protected $filename;
    /**
     * @var Listener
     */
    protected $listener;
    /**
     * @var Template|null
     */
    protected $parent;

    /**
     * @var bool
     */
    protected $partial = false;

    /**
     * @var array
     */
    protected $variables = [];

    public function __construct($filename, $variables = null)
    {

        $this->listener = $this->bind( ViewManager::EVENT_RENDER, function () {

            if ($this->listener !== null) {
                $this->remove( $this->listener );
            }

            return $this;
        }
        );

        $this->setFilename( $filename );
        $this->setVariables( $variables );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->content;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return null|Template
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->listener->getPriority();
    }

    /**
     * @param      $name
     * @param mixed $default
     * @return null
     */
    public function getVariable($name, $default = null)
    {
        $name = (string)$name;
        if (array_key_exists( $name, $this->variables )) {
            return $this->variables[$name];
        }

        return $default;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return ($this->parent !== null);
    }

    /**
     * @return boolean
     */
    public function isPartial()
    {
        return ($this->partial == true);
    }

    /**
     * @param          $name
     * @param Template $template
     * @return $this
     */
    public function setChild($name, Template $template)
    {
        $template->setParent( $this );
        $this->setVariable( $name, $template );

        return $this;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param $filename
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = (string)$filename;

        return $this;
    }

    /**
     * @param Template $parent
     * @return $this
     */
    public function setParent(Template $parent)
    {
        $this->parent = $parent;
        $this->parent->setPriority( $this->listener->getPriority() );

        return $this;
    }

    /**
     * @param boolean $partial
     */
    public function setPartial($partial)
    {
        $this->partial = $partial;
    }

    /**
     * @param int $val
     * @return $this
     */
    public function setPriority($val = 1)
    {
        if ($this->listener instanceof Listener) {
            if ($val >= $this->listener->getPriority()) {
                $this->listener->setPriority( ++$val );
            }

            if ($this->hasParent()) {
                $this->parent->setPriority( $this->listener->getPriority() );
            }
        }

        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setVariable($name, $value)
    {
        $this->variables[(string)$name] = $value;

        return $this;
    }

    /**
     * @param $variables
     * @return $this
     */
    public function setVariables($variables)
    {
        if (empty( $variables )) {
            return $this;
        }

        foreach ($variables as $key => $value) {
            $this->setVariable( $key, $value );
        }

        return $this;
    }
}

/* End of file Template.php */
