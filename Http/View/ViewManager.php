<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View;

use Avant\Event\EventTrait;
use Avant\Exception\ETypeError;
use Avant\Helpers\FileHelper;
use Avant\Http\View\Renderer\RendererInterface;
use Avant\Http\View\Renderer\RendererPhp;
use Avant\Stdlib\InitTrait;

/**
 * Class View
 * @subpackage Avant\Http\View
 */
class ViewManager
{
    use InitTrait, EventTrait;

    const EVENT_BEFORE_RENDER   = 'view:BeforeRender';
    const EVENT_RENDER          = 'view:Render';
    const EVENT_RENDERER        = 'view:Renderer';
    const EVENT_APPEND_LOCATION = 'view:AppendLocation';

    protected $invokeRenderers = [
      'php'  => 'Avant\Http\View\Renderer\RendererPhp',
      'json' => 'Avant\Http\View\Renderer\RendererJson',
    ];

    /**
     * Default layout alias
     * @var string
     */
    protected $layout = '';

    /**
     * @var array
     */
    protected $locations = [];
    /**
     * @var RendererInterface
     */
    protected $renderer;
    protected $views = [];

    public function __construct($config = [])
    {
        $this->initialize( $config, true );
        $this->bind( self::EVENT_APPEND_LOCATION, [$this, 'setLocation'] );
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function getLocations()
    {
        return $this->locations;
    }

    public function getRenderer()
    {
        if ($this->renderer == null) {
            /**
             * @var \Avant\Stdlib\Collection $results
             */
            $results = $this->trigger( self::EVENT_RENDERER, $this, [], function ($result) {
                return ($result instanceof RendererInterface);
            }
            );

            $renderer = $results->forward();
            if (!$renderer instanceof RendererInterface) {
                $renderer = new RendererPhp();
            }

            $this->setRenderer( $renderer );
        }

        return $this->renderer;
    }

    public function getView($alias)
    {
        if (isset( $this->views[$alias] )) {
            return $this->views[$alias];
        }

        return null;
    }

    public function hasView($alias)
    {
        return isset( $this->views[$alias] );
    }

    public function render(Template $template, $config = [])
    {
        // trigger for external actions
        $this->trigger( self::EVENT_BEFORE_RENDER, $this );

        // pre config
        if (count( $config )) {
            $this->initialize( $config, true );
        }

        // trigger
        $this->trigger( self::EVENT_RENDER, $this, [], [$this->getRenderer(), 'render'] );

        if ($template->isPartial()) {
            // return template content
            return $template->getContent();
        }

        // render layout if template is not partial
        $layout = new Template( $this->getLayout(), ['content' => $template] );

        return $this->getRenderer()->render( $layout, true );
    }

    public function resolve($view)
    {
        // preserve
        $viewFile = $view;

        // exists in $views?
        if ($this->hasView( $view )) {
            $viewFile = $this->getView( $view );
        }

        // $viewFile is file?
        if (!is_file( $viewFile )) {
            $viewFilename = FileHelper::prepFilename( $viewFile );
            foreach ($this->locations as $path) {
                if (is_file( $location = FileHelper::prepLocation( $path.$viewFilename ) )) {
                    $this->setView( $view, $location );

                    return $location;
                }
            }
            // if no $viewFile is found by locations - try to return the default template
            if ($this->hasView( 'default' )) {
                return $this->getView( 'default' );
            }
        } else {
            return $viewFile;
        }

        return false;
    }

    public function setLayout($alias, $path = null)
    {
        if (!empty( $path )) {
            if (!isset( $this->views[$alias] )) {
                $this->setView( $alias, $path );
            }
        }
        $this->layout = $alias;
    }

    public function setLocation($location, $primary = true)
    {
        if (is_array( $location )) {
            return $this->setLocations( $location );
        }

        $location = FileHelper::prepPath( $location );

        if (($location != '/') && !in_array( $location, $this->locations )) {
            if ($primary == true) {
                array_unshift( $this->locations, $location );
            } else {
                $this->locations[] = $location;
            }
        }

        return $this;
    }

    public function setLocations($locations, $primary = true)
    {
        if (is_array( $locations )) {
            foreach ($locations as $location) {
                $this->setLocation( $location, $primary );
            }
        }

        return $this;
    }

    /**
     * @param RendererInterface $renderer
     * @throws ETypeError
     */
    public function setRenderer($renderer)
    {
        // if $renderer alias or className
        if (is_string( $renderer )) {
            $className = $renderer;

            if (isset( $this->invokeRenderers[$className] )) {
                $className = $this->invokeRenderers[$className];
            }

            if (class_exists( $className )) {
                $renderer = new $className();
            }
        }

        if (!$renderer instanceof RendererInterface) {
            throw new ETypeError(
              sprintf(
                'Renderer must extend \Avant\Http\View\Renderer\RendererInterface; got type "%s" instead',
                (is_object( $renderer ) ? get_class( $renderer ) : gettype( $renderer ))
              )
            );
        }

        $this->renderer = $renderer;
        $this->renderer->setViewManager( $this );
    }

    public function setView($alias, $path)
    {
        $this->views[$alias] = $path;
    }

    public function setViews($views)
    {
        if (is_array( $views )) {
            foreach ($views as $alias => $path) {
                if (!is_numeric( $alias )) {
                    $this->setView( $alias, $path );
                }
            }
        }

        return $this;
    }
}

/* End of file View.php */
