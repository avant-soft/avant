<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Renderer;

use Avant\Exception\EInOutError;
use Avant\Exception\EMatchError;
use Avant\Exception\ETypeError;
use Avant\Http\View\Plugin\PluginManager;
use Avant\Http\View\Template;
use Avant\Http\View\ViewManager;


/**
 * Class RendererHtml
 * @subpackage Avant\Http\View\Renderer
 */
class RendererPhp extends RendererAbstract
{
    /**
     * @var \Avant\Http\View\Plugin\PluginManager
     */
    protected $plugins;
    protected $views;

    public function __construct()
    {
        /**
         * Лимит обратных ссылок PCRE
         * При малых значениях могут не обрабатываться шаблоны целиком
         * По-умолчанию: 1000000 (но бывают исключения)
         */
        ini_set( 'pcre.backtrack_limit', '2000000' );

        /**
         * Лимит на рекурсию. Не забывайте о том, что если вы установите достаточно высокое значение,
         * то PCRE может превысить размер стэка (установленный операционной системой) и в конце концов вызовет крушение PHP
         * По-умолчанию: 100000
         */
        ini_set( 'pcre.recursion_limit', '500000' );
    }

    public function __call($method, $args)
    {
        $plugin = $this->plugin( $method );

        if (is_callable( $plugin )) {
            return call_user_func_array( $plugin, array_values($args) );
        }

        return $plugin;
    }

    public function __get($name)
    {
        if ($this->getPluginManager()->has( $name )) {
            return $this->plugin( $name );
        }

        return null;
    }

    public function getPluginManager()
    {
        if ($this->plugins == null) {
            $this->setPluginManager( new PluginManager() );
        }

        return $this->plugins;
    }

    public function getViewManager()
    {
        return $this->views;
    }

    /**
     * @param $str
     * @return string
     */
    public function parsePlugins($str)
    {

        /**
         * изоляция синтаксиса "фильтров" для input & textarea
         * критично для использования разметки "фильтров" при редактировании (под)шаблонов через панель управления
         */
        $patterns = array(
          '~<textarea.*?>(.*?)</textarea>~isu',
          '~<input.*?(?:value\s*=\s*"(.*?)"\s*).*?/?>~isu',
        );

        foreach ($patterns as $pattern) {
            preg_match_all( $pattern, $str, $matches );
            for ($i = 0; $i < count( $matches[0] ); $i++) {
                if (!empty( $matches[1][$i] )) {
                    $replace = str_replace( $matches[1][$i], str_replace( array('{', '}'), array('<insul>', '</insul>'), $matches[1][$i] ), $matches[0][$i] );
                    $str     = str_replace( $matches[0][$i], $replace, $str );
                    unset( $replace );
                }
            }
        }

        preg_match_all( '~{('.implode( '|', array_keys( $this->getPluginManager()->getRegistered() ) ).')(?:[\: ]([^}{]+))?}(?:([\x00-\xFF]*?){/\\1})?~i', $str, $matches );
        foreach ($matchesUnique = array_unique( $matches[0] ) as $key => $value) {
            if ($this->getPluginManager()->has( $matches[1][$key] )) {
                $plugin = $this->getPluginManager()->get( $matches[1][$key] );
                if (is_callable( $plugin )) {

                    // any string, below after ":"
                    $params = trim( $matches[2][$key] );

                    // params may be string '-var1=val1 --var2=val2 -var3 value3 --var4="value4"', key value pair based
                    if (preg_match_all( '~ --?(?<key> [^= ]+ ) [ =] (?|" (?<value> [^\\\\"]*+ (?s:\\\\.[^\\\\"]*)*+ ) "|([^ ?"]*) )~x', $params, $matches_internal ) !== false) {
                        if (count( $matches_internal['key'] )) {
                            $params = array_combine( $matches_internal['key'], $matches_internal['value'] );
                        }
                    }

                    // params may be string 'param1[|param2|param3|..paramN]', value based
                    if (is_scalar( $params ) && strpos( $params, '|' ) !== false) {
                        $params = explode( '|', $params );
                    }

                    // params is string?
                    if (is_scalar( $params )) {
                        $params = [$params];
                    }

                    // add to params any string from "content" part - {plugin}content{/plugin}
                    if (!empty( $matches[3][$key] )) {
                        $params[] = trim( $matches[3][$key] );
                    }

                    $replacement = call_user_func_array( $plugin, array_values($params) );
                    if (is_scalar( $replacement ) || (is_object( $replacement ) && method_exists( $replacement, '__toString' ))) {
                        $pattern = "~".preg_quote( $value )."~i";
                        $str     = preg_replace( $pattern, trim( $replacement ), $str );
                    }
                    unset( $replacement, $pattern );
                }
                unset( $plugin );
            }
        }

        /**
         *  реконструкция "фильтров" для input & textarea
         */
        $str = str_replace( array('<insul>', '</insul>'), array('{', '}'), $str );

        return trim( $str );
    }

    /**
     * @param       $name
     * @param array $config
     * @return null|object|callable
     */
    public function plugin($name, $config = [])
    {
        return $this->getPluginManager()->get( $name, $config );
    }

    /**
     * @param Template|null $template
     * @param bool|FALSE    $return
     * @return string|null
     * @throws EInOutError
     * @throws \Avant\Exception\EMatchError
     * @throws \Exception
     */
    public function render($template = null, $return = false)
    {
        if ($template instanceof Template) {

            $vars = $template->getVariables();
            if (array_key_exists( 'this', $vars )) {
                unset( $vars['this'] );
            }
            extract( $vars );

            try {
                ob_start();
                if ($template->getFilename()) {
                    $filename      = $this->view( $template->getFilename() );
                    $includeReturn = include($filename);
                    $buffer        = ob_get_clean();

                    if ($includeReturn === false && empty( $buffer )) {
                        throw new EInOutError( sprintf( '%s: Unable to render layout "%s"; file include failed', __METHOD__, $filename ) );
                    }
                } else {
                    $buffer = $template->getContent();
                }

            } catch (\Exception $exception) {
                @ob_end_clean();
                throw $exception;
            }

            $buffer = $this->parseStr( $buffer, $vars );
            $buffer = $this->parsePlugins( $buffer );

            $template->setContent( $buffer );

            if ($return == true) {
                return $buffer;
            }
        }

        return null;
    }

    /**
     * @param PluginManager $plugins
     * @return $this
     * @throws ETypeError
     */
    public function setPluginManager($plugins)
    {
        if (!$plugins instanceof PluginManager) {
            throw new ETypeError( sprintf( 'Plugin manager must extend \Avant\Http\View\Plugin\PluginManager; got type "%s" instead', (is_object( $plugins ) ? get_class( $plugins ) : gettype( $plugins )) ) );
        }
        $this->plugins = $plugins;

        return $this;
    }

    public function setViewManager($instance)
    {
        if (!$instance instanceof ViewManager) {
            throw new ETypeError(
              sprintf(
                'Renderer must extend \Avant\Http\View\ViewManager; got type "%s" instead',
                (is_object( $instance ) ? get_class( $instance ) : gettype( $instance ))
              )
            );
        }

        $this->views = $instance;
    }

    public function view($template)
    {
        if (($result = $this->getViewManager()->resolve( $template )) !== false) {
            return $result;
        }

        throw new EMatchError( sprintf( 'View "%s" is not found', $template ) );
    }

}

/* End of file RendererHtml.php */
