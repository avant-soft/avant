<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Renderer;


use Avant\Http\View\Template;

/**
 * Class RendererAbstract
 * @subpackage Avant\Http\View\Renderer
 */
abstract class RendererAbstract implements RendererInterface
{
    /**
     * @param $str
     * @param $vars
     * @return mixed
     */
    public function parseStr($str, $vars)
    {
        if (is_array( $vars ) && count( $vars ) > 0) {
            foreach ($vars as $key => $value) {
                if (is_string( $value ) || $value instanceof Template) {
                    $str = str_replace( "{".$key."}", $value, $str );
                }
            }
        }

        return $str;
    }
}

/* End of file RendererAbstract.php */
