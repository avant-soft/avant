<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Renderer;

use Avant\Http\View\Template;


/**
 * Class OutputRendererJson
 * @subpackage Avant\Http\View\Renderer
 */
class RendererJson implements RendererInterface
{
    public function getViewManager() { }

    public function render($template = null, $return = false)
    {
        if ($template instanceof Template) {
            $variables = $template->getVariables();
            array_walk( $variables, function (&$value, $key) {
                if ($value instanceof Template) {
                    if (is_string( $content = $value->getContent() )) {
                        $content = json_decode( $value->getContent() );
                    }
                    $value = $content;
                }
            }
            );

            $template->setContent( $content = json_encode( $variables ) );

            if ($return == true) {
                return $content;
            }
        }
    }

    public function setViewManager($instance) { }
}

/* End of file OutputRendererJson.php */
