<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\View\Renderer;

use Avant\Http\View\ViewCapableInterface;


/**
 * Interface RendererInterface
 * @subpackage Avant\Http\View\Renderer
 */
interface RendererInterface extends ViewCapableInterface
{
    /**
     * @param \Avant\Http\View\Template|null $template
     * @param bool|FALSE                     $return
     *
     * @return mixed
     */
    public function render($template = null, $return = false);
}