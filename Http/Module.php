<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Base\Configurable;
use Avant\Config\IOFactory;
use Avant\Event\Event;
use Avant\Event\EventTrait;
use Avant\Helpers\FileHelper;
use Avant\Http\Exception\EValidation;

/**
 * Class Module
 *
 * @subpackage Avant\Http
 */
class Module extends Configurable {
    use EventTrait;

    const EVENT_IDENTITY     = 'module:Identity';
    const EVENT_ROUTES       = 'module:fetchRoutes';
    const EVENT_ROUTES_REMAP = 'module:fetchRoutesRemap';

    protected $moduleName;
    protected $path;
    protected $registered = false;
    protected $require    = [];
    protected $version;

    protected $description = "";

    public function __construct($config = null) {
        parent::__construct($config);

        $this->trigger(Application::EVENT_REGISTER_VENDOR, null, ['vendor' => $this->moduleName, 'includePath' => $this->getSourcePath()]);

        $this->bind(self::EVENT_IDENTITY, function (Event $event) {
            if ($moduleName = $event->getParam('moduleName', false)) {
                if (strcmp(strtolower($moduleName), strtolower($this->moduleName)) == 0) {
                    // Verification of requirements
                    if (!empty($this->require)) {
                        foreach ($this->require as $key => $value) {
                            if (is_callable($value)) {
                                if (!($result = call_user_func($value))) {
                                    throw new EValidation(sprintf("Requirement '%s' of module %s not satisfied, execution is interrupted.", $key, $moduleName));
                                }
                            }
                        }
                    }
                    // Register
                    if ($this->registered == false) {
                        $this->trigger(Application::EVENT_REGISTER_MODULE, null, ['module' => $this]);
                        $this->registered = true;
                    }
                }
            }
        }, 0
        );

        $this->bind(self::EVENT_ROUTES, [$this, 'fetchRoutes']);
        $this->bind(self::EVENT_ROUTES_REMAP, [$this, 'fetchRoutesRemap']);
    }

    public function fetchRoutes() {
        if (is_file($file = FileHelper::prepFilename($this->path . '/config/routes'))) {
            return IOFactory::fromFile($file, 'routes');
        }

        return null;
    }

    public function fetchRoutesRemap() {
        if (is_file($file = FileHelper::prepFilename($this->path . '/config/routes-remap'))) {
            return IOFactory::fromFile($file, 'routes');
        }

        return null;
    }

    public function findFile($file, $base = null) {
        if (empty($base)) {
            $base = '/config';
        }
        if (is_file($file = FileHelper::prepFilename($this->path . $base . '/' . $file))) {
            return $file;
        }

        return false;
    }

    public function getSourcePath() {
        return $this->getPath('src');
    }

    /**
     * @return boolean
     */
    public function isRegistered() {
        return $this->registered;
    }

    /**
     * Get the value of moduleName
     */
    public function getModuleName() {
        return $this->moduleName;
    }

    /**
     * Set the value of moduleName
     *
     * @return  self
     */
    public function setModuleName($moduleName) {
        $this->moduleName = $moduleName;

        return $this;
    }

    /**
     * Get the value of path
     */
    public function getPath($location = null) {
        return FileHelper::prepPath($this->path . (!empty($location) ? $location : ''), false);
    }

    /**
     * Set the value of path
     *
     * @return  self
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get the value of registered
     */
    public function getRegistered() {
        return $this->registered;
    }

    /**
     * Set the value of registered
     *
     * @return  self
     */
    public function setRegistered($registered) {
        $this->registered = $registered;

        return $this;
    }

    /**
     * Get the value of require
     */
    public function getRequire() {
        return $this->require;
    }

    /**
     * Set the value of require
     *
     * @return  self
     */
    public function setRequire($require) {
        $this->require = $require;

        return $this;
    }

    /**
     * Get the value of version
     */
    public function getVersion() {
        return !empty($this->version) ?: '0.0.0';
    }

    /**
     * Set the value of version
     *
     * @return  self
     */
    public function setVersion($version) {
        $this->version = $version;

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }
}

/* End of file Module.php */
