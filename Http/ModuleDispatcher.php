<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Base\ComponentAbstract;
use Avant\Helpers\FileHelper;
use Avant\Stdlib\Collection;


/**
 * Class ModuleDispatcher
 * @subpackage Avant\Http
 */
class ModuleDispatcher extends ComponentAbstract
{
    protected $modules;
    protected $modules_path = '/modules';

    public function __init()
    {
        $this->modules = new Collection();
    }

    public function clearModules()
    {
        $this->modules->clear();
    }

    /**
     * Return existing module
     *
     * @param $moduleName
     * @return null|Module
     */
    public function getModule($moduleName)
    {
        if ($this->modules->exists( $moduleName )) {
            return $this->modules[$moduleName];
        }

        return null;
    }

    /**
     * Return of registered module
     * @return null|Module
     */
    public function getModuleRegistered()
    {
        /**
         * @var string $moduleName
         * @var Module $module
         */
        foreach ($this->modules as $module) {
            if ($module->isRegistered()) {
                return $module;
            }
        }

        return null;
    }

    /**
     * Return modules list
     * @return array|\Avant\Stdlib\Collection
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @return string
     */
    public function getModulesPath()
    {
        return $this->modules_path;
    }

    /**
     * @param array|\Avant\Stdlib\Collection $modules
     */
    public function loadModules($modules)
    {

        if (count( $modules )) {
            $this->clearModules();
            foreach ($modules as $moduleName => $config) {
                $config['path']             = $this->processModulePath( isset( $config['path'] ) ? $config['path'] : null );
                $config['moduleName']       = $moduleName;
                $this->modules[$moduleName] = new Module( $config );
            }
        }
    }

    /**
     * @param string $modules_path
     */
    public function setModulesPath($modules_path)
    {
        $this->modules_path = $modules_path;
    }

    protected function prepName($name)
    {
        return strtolower( strtr( $name, ['-' => '', '_' => '', ' ' => '', '\\' => '', '/' => ''] ) );
    }

    protected function processModulePath($path)
    {
        if (empty( $path )) {
            return FileHelper::prepPath( constant('APP_PATH') );
        }

        return FileHelper::prepPath( constant('APP_PATH').'/'.$this->modules_path.'/'.$path, false );
    }
}

/* End of file ModuleDispatcher.php */
 