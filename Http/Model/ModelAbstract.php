<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Stdlib\Common;


/**
 * Class ModelAbstract
 * @subpackage Avant\Http\Model
 */
abstract class ModelAbstract
{
    /** @var  \stdClass */
    protected $data;
    /** @var array */
    protected $errors = [];

    protected $key;

    protected $logID = 'system';

    public function __construct($data = null)
    {
        $this->key = md5( uniqid( __CLASS__, true ) );
        $this->load( $data );
    }

    public function __get($attribute)
    {
        if (method_exists( $this, $method = 'get'.Common::normalizeName( $attribute ) )) {
            return call_user_func( [$this, $method] );
        }

        return property_exists( $this->data, $attribute ) ? $this->data->$attribute : null;
    }

    public function __set($attribute, $value)
    {
        if (method_exists( $this, $method = 'set'.Common::normalizeName( $attribute ) )) {
            return call_user_func( [$this, $method], $value );
        }

        if ($this->validate( $attribute, $value )) {
            return $this->data->$attribute = $value;
        }

        return false;
    }

    public function addError($key, $message)
    {
        $this->errors[$key][] = $message;
    }

    public function flushData()
    {
        $this->data = new \stdClass();
        $this->flushErrors();
    }

    public function flushErrors()
    {
        $this->errors = [];
    }

    /**
     * @return \stdClass
     */
    public function getData()
    {
        return $this->data;
    }

    public function getErrors($key = null)
    {
        if ($key !== null) {
            if ($this->hasErrors( $key )) {
                return $this->errors[$key];
            }
        }

        return $this->errors;
    }

    /**
     * Get the value of key
     */
    public function getKey()
    {
        return $this->key;
    }

    public function hasErrors($key = null)
    {
        return $key && isset( $this->errors[$key] ) ? true : count( $this->errors );
    }

    public function load($data)
    {
        $this->flushData();
        if (is_array( $data ) && count( $data )) {
            foreach ($data as $attribute => $value) {
                if ($this->__set( $attribute, $value ) === false) {
                    return false;
                }
            }
            return true;
        }

        return false;
    }

    protected function validate($attribute, $value)
    {
        $validationSchema = $this->validationSchema();
        if (empty( $validationSchema )) {
            return true;
        }

        $validator = isset( $validationSchema[$attribute] ) ? $validationSchema[$attribute] : null;
        if (empty( $validator )) {
            return true;
        }

        if (is_callable( $validator )) {
            $result = call_user_func( $validator, $value );
        } elseif ($validator instanceof ModelValidatorInterface) {
            $result = $validator->validate( $value );
        }

        if (!isset( $result ) || $result == false) {
            $this->addError( $attribute, sprintf( "Invalid value %s of attribute '%s'", var_export( $value, true ), $attribute ) );

            return false;
        }

        return true;
    }

    abstract protected function validationSchema();

    public function getLogID(): string
    {
        return $this->logID;
    }

    public function setLogID(string $logID): self
    {
        $this->logID = $logID;
        return $this;
    }    
}

/* End of file ModelAbstract.php */
