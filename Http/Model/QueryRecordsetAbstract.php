<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Storages\PDO\DatabaseTrait;


/**
 * Class QueryRecordsetAbstract
 * @subpackage Avant\Http\Model
 */
abstract class QueryRecordsetAbstract extends RecordsetAbstract
{
    use DatabaseTrait;

    /**
     * @var FilterManager
     */
    protected $filters;
    /**
     * Page index
     * @var int
     */
    protected $page_index = 1;
    /**
     * Size of portion
     * @var int
     */
    protected $page_size = 25;

    /** @var array */
    protected $params = [];
    /**
     * @var SorterManager
     */
    protected $sorters;

    /**
     * @param array $args
     * @return $this
     */
    public function fetchRows(array $args = [])
    {
        if ($rows = $this->getDatabase()->loadSql( $this->getQuery(), $this->getParams() )->fetchAll( $args )) {
            return $this->setRows( $rows );
        }

        return $this->setRows( [] );
    }

    /**
     * @return FilterManager
     */
    public function getFilterManager()
    {
        if ($this->filters === null) {
            $this->filters = new FilterManager();
        }

        return $this->filters;
    }

    /**
     * @param bool|FALSE $assoc
     * @return array
     */
    public function getFilters($assoc = false)
    {
        return $this->getFilterManager()->getItems( $assoc );
    }

    /**
     * Returns the number of pages
     * @return int
     */
    public function getPageCount()
    {
        return ceil( $this->getRowsTotalCount() / $this->getPageSize() );
    }

    /**
     * Returns number of current page
     * @return int
     */
    public function getPageIndex()
    {
        return $this->page_index;
    }

    /**
     * Returns size of party
     * @return int
     */
    public function getPageSize()
    {
        return $this->page_size;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Returns query sql
     * @return string
     */
    abstract public function getQuery();

    /**
     * Return total count
     * @return int
     */
    public function getRowsTotalCount()
    {
        return $this->getRowsCount();
    }

    public function getRowsTotalIndex()
    {
        return (($this->getPageIndex() - 1) * $this->getPageSize()) + $this->getRowsIndex();
    }

    /**
     * @return SorterManager
     */
    public function getSorterManager()
    {
        if ($this->sorters === null) {
            $this->sorters = new SorterManager();
        }

        return $this->sorters;
    }

    /**
     * @param bool|FALSE $assoc
     * @return array
     */
    public function getSorters($assoc = false)
    {
        return $this->getSorterManager()->getItems( $assoc );
    }

    /**
     * @param FilterManager $value
     */
    public function setFilterManager($value)
    {
        if ($value instanceof FilterManager) {
            $this->filters = $value;
        }
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function setFilters(array $filters)
    {
        $this->getFilterManager()->setItems( $filters );

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setPageIndex($value)
    {
        if ($value && is_numeric( $value ) && $value != $this->page_index) {
            $this->page_index = $value;
        }

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setPageSize($value)
    {
        if (is_numeric( $value ) && $value != $this->page_size) {
            $this->page_size = $value;
        }

        return $this;
    }

    /**
     * @param $params
     * @return $this
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @param $value SorterManager
     * @return $this
     */
    public function setSorterManager($value)
    {
        if ($value instanceof SorterManager) {
            $this->sorters = $value;
        }

        return $this;
    }

    /**
     * @param array $sorters
     * @return $this
     */
    public function setSorters(array $sorters)
    {
        $this->getSorterManager()->setItems( $sorters );

        return $this;
    }
}

/* End of file QueryRecordsetAbstract.php */
