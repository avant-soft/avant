<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Stdlib\Common;


/**
 * Class RecordsetItem
 * @subpackage Avant\Http\Model
 */
class RecordsetItem
{
    public function __get($name)
    {
        if (method_exists( $this, $method = 'get'.Common::normalizeName( $name ) )) {
            return call_user_func( [$this, $method] );
        } elseif (property_exists( $this, $name )) {
            return $this->$name;
        }
    }

    public function __set($name, $value)
    {
        if (method_exists( $this, $method = 'set'.Common::normalizeName( $name ) )) {
            return call_user_func_array( [$this, $method], [$value] );
        } else {
            return $this->$name = $value;
        }
    }
}

/* End of file RecordsetItem.php */
