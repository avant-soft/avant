<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Event\EventTrait;
use Avant\Exception\EDatabaseError;
use Avant\Http\LoggerTrait;
use Avant\Stdlib\Common;
use Avant\Storages\PDO\DatabaseTrait;


/**
 * Class ModelTableAbstract
 * @subpackage Avant\Http\Model
 */
abstract class ModelTableAbstract extends ModelAbstract
{
    use DatabaseTrait, LoggerTrait, EventTrait;

    const ON_INSERT   = 0x01;
    const ON_UPDATE   = 0x02;
    const ON_DELETE   = 0x04;
    const ON_TRUNCATE = 0x08;

    const EVENT_BEFORE_INSERT   = 'model:beforeInsert';
    const EVENT_AFTER_INSERT    = 'model:afterInsert';
    const EVENT_BEFORE_UPDATE   = 'model:beforeUpdate';
    const EVENT_AFTER_UPDATE    = 'model:afterUpdate';
    const EVENT_BEFORE_DELETE   = 'model:beforeDelete';
    const EVENT_AFTER_DELETE    = 'model:afterDelete';
    const EVENT_BEFORE_TRUNCATE = 'model:beforeTruncate';
    const EVENT_AFTER_TRUNCATE  = 'model:afterTruncate';
    const EVENT_BEFORE_LOAD     = 'model:beforeLoad';
    const EVENT_AFTER_LOAD      = 'model:afterLoad';

    /**
     * The name of the default mode of operation.
     */
    const MODE_DEFAULT = 'default';

    /**
     * Model is existing in DB table
     * @var bool
     */
    protected $is_existing = false;

    /**
     * The mode that this model is in. Defaults to [[MODE_DEFAULT]].
     * @var string
     */
    protected $mode = self::MODE_DEFAULT;
    /**
     * Table primary key
     * @var string
     */
    protected $primary_key;
    /**
     * Table name
     * @var string
     */
    protected $table_name;

    public function __construct($data = null)
    {
        parent::__construct( $data );

        if ($this->getDatabase() === null) {
            throw new EDatabaseError( 'Database not detected, please check config' );
        }
    }

    public function __set($attribute, $value)
    {
        if ($attributes = $this->getAttributes()) {
            if (!in_array( $attribute, $attributes )) {
                return;
            }
        }

        parent::__set( $attribute, $value );
    }

    public function delete($id = null)
    {

        if ($this->hasErrors()) {
            $this->logError( sprintf( 'Model(%s) not deleted due to validation errors', get_called_class() ) );

            return false;
        }

        if (!$this->isTransactional( self::ON_DELETE )) {
            return $this->deleteInternal( $id );
        }

        $this->getDatabase()->begin();
        try {
            $result = $this->deleteInternal( $id );
            if ($result === false) {
                $this->getDatabase()->rollBack();
            } else {
                $this->getDatabase()->commit();
            }

            return $result;
        } catch (\Exception $except) {
            $this->getDatabase()->rollback();
            throw $except;
        }
    }

    public function find($id, $assign = true)
    {
        return $this->findByCondition( [$this->primary_key => $id], $assign );
    }

    public function findByCondition($condition, $assign = true)
    {
        $result = null;

        // flush self status
        if ($assign == true) {
            $this->is_existing = false;
        }

        if (($sql = $this->getQuery( $condition )) != null) {
            $row = $this->getDatabase()->loadSql( $sql )->fetch();
            if ($assign == true) {
                if ($this->load( $row ) === false) {
                    return null;
                }
                $this->is_existing = true;
            }

            $result = $row;
        }

        return $result;
    }

    public function getId()
    {
        return $this->__get( $this->primary_key );
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Returns a list of modes and the corresponding active attributes(fields).
     * The returned array should be in the following format:
     *
     * return
     * [
     *     'mode1' => ['attribute11', 'attribute12', ...],
     *     'mode2' => ['attribute21', 'attribute22', ...],
     *     ...
     * ];
     *
     * The default implementation of this method will return single mode found in the model
     * declaration. A special mode named [[MODE_DEFAULT]] will contain all attributes
     * found in the model. Each mode will be associated with the attributes that
     * are being validated by the validation rules that apply to the mode.
     *
     * @return array a list of modes and the corresponding active attributes.
     */
    public function getModes()
    {
        return [self::MODE_DEFAULT => []];
    }

    /**
     * Declares which DB operations should be performed within a transaction in different scenarios.
     * The supported DB operations are: [[ON_INSERT]], [[ON_UPDATE]], [[ON_DELETE]] and [[ON_TRUNCATE]]
     * which correspond to the [[insert()]], [[update()]] ,[[delete()]] and [[truncate]] methods, respectively.
     * By default, these methods are NOT enclosed in a DB transaction.
     *
     * In some modes, to ensure data consistency, you may want to enclose some or all of them
     * in transactions. You can do so by overriding this method and returning the operations
     * that need to be transactional. For example,
     *
     * return [
     *     'admin' => self::ON_INSERT,
     *     'user' => self::ON_INSERT | self::ON_UPDATE | self::ON_DELETE | self::ON_TRUNCATE,
     * ];
     *
     * The above declaration specifies that in the "admin" mode, the insert operation ([[insert()]])
     * should be done in a transaction; and in the "user" mode, all the operations should be done
     * in a transaction.
     *
     * @return array the declarations of transactional operations. The array keys are modes names,
     * and the array values are the corresponding transaction operations.
     */
    public function getTransactions() { return []; }

    public function insert()
    {

        if ($this->hasErrors()) {
            $this->logError( sprintf( 'Model(%s) not inserted due to validation errors', get_called_class() ) );

            return false;
        }

        if ($this->isExisting()) {
            return $this->update();
        }

        if (!$this->isTransactional( self::ON_INSERT )) {
            return $this->insertInternal();
        }

        $this->getDatabase()->begin();
        try {
            $result = $this->insertInternal();
            if ($result === false) {
                $this->getDatabase()->rollBack();
            } else {
                $this->getDatabase()->commit();
            }

            return $result;
        } catch (\Exception $except) {
            $this->getDatabase()->rollback();
            throw $except;
        }
    }

    public function isExisting()
    {
        return ($this->is_existing == true && $this->__get( $this->primary_key ) != null);
    }

    public function isTransactional($type)
    {
        $mode         = $this->getMode();
        $transactions = $this->getTransactions();

        return isset( $transactions[$mode] ) && ($transactions[$mode] & $type);
    }

    public function load($data)
    {
        if ($this->beforeLoad()) {
            $this->trigger( self::EVENT_BEFORE_LOAD, $this, ['data' => $data] );

            if (is_array( $data )) {
                if ($attributes = $this->getAttributes()) {
                    $data = array_intersect_key( $data, array_fill_keys( $attributes, 'empty' ) );
                }
            }

            $result = parent::load( $data );

            $this->afterLoad();
            $this->trigger( self::EVENT_AFTER_LOAD, $this );

            return $result;
        }
    }

    public function primaryKey()
    {
        return $this->primary_key;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function tableName()
    {
        if ($this->table_name === null) {
            $table            = Common::basename( get_called_class() );
            $table            = trim( strtolower( preg_replace( '/(?<![A-Z])[A-Z]/', '_\0', $table ) ), '_' );
            $this->table_name = $table;
        }

        return $this->table_name;
    }

    public function truncate()
    {
        if (!$this->isTransactional( self::ON_TRUNCATE )) {
            return $this->truncateInternal();
        }

        $this->getDatabase()->begin();
        try {
            $result = $this->truncateInternal();
            if ($result === false) {
                $this->getDatabase()->rollBack();
            } else {
                $this->getDatabase()->commit();
            }

            return $result;
        } catch (\Exception $except) {
            $this->getDatabase()->rollback();
            throw $except;
        }
    }

    public function update()
    {

        if ($this->hasErrors()) {
            $this->logError( sprintf( 'Model(%s) not updated due to validation errors', get_called_class() ) );

            return false;
        }

        if (!$this->isExisting()) {
            return $this->insert();
        }

        if (!$this->isTransactional( self::ON_UPDATE )) {
            return $this->updateInternal();
        }


        $this->getDatabase()->begin();
        try {
            $result = $this->updateInternal();
            if ($result === false) {
                $this->getDatabase()->rollBack();
            } else {
                $this->getDatabase()->commit();
            }

            return $result;
        } catch (\Exception $except) {
            $this->getDatabase()->rollback();
            throw $except;
        }
    }

    /**
     * What should I do after deleting?
     * @return mixed
     */
    protected function afterDelete()
    {

    }

    /**
     * What should I do after inserting?
     * @return mixed
     */
    protected function afterInsert()
    {

    }

    /**
     * What should I do after loading?
     * @return mixed
     */
    protected function afterLoad()
    {

    }

    /**
     * What should I do after truncating?
     * @return mixed
     */
    protected function afterTruncate()
    {

    }

    /**
     * What should I do after updating?
     * @return mixed
     */
    protected function afterUpdate()
    {

    }

    /**
     * What should I do before deleting? You must always return a Boolean value
     * @return bool [FALSE|TRUE]
     */
    protected function beforeDelete()
    {
        return true;
    }

    /**
     * What should I do before inserting? You must always return a Boolean value
     * @return bool [FALSE|TRUE]
     */
    protected function beforeInsert()
    {
        return true;
    }

    /**
     * What should I do before load? You must always return a Boolean value
     * @return bool [FALSE|TRUE]
     */
    protected function beforeLoad()
    {
        return true;
    }

    /**
     * What should I do before truncating? You must always return a Boolean value
     * @return bool [FALSE|TRUE]
     */
    protected function beforeTruncate()
    {
        return true;
    }

    /**
     * What should I do before updating? You must always return a Boolean value
     * @return bool [FALSE|TRUE]
     */
    protected function beforeUpdate()
    {
        return true;
    }

    protected function deleteInternal($id)
    {

        if ($id === null) {
            $id = $this->__get( $this->primary_key );
        }

        if ($this->beforeDelete()) {
            $this->trigger( self::EVENT_BEFORE_DELETE, $this );
            /**
             * @var \Avant\Storages\PDO\Builder\BuilderQueryAbstract $query
             */
            if ($query = $this->getDatabase()->builderQuery()) {
                $result = $query->where( $this->primary_key, $id )->delete( $this->tableName(), '', 1 );
                $this->afterDelete();
                $this->trigger( self::EVENT_AFTER_DELETE, $this );

                return $result;
            }
        }

        return false;
    }

    /**
     * Return list of active attributes of model based on current mode
     * @return array|null
     */
    protected function getAttributes()
    {
        if (!empty( $modeList = $this->getModes() )) {
            if (array_key_exists( $mode = $this->getMode(), $modeList )) {
                $attributes = $modeList[$mode];
                if (is_array( $attributes ) && !empty( $attributes )) {

                    if (!in_array( $this->primary_key, $attributes )) {
                        array_unshift( $attributes, $this->primary_key );
                    }

                    return $attributes;
                }
            }
        }

        return null;
    }

    protected function getQuery($condition)
    {
        $result = null;
        if ($query = $this->getDatabase()->builderQuery()) {
            $query
              ->prepare( true )
              ->select()
              ->from( $this->tableName() )
              ->where( $condition )
              ->limit( 1 );
            $result = $query->row();
        }

        return $result;
    }

    protected function getVars()
    {
        return get_object_vars( $this->data );
    }

    protected function insertInternal()
    {
        if ($this->beforeInsert()) {
            $this->trigger( self::EVENT_BEFORE_INSERT, $this );
            /**
             * @var \Avant\Storages\PDO\Builder\BuilderQueryAbstract $query
             */
            if ($query = $this->getDatabase()->builderQuery()) {
                if ($result = $query->set( $this->getVars() )->insert( $this->tableName() )) {
                    $this->__set( $this->primary_key, $this->getDatabase()->getLastInsertID() );
                    $this->is_existing = true;
                    $this->afterInsert();
                    $this->trigger( self::EVENT_AFTER_INSERT, $this );
                }

                return $result;
            }
        }

        return false;
    }

    protected function truncateInternal()
    {
        if ($this->beforeTruncate()) {
            $this->trigger( self::EVENT_BEFORE_TRUNCATE, $this );
            /**
             * @var \Avant\Storages\PDO\Builder\BuilderQueryAbstract $query
             */
            if ($query = $this->getDatabase()->builderQuery()) {
                $result = $query->truncate( $this->tableName() );
                $this->afterTruncate();
                $this->trigger( self::EVENT_AFTER_TRUNCATE, $this );

                return $result;
            }
        }

        return false;
    }

    protected function updateInternal()
    {
        if ($this->beforeUpdate()) {
            $this->trigger( self::EVENT_BEFORE_UPDATE, $this );
            /**
             * @var \Avant\Storages\PDO\Builder\BuilderQueryAbstract $query
             */
            if ($query = $this->getDatabase()->builderQuery()) {
                $result = $query->set( $this->getVars() )->where( $this->primary_key, $this->__get( $this->primary_key ) )->update( $this->tableName() );
                $this->afterUpdate();
                $this->trigger( self::EVENT_AFTER_UPDATE, $this );

                return $result;
            }
        }

        return false;
    }

    /**
     * Return validation schema
     * If you want to configure the validation of field values, the return value must be an array(see below):
     * return [
     *  'fieldName' => new StringValidator(8),
     *  ...
     * ];
     *
     * @return array|null
     */
    protected function validationSchema() { }
}

/* End of file ModelTableAbstract.php */
