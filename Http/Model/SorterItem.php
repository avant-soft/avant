<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Base\HashTableItemAbstract;
use Avant\Stdlib\InitTrait;
use Avant\Storages\PDO\Builder\BuilderQueryAbstract;


/**
 * Class SorterItem
 * @subpackage Avant\Http\Model
 */
class SorterItem extends HashTableItemAbstract
{
    use InitTrait;

    protected $column;
    protected $dir;
    private   $dir_available = ['ASC', 'DESC'];


    public function __invoke($context = null)
    {
        if ($context !== null) {
            return $this->apply( $context );
        } else {
            return $this;
        }
    }

    public function apply($context)
    {
        if ($context instanceof BuilderQueryAbstract) {
            $context->orderBy( $this->getColumn(), $this->getDir() );
        }

        return $this;
    }

    public function assign(SorterItem $sorter)
    {
        $this->id     = $sorter->getId();
        $this->column = $sorter->getColumn();
        $this->dir    = $sorter->getDir();
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getDir()
    {
        if ($this->dir === null) {
            $this->setDir( reset( $this->dir_available ) );
        }

        return $this->dir;
    }

    public function setColumn($value)
    {
        $value = strtolower( strtr( $value, ['-' => '_', ' ' => '', '\\' => '', '/' => '', '"' => '', "'" => ''] ) );
        if ($this->column !== $value) {
            $this->column = $value;
        }

        return $this;
    }

    public function setDir($value)
    {
        if (in_array( ($value = strtoupper( $value )), $this->dir_available )) {
            $this->dir = $value;
        }

        return $this;
    }

    public function toArray()
    {
        return [
          'id'     => $this->getId(),
          'column' => $this->getColumn(),
          'dir'    => $this->getDir(),
        ];
    }
}

/* End of file SorterItem.php */
