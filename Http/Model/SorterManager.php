<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Base\HashTable;
use Avant\Helpers\ArrayHelper;


/**
 * Class SorterManager
 * @subpackage Avant\Http\Model
 *
 */
class SorterManager extends HashTable
{
    public function createItem($column, $dir = null)
    {
        $item = new SorterItem( [
            'column' => $column,
            'dir'    => $dir,
          ]
        );

        return $this->setItem( $item, null );
    }

    /**
     * Get sorters
     * @param bool|FALSE $assoc
     * @return array
     */
    public function getItems($assoc = false)
    {
        if ($assoc == true) {
            $result = [];
            foreach ($this->items as $item) {
                /**
                 * @var SorterItem $item
                 */
                $result[$item->getId()] = $item->toArray();
            }

            return $result;
        }

        return parent::getItems();
    }

    public function indexOf($id)
    {
        return $this->getItemBy( 'id', $id );
    }

    /**
     * @param SorterItem|string|int $key
     * @param SorterItem|mixed      $value
     * @return $this
     */
    public function setItem($key, $value)
    {
        if ($key instanceof FilterItem && $value == null) {
            parent::setItem( $key->getId(), $key );
        } elseif (is_array( $value )) {
            if (ArrayHelper::keysExists( ['column', 'dir'], $value )) {
                $item = new SorterItem( $value );
                $item->setId( $key );
                parent::setItem( $item->getId(), $item );
            }
        }

        return $this;
    }
}

/* End of file SorterManager.php */
