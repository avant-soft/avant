<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Base\HashTableItemAbstract;
use Avant\Stdlib\InitTrait;
use Avant\Storages\PDO\Builder\BuilderQueryAbstract;


/**
 * Class FilterItem
 * @subpackage Avant\Http\Model
 */
class FilterItem extends HashTableItemAbstract
{
    use InitTrait;

    // Begin: Supported operators values
    const OPV_QT         = 'qt';
    const OPV_LT         = 'lt';
    const OPV_LTEQ       = 'lteq';
    const OPV_QTEQ       = 'qteq';
    const OPV_EQ         = 'eq';
    const OPV_NOTEQ      = 'noteq';
    const OPV_LIKE       = 'like';
    const OPV_LIKE_BEGIN = 'like-begin';
    const OPV_LIKE_END   = 'like-end';
    const OPV_LIKE_BOTH  = 'like-both';
    const OPV_IN         = 'in';
    const OPV_NOTIN      = 'notin';
    const OPV_IS         = 'is';
    const OPV_IS_NOT     = 'is-not';
    // End: Supported operators values

    const operatorsList = [
      self::OPV_QT         => 'more(>)',
      self::OPV_LT         => 'less(<)',
      self::OPV_LTEQ       => 'less or equal(<=)',
      self::OPV_QTEQ       => 'more or equal(>=)',
      self::OPV_EQ         => 'equal(==)',
      self::OPV_NOTEQ      => 'not equal(!=)',
      self::OPV_LIKE       => 'like',
      self::OPV_LIKE_BEGIN => 'begin with',
      self::OPV_LIKE_END   => 'ends with',
      self::OPV_LIKE_BOTH  => 'anywhere',
      self::OPV_IN         => 'in',
      self::OPV_NOTIN      => 'not in',
      self::OPV_IS         => 'is',
      self::OPV_IS_NOT     => 'is not',
    ];

    public static $columnFormat = [
      self::OPV_QT     => '%s > ',
      self::OPV_LT     => '%s < ',
      self::OPV_LTEQ   => '%s <= ',
      self::OPV_QTEQ   => '%s >= ',
      self::OPV_EQ     => '%s = ',
      self::OPV_NOTEQ  => '%s != ',
      self::OPV_IS     => '%s IS ',
      self::OPV_IS_NOT => '%s IS NOT ',
    ];

    protected $column;
    protected $operator;
    protected $type;
    protected $value;

    public static function columnFormat($operator, $column)
    {
        if (array_key_exists( $operator, self::$columnFormat )) {
            return sprintf( self::$columnFormat[$operator], $column );
        }

        return $column;
    }

    public function __invoke($context = null)
    {
        if ($context !== null) {
            return $this->apply( $context );
        } else {
            return $this;
        }
    }

    public function apply($context)
    {
        if ($context instanceof BuilderQueryAbstract) {

            //(or)where($key, $value = NULL, $escape = TRUE)
            //(or)where(Not)In($key = NULL, $values = NULL)
            //(or)like($field, $match = '', $side = 'both')

            $prefix = $this->type !== null ? strtolower( $this->type ) : '';//null|or|and
            $prefix = $prefix != 'or' ? '' : $prefix;

            switch ($operator = $this->getOperator()) {
                case self::OPV_QT;
                case self::OPV_LT;
                case self::OPV_LTEQ;
                case self::OPV_QTEQ;
                case self::OPV_EQ;
                case self::OPV_NOTEQ;
                    $method = $prefix.'where';
                    $args   = [self::columnFormat( $operator, $this->getColumn() ), $this->getValue(), true];
                    break;
                case self::OPV_LIKE;
                case self::OPV_LIKE_BEGIN;
                case self::OPV_LIKE_END;
                case self::OPV_LIKE_BOTH;
                    $side = 'both';
                    if ($operator == self::OPV_LIKE_BEGIN) {
                        $side = 'after';
                    } elseif ($operator == self::OPV_LIKE_END) {
                        $side = 'before';
                    } elseif ($operator == self::OPV_LIKE) {
                        $side = 'none';
                    }
                    $method = $prefix.'like';
                    $args   = [self::columnFormat( $operator, $this->getColumn() ), $this->getValue(), $side];
                    break;
                case self::OPV_IN;
                    $method = $prefix.'whereIn';
                    $args   = [self::columnFormat( $operator, $this->getColumn() ), $this->getValue()];
                    break;
                case self::OPV_NOTIN;
                    $method = $prefix.'whereNotIn';
                    $args   = [self::columnFormat( $operator, $this->getColumn() ), $this->getValue()];
                    break;
                case self::OPV_IS;
                case self::OPV_IS_NOT;
                    $method = $prefix.'where';
                    $args   = [self::columnFormat( $operator, $this->getColumn() ), $this->getValue(), false];
                    break;
            }

            if (isset( $method, $args )) {
                call_user_func_array( [$context, $method], $args );
            }
        }

        return $this;
    }

    public function assign(FilterItem $filter)
    {
        $this->id       = $filter->getId();
        $this->column   = $filter->getColumn();
        $this->operator = $filter->getOperator();
        $this->type     = $filter->getType();
        $this->value    = $filter->getValue();
    }

    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    public function setColumn($value)
    {
        $value = strtolower( strtr( $value, ['-' => '_', ' ' => '', '\\' => '', '/' => '', '"' => '', "'" => ''] ) );
        if ($this->column !== $value) {
            $this->column = $value;
        }

        return $this;
    }

    /**
     * @param $operator
     * @return $this
     */
    public function setOperator($operator)
    {
        if (array_key_exists( $operator, self::operatorsList )) {
            $this->operator = $operator;
        }

        return $this;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function toArray()
    {
        return [
          'id'       => $this->getId(),
          'column'   => $this->getColumn(),
          'operator' => $this->getOperator(),
          'type'     => $this->getType(),
          'value'    => $this->getValue(),
        ];
    }
}

/* End of file FilterItem.php */
