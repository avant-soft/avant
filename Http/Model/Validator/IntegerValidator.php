<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model\Validator;

use Avant\Http\Model\ModelValidatorInterface;


/**
 * Class IntegerValidator
 * @subpackage Avant\Http\Model\Validator
 */
class IntegerValidator implements ModelValidatorInterface
{
    public function validate($value)
    {
        return is_int( $value );
    }
}

/* End of file IntegerValidator.php */
