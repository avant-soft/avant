<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model\Validator;

use Avant\Http\Model\ModelValidatorInterface;


/**
 * Class BooleanValidator
 * @subpackage Avant\Http\Model\Validator
 */
class BooleanValidator implements ModelValidatorInterface
{
    public function validate($value)
    {
        return is_bool( $value );
    }
}

/* End of file BooleanValidator.php */
