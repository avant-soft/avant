<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model\Validator;

use Avant\Http\Model\ModelValidatorInterface;


/**
 * Class StringValidator
 * @subpackage Avant\Http\Model\Validator
 */
class StringValidator implements ModelValidatorInterface
{
    private $maxLength = null;

    public function __construct($maxLength = null)
    {
        $this->maxLength = $maxLength;
    }

    public function Validate($value)
    {
        $valid = is_string( $value );
        if ($valid && ($this->maxLength !== null)) {
            $valid = (strlen( $value ) <= $this->maxLength);
        }

        return $valid;
    }
}

/* End of file StringValidator.php */
