<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Stdlib\Common;


/**
 * Class RecordsetAbstract
 * @subpackage Avant\Http\Model
 */
abstract class RecordsetAbstract
{
    protected $columns    = [];
    protected $rows       = [];
    protected $rows_index = 0;

    public function fetchRow()
    {
        if (is_null( $row_index = key( $this->rows ) ) === false) {
            $this->rows_index = $row_index;
            $result           = $this->getRow();
            $this->nextRow();

            return $result;
        }

        return null;
    }

    function firstRow()
    {
        return reset( $this->rows );
    }

    public function flush()
    {
        $this->rows_index = 0;
        $this->rows       = [];
    }

    public function getColumn($column, $row = null)
    {

        if ($row === null) {
            $row = $this->getRow();
        }

        if (is_object( $row )) {
            if (method_exists( $row, $method = 'get'.Common::normalizeName( $column ) )) {
                return call_user_func( [$row, $method] );
            } elseif (property_exists( $row, $column )) {
                return $row->$column;
            }
        } else {
            if (isset( $row[$column] )) {
                return $row[$column];
            }
        }

        return null;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getColumnsCount()
    {
        return count( $this->columns );
    }

    public function getRow($index = null)
    {
        if ($index !== null) {
            if ($this->hasRow( $index )) {
                return $this->rows[$index];
            }
        }

        return current( $this->rows );
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function getRowsCount()
    {
        return count( $this->rows );
    }

    public function getRowsIndex()
    {
        return $this->rows_index;
    }

    public function hasRow($index)
    {
        return isset( $this->rows[$index] );
    }

    public function lastRow()
    {
        return end( $this->rows );
    }

    public function nextRow()
    {
        return next( $this->rows );
    }

    public function prevRow()
    {
        return prev( $this->rows );
    }

    /**
     * @param $columns
     * @return $this
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;

        return $this;
    }

    public function setRow($row)
    {
        if ($row = $this->normalize( $row )) {
            $this->rows[] = $row;
        }
    }

    public function setRows(array $rows)
    {
        $this->flush();
        foreach ($rows as $row) {
            $this->setRow( $row );
        }

        return $this;
    }

    protected function normalize($row)
    {
        if ($columns = $this->getColumns()) {
            // Only for array item
            if (is_array( $row )) {
                $row = array_intersect_key( (array)$row, array_fill_keys( $columns, 'empty' ) );
            } // Only for object item
            elseif (is_object( $row )) {
                foreach (array_keys( get_object_vars( $row ) ) as $property) {
                    if (!in_array( $property, $columns )) {
                        unset( $row->{$property} );
                    }
                }
            }
        }

        return $row;
    }


}

/* End of file RecordsetAbstract.php */
