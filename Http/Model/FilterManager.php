<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Model;

use Avant\Base\HashTable;
use Avant\Helpers\ArrayHelper;


/**
 * Class FilterManager
 * @subpackage Avant\Http\Model
 */
class FilterManager extends HashTable
{
    public function createItem($column, $operator, $value = null, $type = null)
    {
        $item = new FilterItem( [
            'column'   => $column,
            'operator' => $operator,
            'type'     => $type,
            'value'    => $value,
          ]
        );

        return $this->setItem( $item, null );
    }

    /**
     * @param      $id
     * @param bool $assoc
     *
     * @return array|FilterItem|bool|mixed
     * @throws \Avant\Http\Exception\EValidation
     */
    public function getItem($id, $assoc = false)
    {
        $result = false;
        if ($this->indexOf( $id )) {
            /**
             * @var FilterItem $result
             */
            $result = $this->items[$id];
            if ($assoc == true) {
                $result = $result->toArray();
            }
        }

        return $result;
    }

    /**
     * Get sorters
     *
     * @param bool|FALSE $assoc
     *
     * @return array
     */
    public function getItems($assoc = false)
    {
        if ($assoc == true) {
            $result = [];
            foreach ($this->items as $item) {
                /**
                 * @var FilterItem $item
                 */
                $result[$item->getId()] = $item->toArray();
            }

            return $result;
        }

        return parent::getItems();
    }

    /**
     * Find first filter by column name
     *
     * @param $id
     *
     * @return bool|int|string
     * @throws \Avant\Http\Exception\EValidation
     */
    public function indexOf($id)
    {
        return $this->getItemBy( 'id', $id );
    }

    /**
     * Add filter to items
     *
     * @param FilterItem|string|int $key
     * @param FilterItem|mixed      $value
     *
     * @return $this
     */
    public function setItem($key, $value)
    {
        if ($key instanceof FilterItem) {
            parent::setItem( $key->getId(), $key );
        } elseif (is_array( $value )) {
            if (ArrayHelper::keysExists( ['column', 'operator', 'value'], $value )) {
                $item = new FilterItem( $value );
                $item->setId( $key );
                parent::setItem( $item->getId(), $item );
            }
        }

        return $this;
    }
}

/* End of file FilterManager.php */
