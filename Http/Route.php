<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Base\Configurable;

/**
 * Class Route
 * @subpackage Avant\Http
 */
class Route extends Configurable
{
    const REGVAL     = '/({.+?})/';
    const REGVAL_REV = '/(\(.+?\))/';
    /**
     * Callback object
     * @var array|callable
     */
    protected $callback;
    /**
     * Supported request methods
     * @var array
     */
    protected $methods;
    /**
     * Name of route
     * @var string
     */
    protected $name;

    /**
     * RegEx pattern
     * @var string
     */
    protected $pattern;

    /**
     * Supported request types
     * @var integer default supported all types
     */
    protected $types = Request::REQUEST_HTTP | Request::REQUEST_AJAX | Request::REQUEST_CLI;

    private $wildcards = [':any' => '.*', ':num' => '[0-9]+', ':slug' => '[a-z\-]+', ':text' => '[a-zA-Z]+'];

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    public function getMethods()
    {
        return $this->methods;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed|string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    public function getTypes()
    {
        return $this->types;
    }

    public function getUrl(array $params)
    {
        $url = $this->fromRegex( $this->pattern );
        if ($params && preg_match_all( self::REGVAL_REV, $url, $matches )) {
            $matches      = array_slice( $matches[1], 0, count( $params ) );
            $matchesCount = count( $matches );
            for ($i = 0; $i < $matchesCount; $i++) {
                $url = str_replace( $matches[$i], $params[$i], $url );
            }
        }

        return $url;
    }

    /**
     * @param mixed $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    public function setMethods($methods)
    {
        if (!empty( $methods ) && is_string( $methods )) {
            $methods = explode( '|', strtoupper( $methods ) );
        }

        $this->methods = $methods;
    }

    public function setName($name)
    {
        if (!empty( $name )) {
            $this->name = (string)$name;
        }
    }

    /**
     * @param mixed|string $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $this->toRegex( $pattern );
    }

    public function setTypes($types)
    {

        if (!empty( $types ) && is_string( $types )) {
            $types_str = explode( '|', $types );
            $types     = 0;
            foreach (Request::REQUEST_TYPES as $type => $type_str) {
                if (array_search( $type_str, $types_str ) !== false) {
                    $types |= $type;
                }
            }
        }

        $this->types = $types;
    }

    protected function fromRegex($pattern)
    {
        $route = preg_replace_callback( self::REGVAL_REV, function ($matches) {
            $patterns   = array_flip( $this->wildcards );
            $matches[0] = str_replace( ['(', ')', '/^', '$/'], '', $matches[0] );
            if (in_array( $matches[0], array_keys( $patterns ) )) {
                return '({'.$patterns[$matches[0]].'})';
            } else {
                return '('.$matches[0].')';
            }

        }, $pattern
        );

        return str_replace( '\/', '/', trim( $route, '/^$' ) );
    }

    /**
     * @param $pattern
     *
     * @return mixed|string
     */
    protected function toRegex($pattern)
    {
        $route = preg_replace_callback( self::REGVAL, function ($matches) {
            $patterns   = $this->wildcards;
            $matches[0] = str_replace( ['{', '}'], '', $matches[0] );
            if (in_array( $matches[0], array_keys( $patterns ) )) {
                return $patterns[$matches[0]];
            }

            return null;
        }, $pattern
        );

        return '/^'.str_replace( '/', '\/', $route ).'$/';
    }
}

/* End of file Route.php */