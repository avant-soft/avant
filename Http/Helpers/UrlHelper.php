<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Helpers;

use Avant\Helpers\ArrayHelper;

/**
 * Class Url
 * @subpackage Avant\Stdlib
 */
class UrlHelper {
    public static function baseUrl($url = '') {
        return self::getServerProtocol() . '://' . self::getServerName() . (!empty($url) ? ltrim($url, '/') : '');
    }

    public static function getServerName() {
        return rtrim(ArrayHelper::element('HTTP_HOST', $_SERVER, 'localhost'), '/') . '/';
    }

    public static function getServerProtocol() {
        list($https, $port) = ArrayHelper::elements(['HTTPS', 'SERVER_PORT'], $_SERVER, ['off', 80]);

        return ($https !== 'off' || $port == 443) ? "https" : "http";
    }

    /**
     * @param string $url
     *
     * @return array|bool
     * Once URL is matched capturing groups will contain the following information:
     * Protocol: $1 (e.g. http, ftp, etc.)
     * Hostname: $2 (e.g. www.myhost.com)
     * Port: $3 (e.g. 8080)
     * Path: $4 (e.g. /catalogue/tables)
     * File: $5 (e.g. ProductDetails.aspx)
     * Query string: $6 (e.g. first=1&second=2)
     * Hash: $7 (e.g. #description)
     */
    public static function parse($url = '') {
        $pattern = '/^(?:(?P<protocol>\w+):\/{2})?(?P<hostname>[^\/:]+\.[\w]{2,16})(?:\:(?P<port>\d+))?(?P<path>\/(?:[^?]+\/)?)?(?P<file>[^\?#]+)?(?:\?(?P<query>[^#]*))?(?P<hash>\#.*)?$/i';
        if (preg_match($pattern, $url, $matches)) {
            return $matches;
        }

        return false;
    }

    /**
     * @param string $url
     *
     * @return array|bool
     * Protocol: $1
     * User: $3
     * Password: $4
     * Subdomain: $5
     * Domain: $8
     * DomainEnd: $9
     * Path: $10
     * File: $12
     * Filetype: $13
     * GET-Parameters: $15
     */
    public static function parseAlt($url = '') {
        $pattern = '/([a-z0-9_\-]{1,7}:\/\/)?(([a-z0-9_\-]{1,}):([a-z0-9_\-]{1,})\@)?((www\.)|([a-z0-9_\-]{1,}\.)+)?([a-z0-9_\-]{3,})(?\.[a-z]{2,8})(\/([a-z0-9_\-]{1,}\/)+)?([a-z0-9_\-]{1,})?(\.[a-z]{2,})?(\?)?(((\&)?[a-z0-9_\-]{1,}(\=[a-z0-9_\-]{1,})?)+)?/i';
        if (preg_match($pattern, $url, $matches)) {
            return $matches;
        }

        return false;
    }

    public static function redirect($uri = '', $method = 'location', $http_response_code = 302) {
        if (!headers_sent()) {
            if (!preg_match('#^https?://#i', $uri)) {
                $uri = self::baseUrl($uri);
            }

            switch ($method) {
            case 'refresh':
                header("Refresh:0;url=" . $uri);
                break;
            default:
                header("Location: " . $uri, true, $http_response_code);
                header("Redirected-To: " . $uri);
                break;
            }
        }
    }
}

/* End of file Url.php */
