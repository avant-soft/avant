<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */
namespace Avant\Http;


interface ApplicationProxyInterface
{
    public function getApplication();

    public function setApplication(Application $application);
}