<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Behaviors;

use Avant\Config\IOFactory;
use Avant\Event\Event;
use Avant\Http\Application;
use Avant\Http\ApplicationProxy;
use Avant\Http\Module;
use Avant\Loader\ClassAsset;


/**
 * Class ModuleBehavior
 * @subpackage Avant\Http\Listener
 */
final class ModuleBehavior extends ApplicationProxy
{
    public function __init()
    {
        $this->getEvents()->bind( Application::EVENT_REGISTER_MODULE, [$this, 'onModule'], 0 );
        $this->getEvents()->bind( Application::EVENT_REGISTER_VENDOR, [$this, 'onVendor'], 0 );
    }

    public function onModule(Event $event)
    {
        $module = $event->getParam( 'module', null );
        if ($module instanceof Module) {
            $this->getResolver()->addPath( $module->getPath() );

            if ($file = $this->getResolver()->findFile( 'module', '/config' )) {
                $this->getConfig()->fromFile( $file, 'config', false );

                /**
                 * Load components
                 */
                $components = $this->getConfig()->getItem( 'components', null, [] );
                foreach ($components as $name => $instance) {
                    $this->setComponent( $name, $instance, false );
                }

                /**
                 * Load behaviors
                 */
                $listeners = $this->getConfig()->getItem( 'behaviors', null, [] );
                foreach ($listeners as $name => $instance) {
                    $this->setBehavior( $name, $instance, false );
                }
            }
        }
    }

    public function onVendor(Event $event)
    {
        $params = $event->getParams();
        if (isset( $params['vendor'] ) && isset( $params['includePath'] )) {
            ClassAsset::registerVendor( $params['vendor'], $params['includePath'] );
        }
    }
}

/* End of file ModuleListener.php */
