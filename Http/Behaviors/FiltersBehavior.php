<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2022 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */


namespace Avant\Http\Behaviors;


use Avant\Http\ApplicationProxy;
use Avant\Http\Filter\FiltersManager;

/**
 * Class FiltersBehavior
 * @subpackage Avant\Http\Behaviors
 */
class FiltersBehavior extends ApplicationProxy
{
    /** @var FiltersManager */
    protected $filtersManager;

    public function __init()
    {
        $this->setFiltersManager( new FiltersManager( $this->request, $this->response ) );
        $this->getEvents()->bind( FiltersManager::EVENT_FETCH_FILTERS, [$this, 'fetchFilters'] );
        parent::__init();
    }

    public function fetchFilters()
    {
        return $this->getConfig()->getItem( 'filters', null, [] );
    }

    /**
     * @return FiltersManager
     */
    public function getFiltersManager(): FiltersManager
    {
        return $this->filtersManager;
    }

    /**
     * @param FiltersManager $filtersManager
     */
    public function setFiltersManager(FiltersManager $filtersManager): void
    {
        $this->filtersManager = $filtersManager;
    }
}

/* End of file FiltersBehavior.php */
