<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Behaviors;

use Avant\Event\Event;
use Avant\Event\EventManager;
use Avant\Exception\EMethodNotFound;
use Avant\Http\Application;
use Avant\Http\ApplicationProxy;
use Avant\Http\Controller\ControllerBasic;
use Avant\Http\Module;
use Avant\Http\Request;
use Avant\Http\RouteMatch;
use Avant\Http\Router;
use Avant\Loader\ClassAsset;

/**
 * Class RouteBehavior
 * @subpackage Avant\Http\Listener
 * @property EventManager $events
 * @property Router       $router
 * @property Request      $request
 */
final class RouteBehavior extends ApplicationProxy
{
    public function __init()
    {
        $this->getEvents()->bind(Application::EVENT_ROUTE_MATCH, [$this, 'onMatch'], 9999);
        $this->getEvents()->bind(Application::EVENT_ROUTE_DISPATCH, [$this, 'onDispatch'], 9999);
    }

    public function onDispatch(Event $event)
    {
        /**
         * Matched route
         * @var false|RouteMatch $route
         */
        if (($route = $event->getParam('route')) instanceof RouteMatch) {
            $callback = $route->getCallback();
            $args     = $route->getArgs();

            if (is_callable($callback)) {
                $this->getEvents()->trigger('dispatch:pre', $this, []);
                call_user_func_array($callback, $args);
                $this->getEvents()->trigger('dispatch:post', $this, []);

                return true;
            } elseif (is_array($callback)) {
                if (!isset($callback['controller'])) {
                    return false;
                }

                // Basic events
                $className = $callback['controller'];
                $method    = isset($callback['method']) ? $callback['method'] : 'index';

                // Load module
                if ($moduleName = ClassAsset::getVendorName($className)) {
                    $this->getEvents()->trigger(Module::EVENT_IDENTITY, null, ['moduleName' => $moduleName]);
                }

                $this->getEvents()->trigger(ControllerBasic::EVENT_BEFORE_CONSTRUCT, null, ['className' => $className]);
                $class = $this->setComponent('controller', new $className());
                $this->getEvents()->trigger(ControllerBasic::EVENT_CONSTRUCT, null, ['className' => $className]);

                // Override methods/properties
                if (!method_exists($class, $method)) {
                    if (method_exists($class, '__remap')) {
                        $method = '__remap';
                    } else {
                        throw new EMethodNotFound(sprintf('Method "%s" not found in "%s"', $method, str_replace('\\', "/", get_class($class))));
                    }
                }

                $this->getEvents()->trigger(ControllerBasic::EVENT_BEFORE_ACTION, $class, ['action' => $method]);
                call_user_func_array([$class, $method], $args);
                $this->getEvents()->trigger(ControllerBasic::EVENT_ACTION, $class, ['action' => $method]);

                return true;
            }
        }
    }

    public function onMatch(Event $event)
    {
        $event->setParam('route', ($matched = $this->getRouter()->match($this->getRequest()->url(), $this->getRequest()->method())));
        $event->fireStop($matched !== false);

        return $matched;
    }
}

/* End of file RouteListener.php */
