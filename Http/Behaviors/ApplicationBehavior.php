<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Behaviors;

use Avant\Event\Event;
use Avant\Helpers\BytesHelper;
use Avant\Http\Application;
use Avant\Http\ApplicationProxy;
use Avant\Http\Controller\ControllerBasic;
use Avant\Http\LoggerTrait;
use Avant\Http\ModuleDispatcher;
use Avant\Http\Router;
use Avant\Stdlib\Benchmark;


/**
 * Class ApplicationBehavior
 * @subpackage Avant\Http\Listener
 *
 * @method mixed set($name, $instance, $override = false)
 * @method mixed get($name, $config = [])
 * @method null halt($status, $message = null)
 * @method Benchmark getBenchmark()
 */
final class ApplicationBehavior extends ApplicationProxy
{
    use LoggerTrait;

    public function __init()
    {
        $this->getEvents()->bind( Application::EVENT_START, [$this, 'onStart'], -9999 );
        $this->getEvents()->bind( Application::EVENT_FINISH, [$this, 'onFinish'], 9999 );
        $this->getEvents()->bind( Application::EVENT_HALT, [$this, 'onHalt'], 9999 );
        $this->getEvents()->bind( '*', [$this, 'onEventLog'], 9980 );
    }

    public function onEventLog(Event $event)
    {
        switch ($event->getName()) {
            case Application::EVENT_BOOTSTRAP:
                $this->logDebug( 'Application bootstrap' );
                break;

            case Application::EVENT_START:               
                /** Application start */
                $this->logDebug( 'Application start' );
                break;

            case Application::EVENT_FINISH:
                /** Application finish */                
                $this->logDebug( 'Application finish' );

                /** Application memory used */
                $this->logDebug( sprintf( 'Application amount of memory allocated for PHP: %s kb.', BytesHelper::bytesTo( memory_get_usage( false ) ) ) );
                $this->logDebug( sprintf( 'Application peak value of memory allocated by PHP: %s kb.', BytesHelper::bytesTo( memory_get_peak_usage( false ) ) ) );
                break;
            case Application::EVENT_HALT:
                $this->logNotice( 'Application halt' );
                break;    
            default:
                $this->logDebug( 'Triggered '.$event->getName() );
                break;
        }
    }

    public function onFinish(Event $event)
    {
        if ($output = $event->getParam( 'output' )) {
            $this->getSender()->dispatch( $output );
            $event->fireStop( true );
        }

        $this->halt( $event->getParam( 'status', 0 ) );
    }

    public function onHalt(Event $event)
    {
        $this->halt( $event->getParam( 'status', 0 ), $event->getParam( 'message', '' ) );
    }

    public function onStart()
    {
        /**
         * Load default config
         */
        $this->getConfig()->fromFile( $this->getResolver()->findFile( 'application', 'config' ), 'config' );

        /**
         * Exception configure
         */
        $this->getErrors()->initialize( $this->getConfig()->getItem( 'errors', null, [] ) );

        /**
         * Modules load
         * @var ModuleDispatcher $modules
         */
        if ($modules_path = $this->getConfig()->getItem( 'modules_path' )) {
            $this->getModules()->setModulesPath( $modules_path );
        }
        $this->getModules()->loadModules( $this->getConfig()->getItem( 'modules', null, [] ) );

        /**
         * Routes load
         * @var Router $router
         */
        $this->getRouter()->fetchRoutes();
    }
}

/* End of file BasicListener.php */
