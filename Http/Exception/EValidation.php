<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Exception;

use Avant\Exception\EBasicException;


/**
 * Class EValidation
 * @subpackage Avant\Http\Exception
 */
class EValidation extends EBasicException
{

}

/* End of file EValidation.php */
