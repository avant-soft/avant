<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Exception;

use Avant\Event\EventTrait;
use Avant\Exception\OutputRendererAbstract;
use Avant\Http\Application;


/**
 * Class ExceptionRenderer
 * @subpackage Avant\Http\Exception
 */
class ExceptionRenderer extends OutputRendererAbstract
{
    use EventTrait;

    public function output($exception, $debug)
    {
        $status = ($exception instanceof \ErrorException) ? $exception->getSeverity() : $exception->getCode();
        if ($exception instanceof EHttpException) {
            $status = $exception->getStatusCode();
        }

        $this->send( $status, $this->format( $exception, $debug ) );
    }

    protected function send($status, $message = '')
    {
        $this->trigger( Application::EVENT_HALT, $this, ['status' => $status, 'message' => $message] );
    }
}

/* End of file ExceptionRenderer.php */
