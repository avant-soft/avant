<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Exception;

use Avant\Event\EventTrait;
use Avant\Helpers\PathHelper;
use Avant\Stdlib\Common;


/**
 * Class ExceptionRendererHtml
 * @subpackage Avant\Http\Exception
 */
class ExceptionRendererHttp extends ExceptionRenderer
{
    use EventTrait;

    protected $errTypes = [
      0                 => 'PHP Error was encountered',
      E_ERROR           => 'Error',
      E_WARNING         => 'Warning',
      E_PARSE           => 'Parsing Error',
      E_NOTICE          => 'Notice',
      E_CORE_ERROR      => 'Core Error',
      E_CORE_WARNING    => 'Core Warning',
      E_COMPILE_ERROR   => 'Compile Error',
      E_COMPILE_WARNING => 'Compile Warning',
      E_USER_ERROR      => 'User Error',
      E_USER_WARNING    => 'User Warning',
      E_USER_NOTICE     => 'User Notice',
      E_STRICT          => 'Runtime Notice',
      E_DEPRECATED      => 'Runtime Notice',
    ];

    /**
     * @param \ErrorException $exception
     * @param bool|FALSE      $debug TRUE if debug off
     * @return string
     */
    public function format($exception, $debug = false)
    {
        $template = str_replace( "\\", "/", rtrim( (defined( 'APP_PATH' ) ? constant('APP_PATH') : '/'), '/' ).sprintf( '/views/error_%s.php', $debug ? 'general' : 'php' ) );
        if (!file_exists( $template )) {
            return $exception->getMessage();
        }

        // Make vars
        extract( $this->exceptEncode( $exception ) );

        // Render
        ob_start();
        include $template;
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }

    protected function exceptEncode($exception)
    {
        return [
          'severity' => $severity = ($exception instanceof \ErrorException ? $exception->getSeverity() : $exception->getCode()),
          'header'   => isset( $this->errTypes[$severity] ) ? $this->errTypes[$severity] : $severity,
          'message'  => $exception->getMessage(),
          'filename' => PathHelper::overlapPath( $exception->getFile(), defined( 'BASE_PATH' ) ? constant('BASE_PATH') : '/' ),
          'line'     => $exception->getLine(),
        ];
    }

    protected function send($status, $message = '')
    {
        if (!headers_sent()) {
            $http_response_code = 500;
            $http_message       = 'Internal Server Error';
            if (isset( Common::$httpStatuses[$status] )) {
                $http_response_code = $status;
                $http_message       = Common::$httpStatuses[$status];
            }
            header( sprintf( 'HTTP/1.0 %d %s', $http_response_code, $http_message ), true, $http_response_code );
            header( sprintf( 'Status: %d %s', $http_response_code, $http_message ), true, $http_response_code );
        }
        parent::send( $status, $message );
    }
}

/* End of file ExceptionRendererHtml.php */
