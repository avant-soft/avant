<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Exception;

use Avant\Exception\OutputRendererCli;


/**
 * Class ExceptionRenderCli
 * @subpackage Avant\Http\Exception
 */
class ExceptionRendererCli extends OutputRendererCli
{

}

/* End of file ExceptionRenderCli.php */
