<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Exception;


/**
 * Class ExceptionRendererAjax
 * @subpackage Avant\Http\Exception
 */
class ExceptionRendererAjax extends ExceptionRendererHttp
{
    /**
     * @param \ErrorException $exception
     * @param bool|FALSE      $debug
     * @return string
     */
    public function format($exception, $debug = false)
    {
        $error = $this->exceptEncode( $exception );

        /** Detect response type */
        $types = [];
        foreach (['HTTP_ACCEPT', 'CONTENT_TYPE'] as $header) {
            if (isset( $_SERVER[$header] )) {
                $type = strtolower( $_SERVER[$header] );
                if (strpos( $type, ',' )) {
                    $type = current( explode( ',', $type ) );
                }
                $types[] = $type;
            }
        }

        // All supported types
        $formats = [
          'html'       => 'text/html',
          'htm'        => 'text/html',
          'json'       => 'application/json',
          'jsonp'      => 'application/javascript',
          'serialized' => 'application/vnd.php.serialized',
          'php'        => 'text/plain',
          'xml'        => 'application/xml',
        ];

        // Default 'html' where $mimeType == '*/*'
        $format = key( $formats );
        foreach ($formats as $key => $mimeType) {
            foreach ($types as $type) {
                if ($type == $mimeType) {
                    $format = $key;
                    break 2;
                }
            }
        }

        switch ($format) {
            case 'json':
            case 'jsonp':
                $result = json_encode( ['error' => $error], JSON_FORCE_OBJECT );
                break;
            case 'serialized':
                $result = serialize( ['error' => $error] );
                break;
            case 'xml':
                $xml   = new \SimpleXMLElement( "<?xml version='1.0' encoding='utf-8'?><error/>" );
                $array = array_flip( json_decode( json_encode( $error, JSON_FORCE_OBJECT ), true ) );
                array_walk_recursive( $array, [$xml, 'addChild'] );
                $result = $xml->asXML();
                break;
            default:
                $result = $error['message'];
        }

        return $result;
    }
}

/* End of file ExceptionRendererAjax.php */
