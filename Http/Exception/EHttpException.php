<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Exception;

use Avant\Exception\EBasicException;
use Avant\Stdlib\Common;


/**
 * Class EHttp
 * @subpackage Avant\Http\Exception
 */
class EHttpException extends EBasicException
{
    protected $statusCode = 500;

    public function __construct($status, $message = '')
    {
        parent::__construct( $message, $this->statusCode = $status );
    }

    public function getStatus()
    {
        if (isset( Common::$httpStatuses[$this->statusCode] )) {
            return Common::$httpStatuses[$this->statusCode];
        }

        return 'Error';
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}

/* End of file EHttp.php */
