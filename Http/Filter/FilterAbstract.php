<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2022 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */


namespace Avant\Http\Filter;


use Avant\Base\Configurable;
use Avant\Http\RequestInterface;
use Avant\Http\ResponseInterface;

/**
 * Class FilterAbstract
 * @subpackage Avant\Http\Filter
 */
abstract class FilterAbstract extends Configurable implements FilterInterface
{
    public function onInput(RequestInterface $input) { }

    public function onOutput(RequestInterface $input, ResponseInterface $output) { }
}

/* End of file FilterAbstract.php */
