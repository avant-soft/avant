<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2022 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */


namespace Avant\Http\Filter;


use Avant\Event\EventTrait;
use Avant\Http\Application;
use Avant\Http\RequestInterface;
use Avant\Http\ResponseInterface;
use Avant\Stdlib\InstanceManager;

/**
 * Class FilterManager
 * @subpackage Avant\Http\Filter
 */
class FiltersManager extends InstanceManager
{
    use EventTrait;

    const EVENT_FETCH_FILTERS = "filters:fetch";

    /** @var RequestInterface */
    protected $request;
    /** @var ResponseInterface */
    protected $response;

    public function __construct(RequestInterface $request, ResponseInterface $response)
    {
        $this->setRequest( $request );
        $this->setResponse( $response );

        $this->bind( Application::EVENT_START, function () {
            foreach ($this->trigger( self::EVENT_FETCH_FILTERS, $this ) as $result) {
                foreach ($result as $alias => $config) {
                    $this->set( $alias, $config, true, true );
                }
            }
        },
          -9900 );


        $this->bind( Application::EVENT_START, function () {
            foreach ($this->getInstances() as $filter) {
                if ($filter instanceof FilterInterface) {
                    $filter->onInput( $this->getRequest() );
                }
            }
        }, -9800 );

        $this->bind( Application::EVENT_FINISH, function () {
            foreach ($this->getInstances() as $filter) {
                if ($filter instanceof FilterInterface) {
                    $filter->onOutput( $this->getRequest(), $this->getResponse() );
                }
            }
        }, 9998 );
    }

    /**
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * @param RequestInterface $request
     */
    public function setRequest(RequestInterface $request): void
    {
        $this->request = &$request;
    }

    /**
     * @param ResponseInterface $response
     */
    public function setResponse(ResponseInterface $response): void
    {
        $this->response = &$response;
    }

}

/* End of file FilterManager.php */
