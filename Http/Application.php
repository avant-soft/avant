<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Config\Config;
use Avant\Event\EventManager;
use Avant\Exception\EBasicException;
use Avant\Exception\EComponentError;
use Avant\Exception\EMethodNotFound;
use Avant\Exception\EObjectError;
use Avant\Exception\EPropertyError;
use Avant\Exception\ExceptionHandler;
use Avant\Http\Exception\EHttpException;
use Avant\Stdlib\InstanceManager;
use Avant\Stdlib\Resolver;

/**
 * Class Application
 * @package Avant\Http
 * @method Config getConfig()
 * @method EventManager getEvents()
 * @method ExceptionHandler getErrors()
 * @method ModuleDispatcher getModules()
 * @method Request getRequest()
 * @method Resolver getResolver()
 * @method Response getResponse()
 * @method Router getRouter()
 * @method Uri getUri()
 * @property Config           $config
 * @property EventManager     $events
 * @property ExceptionHandler $errors
 * @property ModuleDispatcher $modules
 * @property Request          $request
 * @property Resolver         $resolver
 * @property Response         $response
 * @property Router           $router
 * @property Uri              $uri
 */
class Application
{

    const EVENT_BOOTSTRAP       = 'app:Bootstrap';
    const EVENT_START           = 'app:Start';
    const EVENT_HALTED          = 'app:Halted';
    const EVENT_HALT            = 'app:Halt';
    const EVENT_FINISH          = 'app:Finish';
    const EVENT_ROUTE_MATCH     = 'route:Match';
    const EVENT_ROUTE_DISPATCH  = 'route:Dispatch';
    const EVENT_REGISTER_MODULE = 'register:Module';
    const EVENT_REGISTER_VENDOR = 'register:Vendor';
    const EVENT_EXCEPTION       = 'app:Exception';

    protected static $instance = null;
    protected        $instanceManager;

    /**
     * Application constructor.
     *
     * @param InstanceManager|NULL $instanceManager
     */
    public function __construct(InstanceManager $instanceManager = null)
    {
        self::$instance        = $this;
        $this->instanceManager = !is_null( $instanceManager ) ? $instanceManager : new InstanceManager();
    }

    public static function getDefaultBehaviors()
    {
        return [
          'app'    => '\Avant\Http\Behaviors\ApplicationBehavior',
          'module' => '\Avant\Http\Behaviors\ModuleBehavior',
          'route'  => '\Avant\Http\Behaviors\RouteBehavior',
        ];
    }

    public static function getDefaultComponents()
    {
        return [
          'config'   => '\Avant\Config\Config',
          'events'   => '\Avant\Event\EventManager',
          'errors'   => '\Avant\Exception\ExceptionHandler',
          'modules'  => '\Avant\Http\ModuleDispatcher',
          'request'  => '\Avant\Http\Request',
          'resolver' => '\Avant\Stdlib\Resolver',
          'response' => '\Avant\Http\Response',
          'router'   => '\Avant\Http\Router',
          'uri'      => '\Avant\Http\Uri',
        ];
    }

    /**
     * @return Application|null
     */
    public static function &getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $method
     * @param $arguments
     *
     * @return object|null
     * @throws EMethodNotFound
     * @throws EPropertyError
     */
    public function __call($method, $arguments)
    {
        if (strncmp( strtolower( $method ), 'get', 5 )) {
            $alias = substr( $method, 3 );
            if ($object = $this->__get( $alias )) {
                return $object;
            }
        }

        throw new EMethodNotFound( sprintf( "Unknown method '%s()' of class '%s'", $method, __CLASS__ ) );

    }

    /**
     * @param $name
     *
     * @return object|null
     * @throws EPropertyError
     */
    public function __get($name)
    {
        if ($this->has( $name )) {
            return $this->get( $name );
        } elseif ($this->hasComponent( $name )) {
            return $this->getComponent( $name );
        } elseif ($this->hasBehavior( $name )) {
            return $this->getBehavior( $name );
        }

        throw new EPropertyError( 'Getting unknown property: '.get_class( $this ).'::'.$name );
    }

    /**
     * @param array $config
     *
     * @return $this
     * @throws EBasicException
     */
    public function bootstrap($config = [])
    {
        /**
         * Load components
         */
        $components = static::getDefaultComponents();
        if (isset( $config['components'] ) && is_array( $config['components'] )) {
            $components = array_merge( $components, $config['components'] );
        }

        foreach ($components as $name => $instance) {
            $this->setComponent( $name, $instance, false );
        }

        /**
         * Load behaviors
         */
        $behaviors = static::getDefaultBehaviors();
        if (isset( $config['behaviors'] ) && is_array( $config['behaviors'] )) {
            $behaviors = array_merge( $behaviors, $config['behaviors'] );
        }

        foreach ($behaviors as $name => $instance) {
            $this->setBehavior( $name, $instance, false );
        }

        $this->events->trigger( self::EVENT_BOOTSTRAP );

        return $this;
    }

    public function get($name, $config = [])
    {
        if ($this->instanceManager->has( $name )) {
            return $this->instanceManager->get( $name, $config );
        }

        return null;
    }

    public function getBehavior($name)
    {
        return $this->get( 'behavior::'.$name );
    }

    public function getComponent($name, $config = [])
    {
        return $this->get( 'component::'.$name, $config );
    }

    /**
     * @return InstanceManager|NULL
     */
    public function getInstanceManager()
    {
        return $this->instanceManager;
    }

    /**
     * @param      $status
     * @param mixed $message
     *
     * @throws EObjectError
     */
    public function halt($status, $message = null)
    {
        $this->events->trigger( self::EVENT_HALTED, null, ['status' => $status] );
        if (!empty( $message )) {
            $status = $message;
        }
        exit( $status );
    }

    /**
     * Has a object
     *
     * @param $name
     *
     * @return bool
     * @internal
     */
    public function has($name)
    {
        return $this->instanceManager->has( $name );
    }

    /**
     * Has a object listener
     *
     * @param $name
     *
     * @return bool
     */
    public function hasBehavior($name)
    {
        return $this->has( 'behavior::'.$name );
    }

    /**
     * Alias for get()
     *
     * @param $name
     *
     * @return bool
     */
    public function hasComponent($name)
    {
        return $this->has( 'component::'.$name );
    }

    /**
     * @throws EHttpException
     * @throws EObjectError
     */
    public function run()
    {
        $this->events->trigger( self::EVENT_START );
        $result = $this->events->trigger( self::EVENT_ROUTE_MATCH );
        if (($result->forward() instanceof RouteMatch) == false) {
            throw new EHttpException( 404, sprintf( 'Requested URL "%s" is not found on server', $this->uri->getUri() ) );
        }
        $this->events->trigger( Application::EVENT_ROUTE_DISPATCH, $this, ['route' => $result->forward()] );
        $this->events->trigger( self::EVENT_FINISH );
    }

    /**
     * @param            $name
     * @param            $instance
     * @param bool|FALSE $override
     *
     * @return bool|null
     * @throws EComponentError
     * @internal
     */
    public function set($name, $instance, $override = false)
    {
        return $this->instanceManager->set( $name, $instance, $override, true );
    }

    /**
     * @param      $name
     * @param      $instance
     * @param bool $override
     *
     * @return bool|null
     * @throws EComponentError
     */
    public function setBehavior($name, $instance, $override = false)
    {
        return $this->set( 'behavior::'.$name, $instance, $override );
    }

    /**
     * @param      $name
     * @param      $instance
     * @param bool $override
     *
     * @return bool|null
     * @throws EComponentError
     */
    public function setComponent($name, $instance, $override = false)
    {
        return $this->set( 'component::'.$name, $instance, $override );
    }
}
/* End of file Application.php */