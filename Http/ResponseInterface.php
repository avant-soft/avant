<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2022 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */


namespace Avant\Http;


/**
 * Interface ResponseInterface
 * @subpackage Avant\Http
 */
interface ResponseInterface
{
    public function getHeaders(): array;

    public function getOutput();

    public function send($output = '');

    public function setHeader($header, $replace = true);

    public function setHeaders($headers, $replace = true);

    public function setOutput($output);
}