<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Controller;

use Avant\Http\Application;


/**
 * Trait ControllerTrait
 * @subpackage Avant\Http\Controller
 */
trait ControllerTrait
{
    /**
     * @return ControllerBasic|null
     */
    public function getController()
    {
        static $result;
        if (!$result instanceof ControllerBasic) {
            $result = Application::getInstance()->getComponent( 'controller' );
        }

        return $result;
    }
}