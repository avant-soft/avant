<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Controller;

use Avant\Http\ApplicationProxy;
use Avant\Http\Module;
use Avant\Loader\ClassAsset;

/**
 * Class ControllerBasic
 * @subpackage Avant\Http\Controller
 */
class ControllerBasic extends ApplicationProxy implements ControllerInterface
{
    const EVENT_BEFORE_ACTION    = 'controller:BeforeAction';
    const EVENT_ACTION           = 'controller:Action';
    const EVENT_BEFORE_CONSTRUCT = 'controller:BeforeConstructor';
    const EVENT_CONSTRUCT        = 'controller:Constructor';

    /**
     * @var Module
     */
    protected $module;

    /*
    public function __remap(){}
    public function __module($module = NULL){}
    */

    public function getModule()
    {
        if ($this->module == null) {
            if (method_exists( $this, '__module' )) {
                $this->setModule( call_user_func( [$this, '__module'] ) );
            } else {
                if ($module_name = ClassAsset::getVendorName( get_class( $this ) )) {
                    if ($module = $this->modules->getModule( $module_name )) {
                        $this->setModule( $module );
                    }
                }
            };
        }

        return $this->module;
    }

    public function setModule(Module $module)
    {
        $this->module = $module;

        return $this;
    }
}

/* End of file ControllerBasic.php */
