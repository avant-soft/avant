<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2019 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Controller;

use Avant\Stdlib\GetOpts;

/**
 * Class ConsoleController
 * @package  Avant\Http\Controller
 */
class ConsoleController extends ControllerBasic
{
    /**
     * @var GetOpts
     */
    protected $options;

    public function __init()
    {
        parent::__init();

        if ($this->request->isCliRequest()) {
            /** Modify env $_SERVER for valid working of Stdlib/Url:: */
            $_SERVER['HTTP_HOST']       = $this->config->getItem( 'host', 'console', 'localhost' );
            $_SERVER['HTTP_REFERER']    = $this->config->getItem( 'referer', 'console', '' );
            $_SERVER['HTTP_USER_AGENT'] = $this->config->getItem( 'user_agent', 'console', 'Mozilla/4.5 [en] (X11; U; Linux 2.2.9 i586).' );
            $_SERVER['HTTPS']           = $this->config->getItem( 'https', 'console', '' );
            $_SERVER['SERVER_PORT']     = $this->config->getItem( 'port', 'console', 80 );
        }
    }

    /**
     * @return GetOpts
     */
    public function getOptions()
    {
        if ($this->options === null) {
            $this->options = new GetOpts();
        }

        return $this->options;
    }

    /**
     * Show text message
     *
     * @param string $message
     */
    protected function console($message)
    {
        if ($this->request->isCliRequest()) {
            $console = @fopen( "php://stdout", "w" );
            @fwrite( $console, date( 'Y-m-d H:i:s' ).": ".$message."\n" );
            @fclose( $console );
        }
    }

}