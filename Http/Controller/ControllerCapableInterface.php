<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Controller;


/**
 * Interface ControllerCapableInterface
 * @subpackage Avant\Http\Controller
 */
interface ControllerCapableInterface
{
    public function getController();

    public function setController($controller);
}