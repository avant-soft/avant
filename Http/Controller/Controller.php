<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Controller;

use Avant\Http\View\Plugin\PluginManager;
use Avant\Http\View\Template;
use Avant\Http\View\ViewManager;
use Avant\Stdlib\Common;


/**
 * Class Controller
 *
 * @subpackage Avant\Http
 */
class Controller extends ControllerBasic
{
    /**
     * @var ViewManager
     */
    protected $views;
    protected $views_alias;

    public function __init()
    {
        parent::__init();

        /**
         * Load custom view-plugins
         */
        $this->events->bind( PluginManager::EVENT_FETCH_PLUGINS, function () {
            return $this->config->getItem( 'plugins', null, [] );
        }
        );
    }

    public function getViewManager()
    {
        if ($this->views == null) {
            $this->setViewManager( new ViewManager() );
        }

        return $this->views;
    }

    /**
     * @return mixed
     */
    public function getViewsAlias()
    {
        return $this->views_alias;
    }

    /**
     * @param            $view
     * @param mixed       $variables
     * @param bool|FALSE $partial
     * @param bool|FALSE $return
     *
     * @return string|null
     */
    public function render($view, $variables = null, $partial = false, $return = false)
    {
        $result = null;

        if ($view instanceof Template) {
            // view is template class
            $result = $this->getViewManager()->render( $view );
        } elseif (is_string( $view )) {
            // view is string
            $template = new Template( $view, $variables );
            $template->setPartial( $partial );
            $result = $this->getViewManager()->render( $template );
        }

        if ($return != true && $result != null) {
            $this->response->appendOutput( $result );
        }

        return $result;
    }

    /**
     * @param            $view
     * @param mixed       $variables
     * @param bool|FALSE $return
     *
     * @return string|null
     */
    public function renderPartial($view, $variables = null, $return = false)
    {
        return $this->render( $view, $variables, true, $return );
    }

    public function renderStr($str, $variables)
    {
        $template = new Template( false, $variables );
        $template->setContent( $str );
        $template->setPartial( true );

        return $this->getViewManager()->render( $template );
    }

    public function setViewManager(ViewManager $views)
    {
        if ($views instanceof ViewManager) {

            // configure
            if ($config = $this->config->getItem( 'view' )) {
                $views->initialize( $config );
            }

            // get paths from resolver
            $paths = array_unique( array_merge( $views->getLocations(), $this->resolver->getPaths() ) );
            foreach ($paths as $path) {
                $alias = is_null( $this->views_alias ) ? Common::className( $this, true ) : $this->views_alias;
                foreach (['/views/'.$alias, '/views/'] as $location) {
                    $views->setLocation( $path.$location, false );
                }
            }

            $this->views = $views;
        }
    }

    /**
     * @param mixed $views_alias
     */
    public function setViewsAlias($views_alias)
    {
        $this->views_alias = $views_alias;
    }
}

/* End of file Controller.php */
