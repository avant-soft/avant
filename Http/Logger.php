<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Config\IOFactory;
use Avant\Event\Event;
use Avant\Event\EventTrait;
use Avant\Log\LogFile;


/**
 * Class Logger
 * @subpackage Avant\Http
 * @deprecated deprecated since version 1.8.6
 */
class Logger extends LogFile
{
    use EventTrait;

    public function __construct($config = null)
    {
        parent::__construct( $config );

        if (!is_dir( $this->getPath() )) {
            $this->setPath( (defined( 'APP_PATH' ) ? constant('APP_PATH') : '').'/log' );
        }

        // Bind event for run
        $this->bind( Application::EVENT_START, function () {
            /** @var \Avant\Stdlib\Resolver $resolver */
            if ($resolver = Application::getInstance()->getComponent( 'resolver' )) {
                if ($file = $resolver->findFile( 'logger', 'config' )) {
                    $this->initialize( IOFactory::fromFile( $file, 'config' ) );
                }
            }
        }, 0
        );

        // Bind event for halt, critical when $this->isWriteDirectly() === FALSE
        $this->bind( Application::EVENT_HALTED, function (Event $event) {
            $status = $event->getParam( 'status', 0 );
            if (is_string( $status )) {
                $status = strip_tags( $status );
            }
            $this->log( 'Application halted, exit status: '.$status, self::LOG_INFO );
            $this->write();
        }
        );
    }

    protected function prepSeverity($value)
    {
        switch ($value) {
            case self::LOG_INFO:
                return 'INFO';
                break;
            case self::LOG_DEBUG:
                return 'DEBUG';
                break;
            case self::LOG_WARNING:
                return 'WARNING';
                break;
            case self::LOG_ERROR:
                return 'ERROR';
                break;
            default:
                return $value;
                break;
        }
    }
}

/* End of file Logger.php */
