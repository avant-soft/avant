<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;


/**
 * Trait ApplicationTrait
 * @subpackage Avant\Http
 */
trait ApplicationTrait
{
    /**
     * @return Application|null
     */
    public function getApplication()
    {
        static $result;
        if (!$result instanceof Application) {
            $result = Application::getInstance();
        }

        return $result;
    }
}