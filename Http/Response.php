<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Cache\Cache;
use Avant\Config\IOFactory;
use Avant\Event\Event;

/**
 * Class Response
 * @subpackage Avant\Http
 */
class Response extends ApplicationProxy implements ResponseInterface
{
    const EVENT_CACHE_LOAD = 'response:cacheLoad';
    const EVENT_CACHE_SAVE = 'response:cacheSave';
    const EVENT_BEFORE_SEND = 'response:BeforeSend';
    const EVENT_SEND = 'response:Send';
    const EVENT_OUTPUT_SET = 'response:setOutput';
    const EVENT_OUTPUT_APPEND = 'response:appendOutput';
    const EVENT_HEADERS_SET = 'response:setHeaders';

    /**
     * @var Cache
     */
    protected $cache;
    /**
     * @var string
     */
    protected $cacheID;
    /**
     * @var bool
     */
    protected $cacheLoaded = false;
    /**
     * List of server headers
     *
     * @var array
     * @access  protected
     */
    protected $headers = [];
    /**
     * List of mime types
     *
     * @var array
     * @access  protected
     */
    protected $mimeTypes = [];
    /**
     * Current output string
     *
     * @var string
     * @access  protected
     */
    protected $output;
    /**
     * Determines if output compression is enabled
     *
     * @var bool
     * @access  protected
     */
    protected $zlib_oc = false;

    public function __init()
    {
        $this->events->bind(Application::EVENT_START, function ()
        {
            // configure
            $this->zlib_oc = @ini_get('zlib.output_compression');

            if ($file = $this->resolver->findFile('mimes', 'config'))
            {
                $this->mimeTypes = IOFactory::fromFile($file, 'config');
            }

            // cache
            $this->cacheID = md5($this->uri->getBaseUrl() . $this->uri->getUri() . (($query_string = $this->request->fetchGet()) ? http_build_query($query_string) : ''));
            $this->cache   = $this->setComponent('cache', new Cache($this->config->getItem('cache', null, [])), true);

            // cache enabled only for GET method
            if ($this->cache->isEnabled() && ($this->request->method() != 'GET'))
            {
                $this->cache->setEnabled(false);
            }

            if ($this->cache->isEnabled())
            {
                if ($cached = $this->cache->get($this->cacheID))
                {
                    $this->events->trigger(self::EVENT_CACHE_LOAD, $this, [ 'output' => $cached ]);
                    $this->events->trigger(Application::EVENT_FINISH);
                }
            }

            // buffering
            ob_start();
        }, 0
        );

        $this->events->bind(Application::EVENT_FINISH, function (Event $event)
        {
            return $this->sendFinal($event);
        }, 9990
        );

        $this->events->bind(self::EVENT_CACHE_LOAD, function (Event $event)
        {
            if ($output = $event->getParam('output', null))
            {
                $this->setOutput($output);
                $this->cacheLoaded = true;
            }
        }, 9999
        );
    }

    /**
     * Appends data onto the output string
     * @access  public
     * @param string
     * @return  void|mixed
     */
    public function appendOutput($output)
    {
        if ($this->output == '')
        {
            $this->output = $output;
        }
        else
        {
            $this->output .= $output;
        }
        $this->events->trigger(self::EVENT_OUTPUT_APPEND);

        return $this;
    }

    /**
     * @return string
     */
    public function getCacheID()
    {
        return $this->cacheID;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Get output
     * @return string
     */
    public function getOutput()
    {
        if (($output = ob_get_contents()) && empty($this->output))
        {
            $this->output = $output;
            @ob_end_clean();
        }

        return $this->output;
    }

    /**
     * @return boolean
     */
    public function isCacheLoaded()
    {
        return $this->cacheLoaded;
    }

    public function send($output = '')
    {
        if (!empty($output))
        {
            $this->setOutput($output);
        }

        if ($this->config->getItem('compress_output', null, false) && $this->zlib_oc == false)
        {
            if (extension_loaded('zlib'))
            {
                if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) and strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)
                {
                    ob_start('ob_gzhandler');
                }
            }
        }

        $this->events->trigger(Application::EVENT_FINISH);
    }

    /**
     * Set Content Type Header
     * @param $mimeType
     * @return $this
     * @throws \Avant\Exception\EObjectError
     */
    public function setContentType($mimeType)
    {
        if (strpos($mimeType, '/') === false)
        {
            $extension = ltrim($mimeType, '.');

            // Is this extension supported?
            if (isset($this->mimeTypes[$extension]))
            {
                $mimeType = &$this->mimeTypes[$extension];

                if (is_array($mimeType))
                {
                    $mimeType = current($mimeType);
                }
            }
        }

        $this->headers[] = [ 'Content-Type: ' . $mimeType, true ];
        $this->events->trigger(self::EVENT_HEADERS_SET);

        return $this;
    }

    /**
     * @param      $header
     * @param bool $replace
     */
    public function setHeader($header, $replace = true)
    {
        // If zlib.output_compression is enabled it will compress the output,
        // but it will not modify the content-length header to compensate for
        // the reduction, causing the browser to hang waiting for more data.
        // We'll just skip content-length in those cases.
        if ($this->zlib_oc && strncasecmp($header, 'content-length', 14) == 0)
        {
            return $this;
        }

        $this->headers[] = [ $header, $replace ];
        $this->events->trigger(self::EVENT_HEADERS_SET);

        return $this;
    }

    public function setHeaders($headers, $replace = true)
    {
        if (is_array($headers))
        {
            foreach ($headers as $header)
            {
                $this->setHeader($header, $replace);
            }

            return $this;
        }

        return $this->setHeader($headers, $replace);
    }

    /**
     * Set output
     * @param $output
     * @return $this
     */
    public function setOutput($output)
    {
        $this->output = $output;
        $this->events->trigger(self::EVENT_OUTPUT_SET);

        return $this;
    }

    /**
     * Set HTTP Status Header
     * @param int    $code
     * @param string $text
     * @return $this
     * @throws \Exception
     */
    public function setStatusHeader($code = 200, $text = '')
    {
        \Avant\Stdlib\Common::setStatusHeader($code, $text);

        return $this;
    }

    /**
     * Final output sent to browser
     * @return bool
     */
    protected function sendFinal(Event $event)
    {

        // Last pint for checks & plugins
        $this->events->trigger(self::EVENT_BEFORE_SEND, $this);

        // Cache write
        if ($this->cache->isEnabled() && !$this->isCacheLoaded())
        {
            $this->events->trigger(self::EVENT_CACHE_SAVE, $this);
            $this->cache->save($this->cacheID, $this->getOutput());
        }

        $this->events->trigger(self::EVENT_SEND, $this);

        // Send headers
        if (!headers_sent())
        {
            if (count($this->headers))
            {
                foreach ($this->headers as $header)
                {
                    header($header[0], $header[1]);
                }
            }
        }

        // End output compose
        $event->setParam('output', $this->getOutput());

        return true;
    }
}

/* End of file Response.php */
