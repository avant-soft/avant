<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Sender;

use Avant\Stdlib\Common;


/**
 * Class SenderCli
 * @subpackage Avant\Http\Sender
 */
class SenderOutputCli implements SenderOutputInterface
{
    public function dispatch($output)
    {
        $output = Common::clearHtmlTags( $output );
        $output = trim( $output );

        echo "$output\n";
    }
}

/* End of file SenderCli.php */
