<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Sender;

/**
 * Class SenderOutputInterface
 * @package  Avant
 */
interface SenderOutputInterface
{
    public function dispatch($output);
}

/* End of file SenderOutputInterface.php */
