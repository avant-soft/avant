<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http\Sender;


/**
 * Class SenderAjax
 * @subpackage Avant\Http\Sender
 */
class SenderOutputAjax implements SenderOutputInterface
{
    public function dispatch($output)
    {
        //if ((JsonHelper::isJson($output) == FALSE) && (XmlHelper::isXml($output) == FALSE)) {
        //    $output = json_encode($output, JSON_FORCE_OBJECT | JSON_HEX_QUOT);
        //}
        echo $output;
    }
}

/* End of file SenderAjax.php */
