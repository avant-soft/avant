<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Base\ComponentAbstract;
use Avant\Stdlib\Common;

class Uri extends ComponentAbstract implements UriInterface
{
    protected $host     = 'localhost';
    protected $port     = 80;
    protected $protocol = 'http';
    protected $uri;

    public function __init()
    {
        if (isset( $_SERVER['HTTP_HOST'] )) {
            $this->protocol = isset( $_SERVER['HTTPS'] ) && strtolower( $_SERVER['HTTPS'] ) !== 'off' ? 'https' : 'http';
            $this->host     = $_SERVER['HTTP_HOST'];
            $this->port     = $_SERVER['SERVER_PORT'];
        }

        $this->fetchUriString();
    }

    public function fetchUriString()
    {
        // Is the request coming from the command line?
        if (php_sapi_name() == 'cli' or defined( 'STDIN' )) {
            $this->setUriString( $this->fetchCliArgv() );

            return;
        }

        // Let's try the REQUEST_URI first, this will work in most situations
        if ($uri = $this->fetchUri()) {
            $this->setUriString( $uri );

            return;
        }

        // Is there a PATH_INFO variable?
        // Note: some servers seem to have trouble with getenv() so we'll test it two ways
        $path = (isset( $_SERVER['PATH_INFO'] )) ? $_SERVER['PATH_INFO'] : @getenv( 'PATH_INFO' );
        if (trim( $path, '/' ) != '' && $path != "/".BASE_PATH) {
            $this->setUriString( $path );

            return;
        }

        // No PATH_INFO?... What about QUERY_STRING?
        $path = (isset( $_SERVER['QUERY_STRING'] )) ? $_SERVER['QUERY_STRING'] : @getenv( 'QUERY_STRING' );
        if (trim( $path, '/' ) != '') {
            $this->setUriString( $path );

            return;
        }

        // As a last ditch effort lets try using the $_GET array
        if (is_array( $_GET ) && count( $_GET ) == 1 && trim( key( $_GET ), '/' ) != '') {
            $this->setUriString( key( $_GET ) );

            return;
        }

        // We've exhausted all our options...
        $this->uri = '';

        return;
    }

    public function getBaseUrl()
    {
        return $this->protocol.'://'.$this->host.($this->port != 80 ? ':'.$this->port : '').'/';
    }

    /**
     * @return mixed
     */
    public function getHostname()
    {
        return $this->host;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @return mixed
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setUriString($str)
    {
        // Filter out control characters
        $str = Common::clearInvisibleChars( '/'.ltrim( $str, '/' ), false );

        // If the URI contains only a slash we'll kill it
        $this->uri = ($str == '/') ? '' : urldecode( $str );
    }

    private function fetchCliArgv()
    {
        $uri = array_slice( $_SERVER['argv'], 1 );
        $uri = $uri ? '/'.implode( '/', $uri ) : '';

        if (strncmp( $uri, '?/', 2 ) === 0) {
            $uri = substr( $uri, 2 );
        }
        $parts = preg_split( '#\?#i', $uri, 2 );
        $uri   = $parts[0];
        if (isset( $parts[1] )) {
            parse_str( $parts[1], $_GET );
        }

        return $uri;
    }

    private function fetchUri()
    {
        if (!isset( $_SERVER['REQUEST_URI'] ) or !isset( $_SERVER['SCRIPT_NAME'] )) {
            return '';
        }

        $uri = $_SERVER['REQUEST_URI'];
        if (strpos( $uri, $_SERVER['SCRIPT_NAME'] ) === 0) {
            $uri = substr( $uri, strlen( $_SERVER['SCRIPT_NAME'] ) );
        } elseif (strpos( $uri, dirname( $_SERVER['SCRIPT_NAME'] ) ) === 0) {
            $uri = substr( $uri, strlen( dirname( $_SERVER['SCRIPT_NAME'] ) ) );
        }

        // This section ensures that even on servers that require the URI to be in the query string (Nginx) a correct
        // URI is found, and also fixes the QUERY_STRING server var and $_GET array.
        if (strncmp( $uri, '?/', 2 ) === 0) {
            $uri = substr( $uri, 2 );
        }
        $parts = preg_split( '#\?#i', $uri, 2 );
        $uri   = $parts[0];
        if (isset( $parts[1] )) {
            $_SERVER['QUERY_STRING'] = $parts[1];
            parse_str( $_SERVER['QUERY_STRING'], $_GET );
        } else {
            $_SERVER['QUERY_STRING'] = '';
            $_GET                    = [];
        }

        if ($uri == '/' || empty( $uri )) {
            return '/';
        }

        $uri = parse_url( $uri, PHP_URL_PATH );

        // Do some final cleaning of the URI and return it
        return str_replace( ['//', '../'], '/', trim( $uri, '/' ) );
    }
}