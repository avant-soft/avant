<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Base\ComponentAbstract;
use Avant\Config\Config;
use Avant\Event\EventManager;
use Avant\Exception\ExceptionHandler;
use Avant\Http\Sender\SenderOutputInterface;
use Avant\Stdlib\Resolver;


/**
 * Class ApplicationAccess
 * @subpackage Avant\Http
 *
 * @method bool has($name)
 * @method bool hasBehavior($name)
 * @method bool hasComponent($name)
 * @method Config getConfig()
 * @method EventManager getEvents()
 * @method ExceptionHandler getErrors()
 * @method mixed get($name, $config = [])
 * @method mixed getBehavior($name)
 * @method mixed getComponent($name, $config = [])
 * @method mixed set($name, $instance, $override = false)
 * @method mixed setBehavior($name, $instance, $override = false)
 * @method mixed setComponent($name, $instance, $override = false)
 * @method ModuleDispatcher getModules()
 * @method null halt($status, $message = null)
 * @method Request getRequest()
 * @method Resolver getResolver()
 * @method Response getResponse()
 * @method Router getRouter()
 * @method SenderOutputInterface getSender()
 * @method Uri getUri()
 * @property Config                $config
 * @property EventManager          $events
 * @property ModuleDispatcher      $modules
 * @property Request               $request
 * @property Response              $response
 * @property Router                $router
 * @property Uri                   $uri
 * @property Resolver              $resolver
 * @property ExceptionHandler      $errors
 * @property SenderOutputInterface $sender
 */
class ApplicationProxy extends ComponentAbstract implements ApplicationProxyInterface
{
    /** @var Application */
    protected $application;
    protected string $logID = 'system';

    public function __call($name, $args)
    {
        return call_user_func_array([$this->getApplication(), $name], $args);
    }

    public function __get($name)
    {
        $result = null;

        if ($name == 'application') {
            return $this->getApplication();
        }

        try {
            $result = parent::__get($name);
        } catch (\Exception $exception) {
            $result = $this->getApplication()->__get($name);
        }

        return $result;
    }

    public function getApplication()
    {
        if (is_null($this->application)) {
            $this->application = Application::getInstance();
        }

        return $this->application;
    }

    public function setApplication(Application $application)
    {
        $this->application = $application;
    }

    public function getLogID(): string
    {
        return $this->logID;
    }

    public function setLogID(string $logID): self
    {
        $this->logID = $logID;
        return $this;
    }
}

/* End of file ApplicationAccess.php */
