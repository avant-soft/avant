<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Avant\Helpers\FileHelper;
use Avant\Stdlib\Collection;


/**
 * Class Router
 *
 * @subpackage Avant\Http
 *
 * @property \Avant\Http\Request $request
 */
class Router extends ApplicationProxy
{
    protected $routes       = [];
    protected $routes_remap = [];

    public function __init()
    {
        parent::__init();
        $this->routes = new Collection();
    }

    public function fetchRoutes()
    {
        $this->events->trigger( Module::EVENT_ROUTES_REMAP, $this, [], [$this, 'setRoutesRemap'] );
        $this->events->trigger( Module::EVENT_ROUTES, $this, [], [$this, 'setRoutes'] );
    }

    /**
     * @param $routeName
     *
     * @return mixed
     */
    public function getRoute($routeName)
    {
        if ($this->routes->exists( $routeName )) {
            return $this->routes[$routeName];
        }

        return null;
    }

    /**
     * @return array|Collection
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param       $routeName
     * @param array $params
     *
     * @return mixed
     */
    public function getUrl($routeName, array $params)
    {
        if ($route = $this->getRoute( $routeName )) {
            return $route->getUrl( $params );
        }

        return null;
    }

    public function getUrlRemap($url)
    {
        $result = $url;
        foreach ($this->routes_remap as $pattern => $replacement) {
            $replacement = str_replace( '\\', '/', $replacement );
            if (preg_match( "~$pattern~i", $url )) {
                $result = preg_replace( "~$pattern~i", $replacement, $url );
                break;
            }
        }

        if (is_null( $result ) && $url) {
            $result = $url;
        }

        return $result;
    }

    /**
     * @param        $requestUrl
     * @param string $requestMethod
     * @param mixed   $requestType
     *
     * @return RouteMatch|bool
     */
    public function match($requestUrl, $requestMethod = 'GET', $requestType = null)
    {
        if ($requestUrl == '/' || empty( $requestUrl )) {
            $requestUrl = 'default';
        }


        if ($requestType === null) {
            $requestType = $this->request->type();
        }

        $matched = false;

        // Remap
        $requestUrl = $this->getUrlRemap( $requestUrl );


        // Filter url suffix
        if ($suffix = $this->config->getItem( 'url_suffix' )) {
            $requestUrl = preg_replace( "~".preg_quote( $suffix )."$~i", "", $requestUrl );
        }

        // Search(static routing)
        foreach ($this->routes as $route) {

            // Requested method not supported
            if (!in_array( $requestMethod, (array)$route->getMethods() )) {
                continue;
            }

            // Requested type not supported
            if (($route->getTypes() & $requestType) != $requestType) {
                continue;
            }

            if (preg_match( $route->getPattern(), $requestUrl, $params )) {
                array_shift( $params );

                // Parse $params
                if (is_array( $callback = $route->getCallback() )) {
                    if (isset( $callback['controller'], $callback['method'] )) {
                        foreach ($params as $key => $param) {
                            $pattern = "/{:$key}/";
                            if (preg_match( $pattern, $callback['method'] )) {
                                $callback['method'] = preg_replace( $pattern, $param, $callback['method'] );
                                unset( $params[$key] );
                            }
                        }
                        $route->setCallback( $callback );
                    }
                }

                // Parse args
                $args = [];
                foreach (array_values( $params ) as $param) {
                    $arg  = explode( '/', $param );
                    $args = array_merge( $args, $arg );
                }

                $matched = new RouteMatch( ['name' => $route->getName(), 'callback' => $route->getCallback(), 'args' => $args] );
                break;
            }
        }

        // Search(dynamic routing)
        if ($matched === false && $this->config->getItem( 'allow_dynamic_routing',null,true )) {

            $segments = explode( '/', trim( $requestUrl, '/' ) );
            if (count( $segments ) == 1) {
                $segments[] = 'index';
            }

            array_walk( $segments, function (&$item, $key) {
                $item = ucfirst( $item );
            }
            );


            /** @var Module $module */
            foreach ($this->modules->getModules() as $module) {
                $parts    = $segments;
                $args     = [];
                $callback = [
                  'controller' => '',
                  'method'     => 'actionIndex',
                ];

                // First segment is a module name?
                if (reset( $parts ) == $module->getModuleName()) {
                    array_shift( $parts );
                }

                // First segment is a Controllers folder?
                if (reset( $parts ) == 'Controllers') {
                    array_shift( $parts );
                }

                while (count( $parts )) {
                    $callback['method'] = 'action'.($last_part = array_pop( $parts ));
                    $path               = FileHelper::prepFilename( $module->getSourcePath().($filename = '/Controllers/'.join( '/', $parts )) );
                    if (file_exists( $path )) {
                        $callback['controller'] = str_replace( ['/', '//'], '\\', $module->getModuleName().$filename );
                        $matched                = new RouteMatch( ['name' => $requestUrl, 'callback' => $callback, 'args' => $args] );
                        break 2;
                    }
                    array_unshift( $args, $last_part );
                }

                /**
                 * @deprecated
                 * if (count($parts) > 1) {
                 * $callback['method'] = 'action' . ($last_part = array_pop($parts));
                 * }
                 *
                 * $path = FileHelper::prepFilename($module->getSourcePath() . ($filename = '/Controllers/' . join('/', $parts)));
                 * if (file_exists($path)) {
                 * $callback['controller'] = str_replace(['/', '//'], '\\', $module->getModuleName() . $filename);
                 * $matched                = new RouteMatch(['name' => $requestUrl, 'callback' => $callback, 'args' => $segments]);
                 * break;
                 * }
                 */
            }
        }

        return $matched;
    }

    public function setRoute(Route $route)
    {
        if ($routeName = $route->getName()) {
            $this->routes->set( $routeName, $route );
        } else {
            $this->routes->set( $route );
        }
    }

    /**
     * @param            $routes
     * @param bool|FALSE $clear
     */
    public function setRoutes($routes, $clear = false)
    {

        if (!is_array( $routes ) || empty( $routes )) {
            return;
        }

        if ($clear == true) {
            $this->routes = [];
        }

        foreach ($routes as $pattern => $config) {
            $route = new Route( ['pattern' => $pattern] );
            if (!empty( $config )) {
                $route->initialize( $config );
            }
            $this->setRoute( $route );
        }
    }

    public function setRoutesRemap($routes, $clear = false)
    {
        if (!is_array( $routes ) || empty( $routes )) {
            return;
        }

        if ($clear) {
            $this->routes_remap = [];
        }

        foreach ($routes as $pattern => $replacement) {
            $this->routes_remap[$pattern] = $replacement;
        }
    }
}

/* End of file Router.php */
 