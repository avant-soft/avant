<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Http;

use Chronolog\DateTimeStatement;
use Chronolog\LogBookShelf;
use Chronolog\Severity;

/**
 * Trait LoggerTrait
 * @subpackage Avant\Http
 */
trait LoggerTrait
{
    abstract public function getLogID(): string;

    /**
     * Logs a message with the specified severity level.
     *
     * @param string $message The message to be logged.
     * @param int|Severity $severity The severity level of the log message. Defaults to Severity::Debug.
     * @param array $assets Additional assets to be logged along with the message.
     * @param null|DateTimeStatement $datetime The date and time of the log message. Defaults to null.
     * @return bool Returns true if the message was successfully logged, false otherwise.
     */
    public function logMsg(string $message, int|Severity $severity = Severity::Debug, array $assets = [], null|DateTimeStatement $datetime = null): bool
    {
        if (LogBookShelf::has($id = $this->getLogID())) {
            $logger = LogBookShelf::get($id);
            return $logger->log($severity, $message, $assets, $datetime);
        }

        return false;
    }

    public function logEmergency(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Emergency, $assets);
    }

    public function logAlert(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Alert, $assets);
    }

    public function logCritical(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Critical, $assets);
    }

    public function logError(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Error, $assets);
    }

    public function logWarning(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Warning, $assets);
    }

    public function logNotice(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Notice, $assets);
    }

    public function logInfo(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Info, $assets);
    }

    public function logDebug(string $message, array $assets = []): bool
    {
        return $this->logMsg($message, Severity::Debug, $assets);
    }
}
