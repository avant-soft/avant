<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2022 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */


namespace Avant\Http;


/**
 * Interface RequestInterface
 * @subpackage Avant\Http
 */
interface RequestInterface
{
    public function getRequestBody();

    public function getRequestFormat();

    public function isAjaxRequest();

    public function isCliRequest();

    public function isHttpRequest();

    public function method();

    public function url();

    public function userAgent();

    public function userIP();
}