<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2018 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Services;

use Avant\Base\Configurable;
use Avant\Helpers\ArrayHelper;
use Avant\Helpers\TypeHelper;
use Avant\Stdlib\Requests\RequestsAbstract;

/**
 * Class ServiceRequester
 */
abstract class RequesterAbstract extends Configurable
{
    /** @var string Server port */
    protected $port = 80;
    /** @var string  Server name or IP-address */
    protected $server_name = 'localhost';
    /** @var string  Server protocol [http|https] */
    protected $server_protocol = 'http';
    /** @var \Avant\Stdlib\Requests\RequestsAbstract */
    protected $transport;

    public function __construct(array $config = []){
        parent::__construct($config, false, $this);
    }
    
    /**
     * @return string
     */
    public function getPort()
    {
        $result = (int)$this->port;
        if (empty( $result ) || $result < 1) {
            $result = 80;
        }

        return $result;
    }

    public function getRequestUrl($url = '')
    {
        $result = $this->getServerProtocol().'://'.$this->getServerName();

        if ((string)($port = $this->getPort()) != '80') {
            $result .= ':'.$port;
        }

        if (!empty( $url )) {
            $result .= '/'.trim( $url, '/' );
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getServerName()
    {
        $result = $this->server_name;
        if (empty( $result )) {
            $result = 'localhost';
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getServerProtocol()
    {
        $result = strtolower( strtr( $this->server_protocol, ['-' => '', '_' => '', ' ' => '', '\\' => '', '/' => '', ':' => ''] ) );
        if (empty( $result )) {
            $result = 'http';
        }

        return $result;
    }

    /**
     * @return null|\Avant\Stdlib\Requests\RequestsAbstract
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @param string      $url
     * @param string|null $method
     * @param array|null  $headers
     * @param mixed|null  $content
     * @param bool|FALSE  $async
     * @return \Avant\Services\Response
     */
    abstract public function request($url, $method = null, $headers = null, $content = null, $async = false);

    /**
     * @param $value
     * @return $this
     */
    public function setPort($value)
    {
        $this->port = $value;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setServerName($value)
    {
        $this->server_name = $value;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setServerProtocol($value)
    {
        $this->server_protocol = $value;

        return $this;
    }

    /**
     * @param array|\Avant\Stdlib\Requests\RequestsAbstract $transport
     * @return $this
     */
    public function setTransport($transport)
    {
        if (is_array( $transport )) {
            $className = ArrayHelper::element( 'class', $transport, '\Avant\Stdlib\Requests\CurlRequests', TypeHelper::TYPE_STRING );
            $this->setTransport( new $className( ArrayHelper::element( 'config', $transport, array_diff_key( $transport, array_fill_keys( ['class', 'config'], 'empty' ) ) ) ) );
        } elseif ($transport instanceof RequestsAbstract) {
            $this->transport = $transport;
        }

        return $this;
    }
}