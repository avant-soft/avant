<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2018 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Services;

use Avant\Exception\ETypeError;

/**
 * Class Requester
 */
class Requester extends RequesterAbstract
{
    /**
     * @param string      $url
     * @param string|null $method
     * @param array|null  $headers
     * @param mixed|null  $content
     * @param bool|FALSE  $async
     * @return \Avant\Services\Response
     * @throws ETypeError
     */
    public function request($url, $method = null, $headers = null, $content = null, $async = false)
    {
        if ($transport = $this->getTransport()) {
            $transport->setBlocking( !$async );

            return $transport->request( $url, $method, $headers, $content, new Response() );
        }
        throw new ETypeError( sprintf( '%s: expected a RequestsAbstract instance; received "%s"', __METHOD__, (is_object( $transport ) ? get_class( $transport ) : gettype( $transport )) ) );
    }
}