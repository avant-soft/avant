<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2018 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Services;

use Avant\Helpers\JsonHelper;
use Avant\Helpers\TypeHelper;

/**
 * Class ServiceResponse
 */
class Response extends \Avant\Stdlib\Requests\Response
{
    public function getContents()
    {
        if (JsonHelper::isJson( $this->contents )) {
            $this->contents = json_decode( $this->contents, true );
        }

        return parent::getContents();
    }

    public function getContentsItem($key, $default = null, $expected_type = null)
    {
        $result = $default;
        if (is_array( $array = $this->getContents() ) && array_key_exists( $key, $array )) {
            if ($expected_type !== null) {
                if (TypeHelper::isType( $array[$key], $expected_type )) {
                    $result = $array[$key];
                }
            } else {
                $result = $array[$key];
            }
        }

        return $result;
    }
}