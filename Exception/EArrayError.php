<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class EArrayError
 * @subpackage Avant\Exception
 */
class EArrayError extends ETypeError
{

}

/* End of file EArrayError.php */
