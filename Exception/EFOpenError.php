<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class EFOpenError
 * Error opening file
 * @subpackage Avant\Exception
 */
class EFOpenError extends EInOutError
{

}

/* End of file EFOpenError.php */
