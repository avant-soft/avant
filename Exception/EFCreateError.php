<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class EFCreateError
 * Error file creation
 * @subpackage Avant\Exception
 */
class EFCreateError extends EInOutError
{

}

/* End of file EFCreateError.php */
