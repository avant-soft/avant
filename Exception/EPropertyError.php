<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class EPropertyError
 * Error when reading/writing property values
 * @subpackage Avant\Exception
 */
class EPropertyError extends EBasicException
{

}

/* End of file EPropertyError.php */
