<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class EInOutError
 * I/O error in the file
 * @subpackage Avant\Exception
 */
class EInOutError extends EBasicException
{

}

/* End of file EInOutError.php */
