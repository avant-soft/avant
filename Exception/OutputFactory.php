<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class ExceptionOutputFactory
 * @subpackage Avant\Exception
 */
class OutputFactory
{
    private static $invokeRenderers = [
      'http' => '\Avant\Exception\OutputRendererHttp',
      'cli'  => '\Avant\Exception\OutputRendererCli',
    ];

    private static $renderers = [];


    public static function getRenderer($name = null)
    {
        if (is_null( $name )) {
            $name = php_sapi_name() == 'cli' || defined( 'STDIN' ) ? 'cli' : 'http';
        }

        $name = self::prepName( $name );

        if (isset( self::$renderers[$name] )) {
            return self::$renderers[$name];
        }

        $renderer = null;
        if (isset( self::$invokeRenderers[$name] )) {
            $class    = self::$invokeRenderers[$name];
            $renderer = self::$renderers[$name] = new $class();
        }

        return $renderer;
    }

    public static function isLoaded($name)
    {
        $name = self::prepName( $name );

        return (isset( self::$renderers[$name] ) === true);
    }

    public static function isRegistered($name)
    {
        $name = self::prepName( $name );

        return (isset( self::$invokeRenderers[$name] ) === true);
    }

    public static function registerRenderer($name, $class)
    {
        $name = self::prepName( $name );

        if (!empty( $name ) && !empty( $class ) && !isset( self::$invokeRenderers[$name] )) {
            $className = $class;
            if ($class instanceof OutputRendererInterface) {
                self::$renderers[$name] = $class;
                $className              = get_class( $class );
            }
            self::$invokeRenderers[$name] = $className;

            return true;
        }

        return false;
    }

    private static function prepName($name)
    {
        return strtolower( strtr( $name, ['-' => '', '_' => '', ' ' => '', '\\' => '', '/' => ''] ) );
    }
}

/* End of file ExceptionOutputFactory.php */
