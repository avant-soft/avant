<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2022 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */


namespace Avant\Exception;


/**
 * Class OutputRenderedLog
 * @subpackage Avant\Exception
 */
class OutputRenderedLog extends OutputRendererAbstract
{
    public function output($exception, $debug)
    {
        error_log( $this->format( $exception, $debug ), 0 );
    }
}

/* End of file OutputRenderedLog.php */
