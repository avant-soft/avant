<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;

use Avant\Http\ApplicationProxy;
use Avant\Http\LoggerTrait;

/**
 * Class ExceptionHandler
 * @subpackage Avant\Exception
 */
class ExceptionHandler extends ApplicationProxy
{
    use LoggerTrait;

    const EVENT_RENDER      = 'exception:Render';
    const EVENT_LOG         = 'exception:Log';

    protected $assertion_throw = true;
    protected $assertion_type  = E_USER_ERROR;
    /**
     * @var bool
     */
    protected $debug = false;
    /**
     * Which type of error convert to exception
     * @var int
     */
    protected $error_types = E_ALL;

    /**
     * @var OutputRendererAbstract
     */
    protected $renderer;
    /**
     * if true ignores @-operator and generates exception
     * if false donesn't generate exceptions for errors slashed with @-operator, but logs it
     * @var bool
     */
    protected $scream            = false;
    private   $assertion_options = [
      ASSERT_ACTIVE     => [
        'state'   => 1,
        'restore' => 0,
      ],
      ASSERT_WARNING    => [
        'state'   => 0,
        'restore' => 0,
      ],
      ASSERT_BAIL       => [
        'state'   => 0,
        'restore' => 0,
      ],

    // This has been removed in PHP8  
    //   ASSERT_QUIET_EVAL => [
    //     'state'   => 0,
    //     'restore' => 0,
    //   ],

      ASSERT_CALLBACK   => [
        'state'   => null,
        'restore' => null,
      ],
    ];
    private   $registerFlag      = false;

    public function __init()
    {
        parent::__init();

        // setup environment
        ini_set( 'error_reporting', $this->error_types );
        ini_set( 'display_errors', 0 );
        ini_set( 'display_startup_errors', $this->debug ? 1 : 0 );
        ini_set( 'html_errors', 0 );
        ini_set( 'docref_root', '' );
        ini_set( 'docref_ext', '' );
        ini_set( 'log_errors', 1 );
        // ini_set( 'log_errors_max_len', 0 );
        ini_set( 'ignore_repeated_errors', 0 );
        ini_set( 'ignore_repeated_source', 0 );
        ini_set( 'report_memleaks', 0 );
        ini_set( 'track_errors', 1 );
        ini_set( 'xmlrpc_errors', 0 );
        ini_set( 'xmlrpc_error_number', 0 );
        ini_set( 'error_prepend_string', '' );
        ini_set( 'error_append_string', '' );
    }

    /**
     * @return int
     */
    public function getAssertionType()
    {
        return $this->assertion_type;
    }

    /**
     * @return int
     */
    public function getErrorTypes()
    {
        return $this->error_types;
    }

    /**
     * @return mixed
     */
    public function getRenderer()
    {
        if ($this->renderer === null) {
            $this->setRenderer( OutputFactory::getRenderer() );
        }

        return $this->renderer;
    }

    /**
     * Convert assertion fail to exception
     *
     * @param string $file    The filename where the exception is thrown
     * @param int    $line    The line where the exception is thrown
     * @param string $message The Exception message to throw
     *
     * @throws \ErrorException
     */
    public function handlerAssertion($file, $line, $message)
    {
        $exception = new EAssertionFailed( $message, 0, $this->assertion_type, $file, $line );
        if (!$this->assertion_throw) {
            $this->logWarning( $exception->getMessage(), ['exception' => $exception] );

            return;
        }
        throw $exception;
    }

    /**
     * Convert error to exception
     *
     * @param int    $severity The severity level of the exception
     * @param string $message  The Exception message to throw
     * @param string $file     The filename where the exception is thrown
     * @param int    $line     The line where the exception is thrown
     *
     * @throws EAbort
     */
    public function handlerError($severity, $message, $file, $line)
    {
        $exception = new EAbort( $message, 0, $severity, $file, $line );

        if (error_reporting() === 0 && !$this->scream) {
            $this->logError( $exception->getMessage(), ['exception' => $exception] );

            return;
        } elseif (!($severity & $this->error_types)) {            
            $this->logError( $exception->getMessage(), ['exception' => $exception] );

            return;
        }

        if ($severity & $this->error_types) {
            throw $exception;
        }
    }

    /**
     * Handles uncaught exceptions
     *
     * @param \Exception $exception
     */
    public function handlerException($exception)
    {
        $this->logCritical( $exception->getMessage(), ['exception' => $exception] );
        $this->render( $exception );
    }

    public function handlerFatal()
    {
        //static $last_err;

        // Check for unhandled errors (fatal shutdown)
        $err = error_get_last();

        // If none, check function args (error handler)
        if ($err === null) {
            $err = func_get_args();
        }

        // Return if no error
        if (empty( $err )) {
            return;
        }

        $err       = array_combine( ['severity', 'message', 'filename', 'line', 'context'], array_pad( $err, 5, null ) );
        $exception = new EAbort( $err['message'], 0, $err['severity'], $err['filename'], $err['line'] );

        if ($err['severity'] & $this->error_types) {
            $this->render( $exception );
            exit;
        }
    }

    /**
     * @return bool
     */
    public function isAssertionThrow()
    {
        return $this->assertion_throw;
    }

    /**
     * @return boolean
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @return bool
     */
    public function isScream()
    {
        return $this->scream;
    }

    public function register($outputRenderer = null, $errorTypes = null)
    {

        // setup handler
        if (is_null( $errorTypes )) {
            $errorTypes = E_ALL | E_STRICT;
        }

        if (is_null( $outputRenderer ) || is_string( $outputRenderer )) {
            $outputRenderer = OutputFactory::getRenderer( $outputRenderer );
        }

        $this->setRenderer( $outputRenderer );

        if (!$this->registerFlag) {
            set_error_handler( [$this, 'handlerError'], $errorTypes );
            set_exception_handler( [$this, 'handlerException'] );
            register_shutdown_function( [$this, 'handlerFatal'] );

            // save states assertions
            // unsupported in 8.3
            // foreach ($this->assertion_options as $key => &$assertion) {
            //     $assertion['restore'] = assert_options( $key );
            //     if (is_null( $assertion['state'] )) {
            //         $assertion['state'] = [$this, 'handlerAssertion'];
            //     }
            //     assert_options( $key, $assertion['state'] );
            // }

            $this->registerFlag = true;
        }
    }

    public function render($exception)
    {
        $this->events->trigger( self::EVENT_RENDER, $this, ['exception' => &$exception] );
        $this->getRenderer()->output( $exception, $this->debug );
    }

    public function restore()
    {
        if ($this->registerFlag) {
            restore_error_handler();
            restore_exception_handler();
            // unsupported in 8.3
            // foreach ($this->assertion_options as $key => $assertion) {
            //     assert_options( $key, $assertion['restore'] );
            // }
        }
    }

    /**
     * @param bool $assertion_throw
     */
    public function setAssertionThrow($assertion_throw)
    {
        $this->assertion_throw = $assertion_throw;
    }

    /**
     * @param int $assertion_type
     */
    public function setAssertionType($assertion_type)
    {
        $this->assertion_type = $assertion_type;
    }

    /**
     * @param boolean $debug
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
    }

    /**
     * @param int $error_types
     */
    public function setErrorTypes($error_types)
    {
        $this->error_types = $error_types;
    }

    /**
     * @param mixed $renderer
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;
        $this->renderer->setExceptionHandler( $this );
    }

    /**
     * @param bool $scream
     */
    public function setScream($scream)
    {
        $this->scream = $scream;
    }
}

/* End of file ExceptionHandler.php */
