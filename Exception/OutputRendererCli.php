<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class OutputRendererCli
 * @subpackage Avant\Exception
 */
class OutputRendererCli extends OutputRendererAbstract
{
    protected $fileLinkFormat = ': in %f on line %l';

    public function __construct($fileLinkFormat = null)
    {
        parent::__construct();
        if (!is_null( $fileLinkFormat )) {
            $this->setFileLinkFormat( $fileLinkFormat );
        }
    }

    public function output($exception, $debug)
    {
        exit( $this->format( $exception, $debug ) );
    }

    /**
     * This setting determines the format of the filepath that are made in the
     * display of stack traces where file names are used.
     * ': in %f on line %l' (Netbeans)<br>
     * The possible format specifiers are:<br>
     * %f - the filename<br>
     * %l - the line number<br>
     *
     * @param string $fileLinkFormat
     */
    public function setFileLinkFormat($fileLinkFormat)
    {
        $this->fileLinkFormat = $fileLinkFormat;
    }

    /**
     * @param string $file
     * @param int    $line
     * @return string
     */
    protected function getFileLink($file, $line)
    {
        if (is_null( $file ) || !strlen( $this->fileLinkFormat )) {
            return parent::getFileLink( $file, $line );
        }
        $fileLink = str_replace( array('%f', '%l'), array($file, $line), $this->fileLinkFormat );

        return '    '.$fileLink;
    }

}

/* End of file OutputRendererCli.php */
