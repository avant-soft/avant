<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


use Avant\Helpers\ArrayHelper;

/**
 * Class OutputRendererAbstract
 * @subpackage Avant\Exception
 */
abstract class OutputRendererAbstract implements OutputRendererInterface
{
    /** @var ExceptionHandler */
    protected $exceptionHandler;
    /**
     * @var int
     */
    protected $maxchars = 40;

    /**
     * @var bool
     */
    protected $utf = false;

    public function __construct()
    {
        if (extension_loaded( 'mbstring' )) {
            $this->utf = (ini_get( 'mbstring.internal_encoding' ) == 'UTF-8');
        }
    }

    /**
     * @param mixed $arg
     * @return string utf-8
     */
    public function argToString($arg)
    {
        switch (gettype( $arg )) {
            case 'boolean':
                $arg = $arg ? 'true' : 'false';
                break;
            case 'NULL':
                $arg = 'null';
                break;
            case 'integer':
            case 'double':
            case 'float':
                $arg = (string)$arg;
                if ($this->utf) {
                    $arg = str_replace( 'INF', '∞', $arg ); //is_infinite($arg)
                }
                break;
            case 'string':
                if (is_callable( $arg, false, $callable_name )) {
                    $arg = 'fs:'.$callable_name;
                } elseif (class_exists( $arg, false )) {
                    $arg = 'c:'.$arg;
                } elseif (interface_exists( $arg, false )) {
                    $arg = 'i:'.$arg;
                } else {
                    $strlen = $this->stringLength( $arg );
                    $arg    = $this->formatString( $arg );
                    if ($strlen <= $this->maxchars) {
                        $arg = '"'.$arg.'"';
                    } else {
                        $arg = '"'.$arg.'"('.$strlen.')';
                    }

                    return $arg = str_replace( "\n", '\n', $arg );
                }
                break;
            case 'array':
                if (is_callable( $arg, false, $callable_name )) {
                    $arg = 'fa:'.$callable_name;
                } else {
                    $arg = 'array('.count( $arg ).')';
                }
                break;
            case 'object':
                $arg = get_class( $arg ).'()';//.':'.spl_object_hash($arg);
                break;
            case 'resource':
                // @see http://php.net/manual/en/resource.php
                $arg = 'r:'.get_resource_type( $arg );
                break;
            default:
                $arg = 'unknown type';
                break;
        }

        return $arg;
    }

    /**
     * @param array $args
     * @return string
     */
    public function argumentsToString($args)
    {
        if (!is_null( $args )) {
            foreach ($args as $iArg => $arg) {
                $args[$iArg] = $this->argToString( $arg );
            }

            return '('.implode( ', ', $args ).')';
        }

        return '()';
    }

    /**
     * @param \Exception $exception
     * @param bool|FALSE $debug
     * @return string
     */
    public function format($exception, $debug = false)
    {
        // fix for 7.2 except on get_class(NULL)
        $exceptionClass = 'Exception';
        if (is_object( $exception )) {
            $exceptionClass = get_class( $exception );
        }

        $result = $exceptionClass." - ".$this->getMessage( $exception )."\n";
        if ($debug == true) {
            foreach ($trace = $this->getTrace( $exception ) as $index => $lines) {
                $line = '#'.$index.': ';

                if (array_key_exists( 'class', $lines )) {
                    $line .= $lines['class'].$lines['type'];
                }

                if (array_key_exists( 'function', $lines )) {
                    $line .= $lines['function'];
                    $line .= $this->argumentsToString( ArrayHelper::element( 'args', $lines, [] ) );
                }

                if (array_key_exists( 'file', $lines )) {
                    $line .= "\n".$this->getFileLink(
                        $lines['file'], $lines['line']
                      );
                } else {
                    $line .= "\n".$this->getFileLink( null, null );
                }

                $result .= $line."\n";
            }
        }

        return strip_tags( $result );
    }

    /**
     * @param string $str
     * @return string
     */
    public function formatString($str)
    {
        if ($this->stringLength( $str ) > $this->maxchars) {
            if ($this->utf) {
                $hellip = '…';
                $str    = trim( mb_substr( $str, 0, $this->maxchars / 2 ) ).$hellip.trim( mb_substr( $str, -$this->maxchars / 2 ) );
            } else {
                $hellip = '...';
                $str    = substr( $str, 0, $this->maxchars / 2 ).$hellip.substr( $str, -$this->maxchars / 2 );
            }
        }

        return $str;
    }

    /**
     * @return ExceptionHandler
     */
    public function getExceptionHandler()
    {
        return $this->exceptionHandler;
    }

    /**
     * @param $exception
     * @return string
     */
    public function getMessage($exception)
    {
        $result = '';
        if ($exception instanceof EBasicException) {
            $result = $this->severityToString( $exception->getSeverity() );
            if (strlen( $exception->getMessage() )) {
                $result .= ' - ';
            }
        }
        $result .= $exception->getMessage();

        return $result;
    }

    /**
     * @param $exception
     * @return array
     */
    public function getTrace($exception)
    {
        $backtrace = $exception->getTrace();
        $count     = count( $backtrace );

        // fix for 7.2 except on get_class(NULL)
        $handlerClass = "Unknown";
        if (is_object( $this->exceptionHandler )) {
            $handlerClass = get_class( $this->exceptionHandler );
        }

        /**
         * bug
         * @see http://www.php.net/manual/en/class.errorexception.php#86985
         */
        if (strpos( phpversion(), '5.2' ) === 0 && $exception instanceof \ErrorException) {
            for ($i = $count - 1; $i > 0; --$i) {
                $backtrace[$i]['args'] = $backtrace[$i - 1]['args'];
            }
            $backtrace[0]['args'] = null;
        }
        $result = array();
        for ($i = 0; $i < $count; $i++) {
            if (array_key_exists( 'class', $backtrace[$i] ) && $backtrace[$i]['class'] == $handlerClass) {
                continue;
            }
            $result[] = $backtrace[$i];
        }

        return $result;
    }

    /**
     * @param \Exception $exception
     * @param bool       $debug
     */
    abstract public function output($exception, $debug);

    /**
     * @param mixed $exceptionHandler
     */
    public function setExceptionHandler($exceptionHandler)
    {
        $this->exceptionHandler = $exceptionHandler;
    }

    /**
     * @param $severity
     * @return string
     */
    public function severityToString($severity)
    {
        switch ($severity) {
            case 1:
                return 'E_ERROR';
                break;
            case 2:
                return 'E_WARNING';
                break;
            case 4:
                return 'E_PARSE';
                break;
            case 8:
                return 'E_NOTICE';
                break;
            case 16:
                return 'E_CORE_ERROR';
                break;
            case 32:
                return 'E_CORE_WARNING';
                break;
            case 64:
                return 'E_COMPILE_ERROR';
                break;
            case 128:
                return 'E_COMPILE_WARNING';
                break;
            case 256:
                return 'E_USER_ERROR';
                break;
            case 512:
                return 'E_USER_WARNING';
                break;
            case 1024:
                return 'E_USER_NOTICE';
                break;
            case 2048:
                return 'E_STRICT';
                break;
            case 4096:
                return 'E_RECOVERABLE_ERROR';
                break;
            case 8192:
                return 'E_DEPRECATED';
                break;
            case 16384:
                return 'E_USER_DEPRECATED';
                break;
            case 30719:
                return 'E_ALL';
                break;
        }

        return 'E_UNDEFINED';
    }

    /**
     * @param string $str
     * @return int
     */
    public function stringLength($str)
    {
        if ($this->utf) {
            $strlen = mb_strlen( $str );
        } else {
            $strlen = strlen( $str );
        }

        return $strlen;
    }

    /**
     * @param string $file
     * @param int    $line
     * @return string
     */
    protected function getFileLink($file, $line)
    {
        if (is_null( $file )) {
            return '    : unknown file';
        }

        return '    '.str_replace( array('%f', '%l'), array($file, $line), ': in %f on line %l' );
    }

}

/* End of file ExceptionOutputRenderer.php */
