<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Exception;


/**
 * Class OutputRendererHtml
 * @subpackage Avant\Exception
 */
class OutputRendererHttp extends OutputRendererAbstract
{
    protected $fileLinkFormat = 'nb://%f:%l';

    public function __construct($fileLinkFormat = null)
    {
        parent::__construct();
        if (!is_null( $fileLinkFormat )) {
            $this->setFileLinkFormat( $fileLinkFormat );
        }
    }

    public function output($exception, $debug)
    {

        if (!headers_sent()) {
            header( 'HTTP/1.0 500 Internal Server Error', true, 500 );
            header( 'Status: 500 Internal Server Error', true, 500 );
        }

        exit( $this->format( $exception, $debug ) );
    }

    /**
     * This setting determines the format of the links that are made in the
     * display of stack traces where file names are used. This allows IDEs to
     * set up a link-protocol that makes it possible to go directly to a line
     * and file by clicking on the filenames that shows in stack traces.
     * An example format might look like:
     * 'txmt://open/?file://%f&line=%l' (TextMate)
     * 'gvim://%f@%l' (gVim - with additional hack)
     * 'nb://%f:%l' (NetBeans - with additional hack)
     * The possible format specifiers are:
     * %f - the filename
     * %l - the line number
     *
     * @see https://bugs.eclipse.org/bugs/show_bug.cgi?id=305345
     * @see http://code.google.com/p/coda-protocol/
     *
     * @param string $fileLinkFormat
     */
    public function setFileLinkFormat($fileLinkFormat)
    {
        $this->fileLinkFormat = $fileLinkFormat;
        ini_set( 'xdebug.file_link_format', $this->fileLinkFormat );
    }

    /**
     * @param string $file
     * @param int    $line
     * @return string
     */
    protected function getFileLink($file, $line)
    {
        if (is_null( $file ) || !strlen( $this->fileLinkFormat )) {
            return parent::getFileLink( $file, $line );
        }

        $fileLink = str_replace( array('%f', '%l'),
          array(urlencode( $file ), $line),
          $this->fileLinkFormat );

        return '    <a href="'.$fileLink.'">'.$this->formatString( parent::getFileLink( $file, $line ) ).'</a>';
    }
}

/* End of file OutputRendererHtml.php */
