<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n;


/**
 * Class LocaleHttp
 * @subpackage Avant\i18n
 */
class LocaleHttp extends LocaleIdentityAbstract
{
    /**
     * @return string - default 'en'
     */
    public function identify()
    {
        if (extension_loaded( 'intl' ) && isset( $_SERVER['HTTP_ACCEPT_LANGUAGE'] )) {
            return locale_accept_from_http( $_SERVER['HTTP_ACCEPT_LANGUAGE'] );
        }

        return 'en';
    }
}

/* End of file LocaleHttp.php */
