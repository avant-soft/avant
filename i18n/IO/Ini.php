<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n\IO;

use Avant\Exception\EInOutError;
use Avant\Exception\ETypeError;


/**
 * Class Ini
 * @subpackage Avant\i18n\IO
 */
class Ini extends TranslationLoaderAbstract
{
    public function load($filename)
    {

        if (!$this->validateFile( $filename )) {
            throw new EInOutError( sprintf(
                'Could not find or open file %s for reading',
                $filename
              )
            );
        }

        $messages = parse_ini_file( $filename, true );

        if (!is_array( $messages )) {
            throw new ETypeError( sprintf(
                'Expected an array, but received %s',
                gettype( $messages )
              )
            );
        }

        return new \ArrayObject( $messages );
    }
}

/* End of file Ini.php */
