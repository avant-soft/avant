<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n\IO;


/**
 * Class TranslationLoaderAbstract
 * @subpackage Avant\i18n\IO
 */
abstract class TranslationLoaderAbstract implements TranslationLoaderInterface
{
    public function validateFile($filename)
    {
        return is_file( $filename ) && is_readable( $filename );
    }
}

/* End of file TranslationLoaderAbstract.php */
