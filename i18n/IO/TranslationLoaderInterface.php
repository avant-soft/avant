<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n\IO;


/**
 * Interface TranslationLoaderInterface
 * @subpackage Avant\i18n\IO
 */
interface TranslationLoaderInterface
{
    /**
     * @param string $filename
     * @return \ArrayObject
     */
    public function load($filename);
}