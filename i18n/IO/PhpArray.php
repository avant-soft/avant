<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n\IO;

use Avant\Exception\EInOutError;
use Avant\Exception\ETypeError;


/**
 * Class PhpArray
 * @subpackage Avant\i18n\IO
 */
class PhpArray extends TranslationLoaderAbstract
{
    public function load($filename)
    {
        if (!$this->validateFile( $filename )) {
            throw new EInOutError( sprintf(
                'Could not find or open file %s for reading',
                $filename
              )
            );
        }

        $messages = include $filename;

        if (!is_array( $messages )) {
            throw new ETypeError( sprintf(
                'Expected an array, but received %s',
                gettype( $messages )
              )
            );
        }

        return new \ArrayObject( $messages );
    }
}

/* End of file PhpArray.php */
