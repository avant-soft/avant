<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n\IO;

use Avant\Exception\EInOutError;


/**
 * Class Gettext
 * @subpackage Avant\i18n\IO
 */
class Gettext extends TranslationLoaderAbstract
{
    public function load($filename)
    {
        if (!$this->validateFile( $filename )) {
            throw new EInOutError( sprintf(
                'Could not find or open file %s for reading',
                $filename
              )
            );
        }

        $messages   = [];
        $matches    = [];
        $matchCount = preg_match_all( '/msgid\s+((?:".*(?<!\\\\)"\s*)+)\s+msgstr\s+((?:".*(?<!\\\\)"\s*)+)/', file_get_contents( $filename ), $matches );
        for ($i = 0; $i < $matchCount; ++$i) {
            $message            = $this->decode( $matches[1][$i] );
            $translation        = $this->decode( $matches[2][$i] );
            $messages[$message] = $translation;
        }

        return new \ArrayObject( $messages );
    }

    protected function decode($string)
    {
        $string = preg_replace(
          ['/"\s+"/', '/\\\\n/', '/\\\\r/', '/\\\\t/', '/\\\\"/'],
          ['', "\n", "\r", "\t", '"'],
          $string
        );

        return substr( rtrim( $string ), 1, -1 );
    }
}

/* End of file Gettext.php */
