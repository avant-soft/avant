<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n;

use Avant\Stdlib\InstanceManager;


/**
 * Class LoaderFactory
 * @subpackage Avant\i18n
 */
class LoaderManager extends InstanceManager
{
    protected $invokableClasses = [
      'gettext'  => '\Avant\i18n\IO\Gettext',
      'ini'      => '\Avant\i18n\IO\Ini',
      'phparray' => '\Avant\i18n\IO\PhpArray',
    ];
}

/* End of file LoaderFactory.php */
