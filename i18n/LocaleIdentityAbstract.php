<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n;

use Avant\Base\Configurable;


/**
 * Class LocaleIdentityAbstract
 * @subpackage Avant\i18n
 */
abstract class LocaleIdentityAbstract extends Configurable implements LocaleIdentityInterface
{
    protected $locale_name;

    public function __construct(array $config = null)
    {
        parent::__construct( $config );

        if ($locale = $this->identify()) {
            $this->locale_name = $locale;
        }
    }

    public function getName()
    {
        return $this->locale_name;
    }

    abstract protected function identify();
}

/* End of file LocaleIdentityAbstract.php */
