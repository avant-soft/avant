<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n;


/**
 * Interface LocaleIdentityInterface
 * @subpackage Avant\i18n
 */
interface LocaleIdentityInterface
{
    public function getName();
}