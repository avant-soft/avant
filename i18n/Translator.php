<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\i18n;

use Avant\Base\ComponentAbstract;
use Avant\Exception\EMatchError;
use Avant\Exception\ERuntimeError;
use Avant\Exception\ETypeError;
use Avant\Http\Application;
use Avant\i18n\IO\TranslationLoaderAbstract;
use Avant\Stdlib\Resolver;


/**
 * Class Translator
 * @subpackage Avant\i18n
 */
class Translator extends ComponentAbstract
{
    /**
     * @var array
     */
    protected $files = [];
    /**
     * @var LoaderManager
     */
    protected $loaderManager;

    /**
     * Current locale(en,en_US etc.)
     * @var string
     */
    protected $locale;
    /**
     * Current folder in module context
     * @var string
     */
    protected $localeBase = 'messages';
    /**
     * Locale identify manager
     * @var LocaleIdentityInterface
     */
    protected $localeIdentity;
    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @var \Avant\Stdlib\Resolver
     */
    protected $resolver;

    /**
     * Adds a translation file to the translator.
     *
     * @param string $type The type of the translation file.
     * @param string $filename The filename of the translation file.
     * @param string $textDomain The text domain of the translation file. Default is 'default'.
     * @return self Returns an instance of the Translator class.
     */
    public function addTranslationFile(string $type, string $filename, string $textDomain = 'default'): self
    {

        if (!isset($this->files[$textDomain])) {
            $this->files[$textDomain] = [];
        }

        $this->files[$textDomain][] = [
            'type'     => $type,
            'filename' => $filename,
        ];

        return $this;
    }

    /**
     * Get the loader manager instance.
     *
     * @return LoaderManager The loader manager instance.
     */
    public function getLoaderManager(): LoaderManager
    {
        if (!$this->loaderManager instanceof LoaderManager) {
            $this->setLoaderManager(new LoaderManager());
        }

        return $this->loaderManager;
    }

    /**
     * Get the current locale of the translator.
     *
     * @return string The current locale.
     */
    public function getLocale(): string
    {
        if ($this->locale === null) {
            $this->locale = $this->getLocaleIdentity()->getName();
        }

        return $this->locale;
    }

    /**
     * Retrieves the base path for the specified locale.
     *
     * @param string|null $locale The locale to retrieve the base path for. If null, the default locale will be used.
     * @return string The base path for the specified locale.
     */
    public function getLocaleBase(?string $locale = null)
    {
        if ($locale === null) {
            $locale = $this->getLocale();
        }

        return $this->localeBase . '/' . $locale;
    }

    /**
     * Retrieves the locale identity.
     *
     * @return LocaleIdentityInterface The locale identity.
     */
    public function getLocaleIdentity(): LocaleIdentityInterface
    {
        if ($this->localeIdentity === null) {
            $this->localeIdentity = new LocaleHttp();
        }

        return $this->localeIdentity;
    }

    /**
     * Get the resolver instance.
     *
     * @return Resolver The resolver instance.
     */
    public function getResolver(): Resolver
    {
        if ($this->resolver == null) {
            if (!($instance = Application::getInstance()->getComponent('resolver')) instanceof Resolver) {
                $instance = new Resolver();
            }
            $this->setResolver($instance);
        }

        return $this->resolver;
    }

    /**
     * Sets the loader manager for the translator.
     *
     * @param LoaderManager $loaderManager The loader manager to set.
     * @return self Returns the Translator instance for method chaining.
     */
    public function setLoaderManager(LoaderManager $loaderManager): self
    {
        $this->loaderManager = $loaderManager;

        return $this;
    }

    /**
     * Sets the locale for the translator.
     *
     * @param string $locale The locale to set.
     * @return void
     */
    public function setLocale(string $locale):void
    {
        $this->locale = $locale;
    }


    /**
     * Sets the locale identity for translation.
     *
     * @param mixed $instance The locale identity to set.
     * @return void
     */
    public function setLocaleIdentity(mixed $instance): void
    {

        if (is_string($instance)) {
            $className = $instance;
            if (!class_exists((string)$className)) {
                throw new EMatchError(sprintf('%s: failed retrieving "%s" via invokable class "%s"; class does not exist', __METHOD__, $className, $instance));
            }
            $instance = new $className();
        } elseif (is_array($instance)) {
            if (!isset($instance['class'])) {
                throw new EMatchError(sprintf('%s: failed retrieving class name of "LocaleIdentity"; class is not defined', __METHOD__));
            }

            $className   = $instance['class'];
            $classConfig = isset($instance['config']) ? $instance['config'] : array_diff_key($instance, array_fill_keys(['class', 'config'], 'empty'));

            if (!class_exists((string)$className)) {
                throw new EMatchError(sprintf('%s: failed retrieving "%s" via config; class does not exist', __METHOD__, $className));
            }

            $instance = new $className($classConfig);
        }

        if (!$instance instanceof LocaleIdentityInterface) {
            throw new ETypeError(
                sprintf(
                    'Expected \Avant\i18n\LocaleIdentityInterface, got type "%s" instead',
                    (is_object($instance) ? get_class($instance) : gettype($instance))
                )
            );
        }

        $this->localeIdentity = $instance;
    }

    /**
     * Sets the resolver instance for the translator.
     *
     * @param Resolver $instance The resolver instance to set.
     * @return void
     */
    public function setResolver(Resolver $instance)
    {
        $this->resolver = $instance;
    }

    /**
     * Translates a message to the specified text domain and locale.
     *
     * @param string      $message    The message to be translated.
     * @param string      $textDomain The text domain to translate the message in. Default is 'default'.
     * @param string|null $locale     The locale to translate the message to. If null, the default locale will be used.
     *
     * @return string The translated message.
     */
    public function translate(string $message, string $textDomain = 'default', ?string $locale = null)
    {
        $locale = ($locale ?: $this->getLocale());
        if (!is_null($translation = $this->getTranslation($message, $textDomain, $locale))) {
            return $translation;
        }

        return $message;
    }

    /**
     * Retrieves the translation for a given message, text domain, and locale.
     *
     * @param string $message The message to be translated.
     * @param string $textDomain The text domain for the translation.
     * @param string $locale The locale for the translation.
     * @return mixed The translated message, or null if no translation is found.
     */
    protected function getTranslation(string $message, string $textDomain, string $locale): mixed
    {
        if (empty($message)) {
            return null;
        }

        // check textDomain
        if (!isset($this->messages[$textDomain][$locale])) {
            if (!$this->loadTranslation($textDomain, $locale)) {
                return null;
            }
        }

        // check & return message
        if (isset($this->messages[$textDomain][$locale][$message])) {
            return $this->messages[$textDomain][$locale][$message];
        } else return null;
    }

    /**
     * Loads the translation for a specific text domain and locale.
     *
     * @param string $textDomain The text domain to load the translation for.
     * @param string $locale The locale to load the translation in.
     * @return bool Returns true if the translation was successfully loaded, false otherwise.
     */
    protected function loadTranslation(string $textDomain, string $locale):bool
    {
        $result = false;

        if (!isset($this->messages[$textDomain])) {
            $this->messages[$textDomain] = [];
        }

        if (isset($this->files[$textDomain])) {
            foreach ($this->files[$textDomain] as $file) {
                /**
                 * @var TranslationLoaderAbstract $loader
                 */
                if (!($loader = $this->getLoaderManager()->get($file['type'])) instanceof TranslationLoaderAbstract) {
                    throw new ERuntimeError('Loader is not a translation file loader');
                }

                if ($filename = $this->getResolver()->findFile($file['filename'], $this->getLocaleBase($locale))) {
                    $messages = $loader->load($filename);
                    if (isset($this->messages[$textDomain][$locale])) {
                        $this->messages[$textDomain][$locale]->exchangeArray(
                            array_replace(
                                $this->messages[$textDomain][$locale]->getArrayCopy(),
                                $messages->getArrayCopy()
                            )
                        );
                    } else {
                        $this->messages[$textDomain][$locale] = $messages;
                    }

                    $result = true;
                }
            }

            reset($this->files[$textDomain]);
        }


        return $result;
    }

    /**
     * Sets the files for translation.
     *
     * @param array $files The array of files to set.
     * @return self The updated Translator instance.
     */
    protected function setFiles(array $files): self
    {
        foreach ($files as $file) {
            if (isset($file['type'], $file['filename'])) {
                $this->addTranslationFile($file['type'], $file['filename'], isset($file['textDomain']) ? $file['textDomain'] : 'default');
            }
        }

        return $this;
    }
}

/* End of file Translator.php */
