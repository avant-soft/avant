<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache;

use Avant\Base\Configurable;
use Avant\Stdlib\InitTrait;

/**
 * Class Cache
 * @subpackage Avant\Cache
 */
class Cache extends Configurable implements IODriverInterface
{
    use InitTrait;

    /** @var  String */
    protected $driver;
    /** @var  IODriverInterface */
    protected $driver_instance;
    /** @var array */
    protected $drivers = [];
    /** @var bool */
    protected $enabled = false;

    public function cacheInfo($type = null)
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            return $driver->cacheInfo($type);
        }

        return null;
    }

    public function clean()
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            $driver->clean();
        }
    }

    public function delete($id)
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            return $driver->delete($id);
        }

        return null;
    }

    public function get($id)
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            return $driver->get($id);
        }

        return null;
    }

    public function getDriver()
    {
        if (is_null($this->driver_instance)) {
            $driver_options = isset($this->drivers[$this->driver]) ? $this->drivers[$this->driver] : [];
            if ($driver = IOFactory::driver($this->driver, $driver_options)) {
                $this->driver_instance = $driver;
            }
        }

        return $this->driver_instance;
    }

    public function getMetadata($id)
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            return $driver->getMetadata($id);
        }

        return null;
    }

    public function isEnabled()
    {
        return $this->enabled === true;
    }

    public function isSupported()
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            return $driver->isSupported();
        }

        return false;
    }

    public function save($id, $data)
    {
        if ($this->isEnabled() && ($driver = $this->getDriver())) {
            return $driver->save($id, $data);
        }

        return null;
    }

    public function setDriver($type)
    {
        if (empty($type) || ($driver = strtolower($type)) == $this->driver) {
            return;
        }
        $this->driver = $driver;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get the value of drivers
     */
    public function getDrivers()
    {
        return $this->drivers;
    }

    /**
     * Set the value of drivers
     *
     * @return  self
     */
    public function setDrivers($drivers)
    {
        $this->drivers = $drivers;

        return $this;
    }

    /**
     * Get the value of enabled
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}

/* End of file Cache.php */
