<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache;


/**
 * Class IOFactory
 * @subpackage Avant\Cache
 */
class IOFactory
{
    // protected static $drivers       = [];
    protected static $invokeClasses = [
      'file'     => 'Avant\Cache\IO\File',
      'apc'      => 'Avant\Cache\IO\Apc',
      'redis'    => 'Avant\Cache\IO\Redis',
      'memcache' => 'Avant\Cache\IO\Memcache',
    ];

    public static function driver($type, $options = [])
    {
        $driver = null;
        // if (isset( self::$drivers[$type] )) {
        //     return self::$drivers[$type];
        // }

        // if (isset( self::$invokeClasses[$type] )) {
        //     $className = self::$invokeClasses[$type];
        //     $driver    = self::$drivers[$type] = new $className( $options );
        // }

        if (isset( self::$invokeClasses[$type] )) {
            $className = self::$invokeClasses[$type];
            $driver    = new $className( $options );
        }
        return $driver;
    }
}

/* End of file IOFactory.php */
