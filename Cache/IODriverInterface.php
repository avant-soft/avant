<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache;


/**
 * Interface IODriverInterface
 * @subpackage Avant\Cache
 */
interface IODriverInterface
{
    public function cacheInfo($type = null);

    public function clean();

    public function delete($id);

    public function get($id);

    public function getMetadata($id);

    public function isSupported();

    public function save($id, $data);
}