<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache\IO;

use Avant\Base\Configurable;
use Avant\Cache\IODriverInterface;
use Avant\Exception\ECacheError;


/**
 * Class Apc
 * @subpackage Avant\Cache\IO
 */
class Apc extends Configurable implements IODriverInterface
{
    protected $lifetime = 60;

    public function __construct($config = [])
    {
        parent::__construct( $config );

        if (!$this->isSupported()) {
            throw new ECacheError( 'APC is not supported' );
        }
    }

    public function cacheInfo($type = null)
    {
        return apc_cache_info( $type );
    }

    public function clean()
    {
        return apc_clear_cache( 'user' );
    }

    public function delete($id)
    {
        return apc_delete( $id );
    }

    public function get($id)
    {
        $data = apc_fetch( $id );

        return (is_array( $data )) ? $data[0] : false;
    }

    public function getMetadata($id)
    {
        $stored = apc_fetch( $id );

        if (count( $stored ) !== 3) {
            return false;
        }

        list( $data, $time, $lifetime ) = $stored;

        return [
          'expire' => $time + $lifetime,
          'time'   => $time,
          'data'   => $data,
        ];
    }

    public function isSupported()
    {
        return extension_loaded( 'apc' );// && ini_get('apc.enabled') == "1";
    }

    public function save($id, $data)
    {
        return apc_store( $id, [$data, time(), $this->lifetime], $this->lifetime );
    }
}

/* End of file Apc.php */
