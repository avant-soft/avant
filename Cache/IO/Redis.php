<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2016 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache\IO;

use Avant\Base\Configurable;
use Avant\Cache\IODriverInterface;
use Avant\Helpers\ArrayHelper;
use Avant\Storages\Redis\Client;

/**
 * Class Redis
 * @subpackage Avant\Cache\IO
 */
class Redis extends Configurable implements IODriverInterface
{
    /**
     * @var int
     */
    protected $db = 0;
    /**
     * @var string
     */
    protected $host = '127.0.0.1';
    /**
     * @var int
     */
    protected $lifetime = 60;
    /**
     * @var string
     */
    protected $namespacePrefix = '';

    /**
     * @var int
     */
    protected $port = 6379;
    /**
     * @var Client
     */
    protected $resource;
    /**
     * @var int
     */
    protected $ttl = 60;

    public function cacheInfo($type = null) { }

    public function clean()
    {
        if ($redis = $this->getRedisResource()) {
            $redis->flushdb();
        }
    }

    public function delete($id)
    {
        if ($redis = $this->getRedisResource()) {
            return (bool)$redis->del( $this->getKey( $id ) );
        }

        return false;
    }

    public function get($id)
    {
        if ($cached = $this->getInternal( $id )) {
            if (!empty( $cached['data'] )) {
                return unserialize( base64_decode($cached['data']) );
            }
        }

        return false;
    }

    public function getMetadata($id)
    {
        if ($cached = $this->getInternal( $id )) {
            return [
              'expire' => ArrayHelper::element( 'time', $cached, 0 ) + ArrayHelper::element( 'lifetime', $cached, $this->lifetime ),
              'time'   => ArrayHelper::element( 'time', $cached, 0 ),
              'data'   => ArrayHelper::element( 'data', $cached, null ),
            ];
        }

        return false;
    }

    public function isSupported() { }

    public function save($id, $data)
    {
        if ($redis = $this->getRedisResource()) {
            $contents = [
              'time'     => time(),
              'lifetime' => $this->lifetime,
              'data'     => base64_encode(serialize( $data )),
            ];

            if ($this->lifetime) {
                $redis->setex( $this->getKey( $id ), $this->lifetime, json_encode( $contents, JSON_FORCE_OBJECT ) );
            } else {
                $redis->set( $this->getKey( $id ), json_encode( $contents, JSON_FORCE_OBJECT ) );
            }

            return true;
        }

        return false;
    }

    protected function getInternal($id)
    {
        if ($redis = $this->getRedisResource()) {
            if (($value = $redis->get( $this->getKey( $id ) )) !== false) {
                return json_decode( $value, true );
            }
        }

        return null;
    }

    protected function getKey($id)
    {
        if (!empty( $this->namespacePrefix )) {
            return $this->namespacePrefix.':'.$id;
        }

        return $id;
    }

    /**
     * @return Client
     */
    protected function getRedisResource()
    {
        if ($this->resource == null) {
            $this->resource = new Client( ['server' => $this->getServer(), 'ttl' => $this->ttl, 'db' => $this->db] );
        }

        return $this->resource;
    }

    protected function getServer()
    {
        //'tcp://127.0.0.1:6379'
        return sprintf( 'tcp://%s:%d', $this->host, $this->port );
    }
}

/* End of file Redis.php */
