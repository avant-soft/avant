<?php

/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache\IO;

use Avant\Base\Configurable;
use Avant\Cache\IODriverInterface;
use Avant\Helpers\FileHelper;
use Exception;

/**
 * Class File
 * @subpackage Avant\Cache\IO
 */
class File extends Configurable implements IODriverInterface
{
    protected $lifetime = 60;
    protected $path;

    public function cacheInfo($type = null)
    {
        return FileHelper::getDir(FileHelper::prepPath($this->path));
    }

    public function clean()
    {
        FileHelper::deleteFiles(FileHelper::prepPath($this->path));
    }

    public function delete($id)
    {
        return FileHelper::deleteFile(FileHelper::prepFilename($this->path . '/' . $this->prepID($id)));
    }

    public function get($id)
    {
        if (!file_exists($file = FileHelper::prepFilename($this->path . '/' . $this->prepID($id)))) {
            return false;
        }

        $data = FileHelper::readFile($file);
        try {
            if (empty($data) || (($data = unserialize(base64_decode($data))) === false)) {
                return false;
            }
        } catch (Exception $exception) {
            return false;
        }

        if (time() > $data['time'] + $data['lifetime']) {
            $this->delete($id);

            return false;
        }

        return $data['data'];
    }

    public function getMetadata($id)
    {
        if (!file_exists($file = FileHelper::prepFilename($this->path . '/' . $this->prepID($id)))) {
            return false;
        }

        $data = FileHelper::readFile($file);
        if (empty($data) || (($data = @unserialize(base64_decode($data))) == false)) {
            return false;
        }

        $data = unserialize($data);

        if (is_array($data)) {
            $time = filemtime($file);

            if (!isset($data['lifetime'])) {
                return false;
            }

            return [
                'expire' => $time + $data['lifetime'],
                'time' => $time,
            ];
        }

        return false;
    }

    public function isSupported()
    {
        return FileHelper::isReallyWritable(FileHelper::prepPath($this->path));
    }

    public function save($id, $data)
    {
        $contents = [
            'time' => time(),
            'lifetime' => $this->lifetime,
            'data' => $data,
        ];

        if (FileHelper::writeFile($file = FileHelper::prepFilename($this->path . '/' . $this->prepID($id)), base64_encode(serialize($contents)) /*json_encode($contents, JSON_FORCE_OBJECT)*/)) {
            @chmod($file, 0777);

            return true;
        }

        return false;
    }

    public function setPath($path)
    {
        if (!file_exists($path) && defined('APP_PATH')) {
            $path = constant('APP_PATH') . $path;
        }

        $this->path = $path;
    }

    private function prepID($id)
    {
        $id = pathinfo($id, PATHINFO_EXTENSION) ? $id : $id . '.cache';

        return $id;
    }
}

/* End of file File.php */