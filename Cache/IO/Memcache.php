<?php
/**
 * @author    Maxim Kirichenko
 * @copyright Copyright (c) 2009-2017 Maxim Kirichenko (kirichenko.maxim@gmail.com)
 * @license   GNU General Public License v3.0 or later
 */

namespace Avant\Cache\IO;

use Avant\Base\Configurable;
use Avant\Cache\IODriverInterface;
use Avant\Helpers\ArrayHelper;

/**
 * Class Memcache
 * @subpackage Avant\Cache\IO
 */
class Memcache extends Configurable implements IODriverInterface
{
    protected $classType = '\Memcache';
    protected $host      = '127.0.0.1';
    protected $lifetime  = 60;
    protected $port      = 11211;
    protected $weight    = 1;
    private   $instance;

    public function cacheInfo($type = null)
    {
        if ($this->instance instanceof \Memcached) {
            return $this->instance->getStats();
        } elseif ($this->instance instanceof \Memcache) {
            return $this->instance->getStats( $type );
        }
    }

    public function clean()
    {
        return $this->instance->flush();
    }

    public function delete($id)
    {
        return $this->instance->delete( $id );
    }

    public function get($id)
    {
        if ($cached = $this->getInternal( $id )) {
            if (isset( $cached['data'] )) {
                return $cached['data'];
            }
        }

        return false;
    }

    public function getMetadata($id)
    {
        if ($cached = $this->getInternal( $id )) {
            return [
              'expire' => ArrayHelper::element( 'time', $cached, 0 ) + ArrayHelper::element( 'lifetime', $cached, $this->lifetime ),
              'time'   => ArrayHelper::element( 'time', $cached, 0 ),
              'data'   => ArrayHelper::element( 'data', $cached, null ),
            ];
        }
    }

    public function initialize($config = [])
    {
        parent::initialize( $config );

        if ($this->isSupported()) {
            $this->instance = new $this->classType;
            $this->instance->addserver( $this->host, $this->port, $this->weight );
        }
    }

    public function isSupported()
    {
        return extension_loaded( 'memcache' );
    }

    public function save($id, $data)
    {
        $contents = [
          'time'     => time(),
          'lifetime' => $this->lifetime,
          'data'     => $data,
        ];

        if ($this->instance instanceof \Memcached) {
            return $this->instance->set( $id, $contents, $this->lifetime );
        } elseif ($this->instance instanceof \Memcache) {
            return $this->instance->set( $id, $contents, 0, $this->lifetime );
        }

        return false;
    }

    protected function getInternal($id)
    {
        if ($cached = $this->instance->get( $id )) {
            return $cached;
        }
    }
}

/* End of file Memcache.php */
